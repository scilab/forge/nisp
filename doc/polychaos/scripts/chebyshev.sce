// Copyright (C) 2015 - Michael Baudin
//
// This file must be used under the terms of the 
// GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

n=11;
x=linspace(-1,1,100);
y=chebyshev_eval(x,n);
plot(x,y)
[r,w]=chebyshev_quadrature(n);
plot(r,zeros(r),"rx")
xlabel("x")
ylabel("P(x)")
title(msprintf("Chebyshev polynomial - Degree %d",n))

