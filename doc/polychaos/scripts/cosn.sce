
// Computes integral of (x^2-1)^n from -1 to 1

function y=integrand(x,n)
    y=(x.^2-1).^n
endfunction

function I=exact(n)
    I=(-1)^n*factorial(n)^2*2^(2*n+1)/factorial(2*n+1)
endfunction

for n=0:100
    [I,e]=intg(-1,1,list(integrand,n));
    mprintf("n=%3d, I=%8.4f (@ %d digits), exact=%8.4f\n",..
    n,I,floor(-log10(e/abs(I))),exact(n))
end
