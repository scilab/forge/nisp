// Copyright (C) 2013 - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Reference
// http://en.wikipedia.org/wiki/Legendre_polynomials

//
// Evaluate Legendre Polynomials
//



    // Check the values.
    x=linspace(-1,1,10)';
    //
    y1=legendre_eval(x,0);
    L=legendre_poly(0);
    y2=horner(L,x);
    mprintf("n=0\n");
    disp([y1 y2])
    //
    y1=legendre_eval(x,1);
    L=legendre_poly(1);
    y2=horner(L,x);
    mprintf("n=1\n");
    disp([y1 y2])
    //
    y1=legendre_eval(x,4);
    L=legendre_poly(4);
    y2=horner(L,x);
    mprintf("n=4\n");
    disp([y1 y2])

///////////////////////////////////
//
//


end

///////////////////////////////////
//

if (%f) then
    m=100000;
    for n=0:5
        mprintf("n=%d\n",n)
        x=distfun_unifrnd(-1,1,m,1);
        y=legendre_eval(x,n);
        mprintf("    Empirical Variance(Hen(X))=%f\n",variance(y))
        mprintf("    Exact Variance(Hen(X))=%f\n",legendre_variance(n))
    end
end
