% Copyright (C) 2013 - 2015 - Michael Baudin
%
% This file must be used under the terms of the 
% Creative Commons Attribution-ShareAlike 3.0 Unported License :
% http://creativecommons.org/licenses/by-sa/3.0/

\section{Polynomial chaos}
\label{sec-pc}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Introduction}
\label{sec-intropc}

The polynomial chaos, introduced by Wiener \cite{Wiener1938}, 
uses Hermite polynomials as the basis and involves independent 
Gaussian random variables. 

Denote \cite{BranickiMajda2012,Wuan2006} the set of multi-indices with finite number of 
nonzero components as 
\begin{eqnarray}
\label{eq-inftymulind}
\nJ=\left\{\balpha=(\alpha_i)_{i\geq 1}, \alpha_i\in\{0,1,2,...\}, |\balpha|<\infty\right\},
\end{eqnarray}
where
\begin{eqnarray}
\label{eq-inftydegree}
|\balpha|=\sum_{i=1}^\infty \alpha_i.
\end{eqnarray}
If $\balpha\in\nJ$, then there is only a finite number of 
nonzero components (otherwise $|\balpha|$ would be infinite).

Let $\{X_i\}_{i\geq 0}$ be an infinite set of 
independent standard normal random variables and let 
$f$ be the associated multivariate tensor product normal 
distribution function. 
Assume that $g(\bX)$ is a random variable with finite variance. 
We are interested in the decomposition of $g(\bX)$ onto 
$He_{\alpha_i}$, the Hermite polynomial of degree $\alpha_i\geq 0$. 

The expectation of $g(\bX)$ is
\begin{eqnarray}
\label{eq-wickpoly4}
E(g(\bX)) = \int g(\bx) f(\bx) d\bx,
\end{eqnarray}
and its variance is:
\begin{eqnarray}
\label{eq-wickpoly5}
V(g(\bX)) = E\left( (g(\bX)-\mu)^2\right),
\end{eqnarray}
where $\mu=E(g(\bX))$.

For any $\balpha\in \nJ$, the Wick polynomial is
\begin{eqnarray}
\label{eq-wickpoly}
\Psi_\balpha(\bX) = \prod_{i=1}^\infty He_{\alpha_i}(X_i).
\end{eqnarray}
The degree of the polynomial $T_\balpha$ is $|\balpha|$.
Notice that, since there is only a finite number of nonzero components in $\balpha$, 
the right hand side of the expression \ref{eq-wickpoly} has a finite 
number of factors.

We consider the inner product
\begin{eqnarray}
\label{eq-wickpoly2}
\lwdotprod{g}{\Psi_\balpha} = \int g(\bx) \Psi_\balpha(\bx) f(\bx) d\bx.
\end{eqnarray}
We use the norm
\begin{eqnarray}
\label{eq-wickpoly3}
\|\Psi_\balpha\|^2 = \int \Psi_\balpha(\bx)^2 f(\bx) d\bx.
\end{eqnarray}

The following theorem is due to Cameron and Martin \cite{CameronMartin1947}.

\begin{theorem}
\label{prop-pcdecom}
(\emph{Cameron-Martin})
Let $\{x_i\}_{i=1}^\infty$ be an infinite set of independent 
standard normal random variables. 
Let $g(\bX)$ be a random variable with finite variance. 
Then $g(\bX)$ has the decomposition
\begin{eqnarray}
\label{eq-pcdecom}
g(\bX) = \sum_{\balpha\in\nJ} a_\balpha \Psi_\balpha(\bX)
\end{eqnarray}
where $\bX=(\xi_1,\xi_2,...)$ and
\begin{eqnarray}
\label{eq-pcdecom1}
a_\balpha=\frac{\lwdotprod{g}{\Psi_\balpha}}{\|\Psi_\balpha\|^2}.
\end{eqnarray}
Moreover, 
\begin{eqnarray}
\label{eq-pcdecom2}
E(g(\bX))&=& a_{\balpha_0},\\
V(g(\bX))&=&\sum_{\balpha\in\nJ} a_\balpha^2 \|\Psi_\balpha\|^2,
\end{eqnarray}
where $\alpha_0=(0,0,...)$.
\end{theorem}

The previous theorem is due to \cite{CameronMartin1947} and will not be proved 
here. 

Moreover, the decomposition \ref{eq-pcdecom} converges in the $L^2_w$ sense. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Truncated decomposition}

In the decomposition \ref{eq-pcdecom}, there is an infinite number of random variables 
$x_i$ and an unrestricted polynomial degree $|\alpha|$. 
In order to use such an expansion in practice, we have to perform 
a double truncation. 
This is why we keep only $p$ independent normal random variables and 
only the order $d$ multivariate polynomials.
Define the truncated index set \cite{Hou06wienerchaos}
\begin{eqnarray}
\label{eq-inftymulind2}
\nJ_{p,d}=\left\{\balpha=(\alpha_1,\alpha_2,...,\alpha_p), |\balpha|\leq d\right\}.
\end{eqnarray}
The finite decomposition is
\begin{eqnarray}
\label{eq-pcdecom4}
g(\bX) \approx \sum_{\balpha\in\nJ_{p,d}} a_\balpha \Psi_\balpha(\bX),
\end{eqnarray}
where $\bX=(X_1,X_2,...,X_p)$ are independent standard normal random variables.
There is a one-to-one mapping from the multi-indices of $\balpha$ of 
the set $\nJ_{p,d}$ and the indices $k=1,2,...,P_d^p$ defined in the 
section \ref{sec-genmultiindices}.
Therefore, the decomposition \ref{eq-pcdecom} can be written:
\begin{eqnarray}
\label{eq-pcdecom3}
g(\bX) \approx \sum_{k=1}^{P_d^p} a_k \Psi_k(\bX),
\end{eqnarray}
where $\Psi_k$ is defined by the equation \ref{eq-multiorthpoly1}.

\begin{proposition}
(\emph{Expectation and variance of the truncated PC expansion})
\label{prop-expvartrpc}
The truncated expansion \ref{eq-pcdecom3} is such that
\begin{eqnarray}
\label{eq-expvartrpc1}
E(g(\bX))&=&a_1 \\
V(g(\bX))&=&\sum_{k=2}^{P_d^p} a_k^2 V(\Psi_k(\bX)).
\label{eq-expvartrpc2}
\end{eqnarray}
\end{proposition}

Since the expansion \ref{eq-pcdecom3} involves only a finite 
number of terms, it is relatively easy to prove the previous proposition.

\begin{proof}
The expectation of $g(\bX)$ is
\begin{eqnarray*}
E(g(\bX))
&=&E\left( \sum_{k=1}^{P_d^p} a_k \Psi_k(\bX) \right) \\
&=&\sum_{k=1}^{P_d^p} E(a_k \Psi_k(\bX)) \\
&=&\sum_{k=1}^{P_d^p} a_k E(\Psi_k(\bX)),
\end{eqnarray*}
since the expectation of a sum is the sum of expectations. 
Then, the equation \ref{eq-expvartrpc1} is a straightforward 
consequence of the proposition \ref{def-expmop}. 

The variance of $g(\bX)$ is
\begin{eqnarray*}
V(g(\bX))
&=&V\left(\sum_{k=1}^{P_d^p} a_k \Psi_k(\bX) \right) \\
&=&\sum_{k=1}^{P_d^p} V\left(a_k \Psi_k(\bX) \right) 
 + \sum_{\substack{k,\ell=1\\ k\neq \ell}}^{P_d^p}
    Cov(a_k \Psi_k(\bX),a_\ell \Psi_\ell(\bX)) \\
&=&\sum_{k=1}^{P_d^p} a_k^2 V\left(\Psi_k(\bX) \right) 
 + \sum_{\substack{k,\ell=1\\ k\neq \ell}}^{P_d^p}
    a_k a_\ell Cov(\Psi_k(\bX),\Psi_\ell(\bX)).
\end{eqnarray*}
However, the proposition \ref{def-covarmop} states that the 
covariances are zero. 
Moreover, the variance of $\Psi_1$ is zero, since this is a constant. 
This leads to the equation \ref{eq-expvartrpc2}.
\end{proof}

In the following proposition, we prove the 
equation \ref{eq-pcdecom1} in the case of the truncated decomposition.

\begin{proposition}
\label{prop-pcak}
(\emph{Coefficients of the truncated decomposition})
The truncated expansion \ref{eq-pcdecom3} is such that
\begin{eqnarray}
\label{eq-pcak1}
a_k=\frac{\lwdotprod{g}{\Psi_k}}{\|\Psi_k\|^2}.
\end{eqnarray}
\end{proposition}

\begin{proof}
Indeed, 
\begin{eqnarray*}
\lwdotprod{g}{\Psi_k} 
&=& \int_I g(\bx) \Psi_k(\bx) w(\bx)d\bx\\
&=& \sum_{\ell=1}^{P_d^p} a_\ell \int_I \Psi_\ell(\bx) \Psi_k(\bx) w(\bx)d\bx\\
&=& \sum_{\ell=1}^{P_d^p} a_\ell \lwdotprod{\Psi_\ell}{\Psi_k} \\
&=& a_k \lwdotprod{\Psi_k}{\Psi_k} \\
&=& a_k \|\Psi_k\|^2,
\end{eqnarray*}
where we have used the 
orthogonality of the functions $\{\Psi_k\}_{k\geq 1}$.
\end{proof}

An immediate consequence of the equation \ref{prop-pcak} is 
that the decomposition \ref{eq-pcdecom3} is unique. 
Indeed, assume that $a_k$ and $b_k$ are real numbers and 
consider the decompositions  
\begin{eqnarray*}
g(\bX) \approx \sum_{k=1}^{P_d^p} a_k \Psi_k(\bX),
\end{eqnarray*}
and
\begin{eqnarray*}
g(\bX) \approx \sum_{k=1}^{P_d^p} b_k \Psi_k(\bX).
\end{eqnarray*}
The equation \ref{eq-pcak1} is satisfied both by the coefficients $a_k$ 
and $b_k$, so that 
\begin{eqnarray*}
a_k=b_k.
\end{eqnarray*}

The following decomposition shows how the coefficients 
be be expressed in termes of expectations and variances. 

\begin{proposition}
\label{prop-pcakE}
(\emph{Coefficients of the truncated decomposition (2)})
The truncated expansion \ref{eq-pcdecom3} is such that
\begin{eqnarray}
\label{eq-pcakE1}
a_1=E(g(\bX))
\end{eqnarray}
and
\begin{eqnarray}
\label{eq-pcakE2}
a_k=\frac{E\left(g(\bX) \Psi_k(\bX)\right)}{V(\Psi_k(\bX))}
\end{eqnarray}
for $k>1$.
\end{proposition}

\begin{proof}
By definition of the expectation, we have 
\begin{eqnarray*}
E\left(g(\bX) \Psi_k(\bX)\right)
&=& \int_I g(\bx) \Psi_k(\bx) f(\bx) d\bx \\
&=& \frac{1}{\int_I w(\bx) d\bx} \int_I g(\bx) \Psi_k(\bx) w(\bx) d\bx \\
&=& \frac{\lwdotprod{g}{\Psi_k}}{\int_I w(\bx) d\bx } \\
&=& \frac{a_k\|\Psi_k\|^2}{\int_I w(\bx) d\bx },
\end{eqnarray*}
where the last equation is implied by \ref{eq-pcak1}.
Hence, 
\begin{eqnarray*}
E\left(g(\bX) \Psi_k(\bX)\right)
&=& \frac{a_k}{\int_I w(\bx) d\bx } \int_I \Psi_k(\bx)^2 w(\bx) d\bx \\
&=& a_k \int_I \Psi_k(\bx)^2 f(\bx) d\bx \\
&=& a_k E(\Psi_k(\bX)^2).
\end{eqnarray*}
This implies:
\begin{eqnarray*}
a_k = \frac{E\left(g(\bX) \Psi_k(\bX)\right)}{E(\Psi_k(\bX)^2)}.
\end{eqnarray*}
For $k=1$, we have $\Psi_1(\bX)=1$, so that the previous equation implies \ref{eq-pcakE1}. 
For $k>1$, we have 
\begin{eqnarray*}
a_k = \frac{E\left(g(\bX) \Psi_k(\bX)\right)}{V(\Psi_k(\bX))+E(\Psi_k(\bX))^2}.
\end{eqnarray*}
However, we know from proposition \ref{def-expmop} that, for $k>1$, 
we have $E(\Psi_k(\bX))=0$, which concludes the proof.
\end{proof}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Univariate decomposition examples}
\label{sec-pcexamples}

In this section, we present several examples of decomposition of 
univariate random variables on Hermite polynomials.

Assume that $X$ is a standard univariate normal random variable (i.e. $p=1$) 
and consider the random variable $g(X)$ where $g$ is a square integrable function. 
Its truncated polynomial chaos decomposition is
\begin{eqnarray}
\label{eq-pcexamples1}
g(X) \approx \sum_{k=1}^{P_d^1} a_k \Psi_k(X),
\end{eqnarray}
where $\Psi_k$ are the univariate Hermite polynomials.
The number of univariate polynomials is given by the proposition \ref{prop-multipolymnb}, 
which states that, for $p=1$, we have
\begin{eqnarray*}
P_d^1
&=&\choosefun{1+d}{d} \\
&=& \frac{(1+d)!}{d!1!} \\
&=& 1+d.
\end{eqnarray*}
Moreover, the equation \ref{eq-multiorthpoly1} states that the functions $\Psi_k$ are 
defined in terms of the Hermite polynomials as
\begin{eqnarray*}
\Psi_k(x) = He_{\alpha^{(k)}}(x),
\end{eqnarray*}
for any $x\in\RR$ and $k\geq 1$, where 
\begin{eqnarray*}
\alpha^{(k)} = k-1.
\end{eqnarray*}
This implies :
\begin{eqnarray*}
\Psi_k(x) = He_{k-1}(x),
\end{eqnarray*}
for any $x\in\RR$ and $k\geq 1$.
The equation \ref{eq-pcexamples1} simplifies to:
\begin{eqnarray*}
g(X) \approx a_1 He_0(X) + a_2 He_1(X) + ... + a_{d+1} He_d(X),
\end{eqnarray*}
where $d\geq 1$ is an integer and $\{He_i\}_{i=0,1,...,d}$ are Hermite polynomials.

The proposition \ref{prop-pcakE} states that the coefficients are 
\begin{eqnarray*}
a_1=E(g(X))
\end{eqnarray*}
and
\begin{eqnarray*}
a_k
&=&\frac{E\left(g(X) He_{k-1}(X)\right)}{V(He_{k-1}(X))} \\
&=&\frac{E\left(g(X) He_{k-1}(X)\right)}{(k-1)!}
\end{eqnarray*}
for $k>1$.
In general, this requires to compute the integral
\begin{eqnarray}
E\left(g(X) He_{k-1}(X)\right)
&=&\int_\RR g(x) He_{k-1}(x) f(x) dx,
\label{eq-pcexamples9}
\end{eqnarray}
for $k\geq 1$, where $f$ is the Gaussian probability distribution function defined 
in the equation \ref{eq-pdfhermite}.

In this section, we consider several specific examples of functions $g$ and 
present the associated coefficients $a_i$, for $i=1,2,...,d+1$.

\begin{example}
\label{ex-pcexampleC1}
(\emph{Constant})
Assume that 
\begin{eqnarray*}
g(x) = c,
\end{eqnarray*}
for some real constant $c$. 
A possible exact decomposition is:
\begin{eqnarray*}
g(X) = c He_0(X).
\end{eqnarray*}
Since the polynomial chaos decomposition is unique, this 
is \emph{the} decomposition. 
But it is interesting to see how the coefficients can be computed 
in this particular case.
We have 
\begin{eqnarray*}
a_1
&=&E(c)\\
&=&c.
\end{eqnarray*}
Moreover, 
\begin{eqnarray*}
a_k
&=&\frac{E\left(c He_{k-1}(X)\right)}{(k-1)!} \\
&=&c \frac{E\left(He_{k-1}(X)\right)}{(k-1)!}
\end{eqnarray*}
However, the proposition \ref{prop-expecpoly} 
states that $E\left(He_{k-1}(X)\right)=0$ for $k>1$. 
This implies
\begin{eqnarray*}
a_k=0
\end{eqnarray*}
for $k>1$. 
\end{example}

\begin{example}
(\emph{Standard normal random variable}) 
Assume that 
\begin{eqnarray*}
g(x) = x,
\end{eqnarray*}
for any $x\in\RR$. 
The exact polynomial chaos decomposition is 
\begin{eqnarray}
\label{eq-examplepcdecompX}
g(X) = He_1(X),
\end{eqnarray}
since $He_1(X)=X$. 
Again, it is interesting to see how the coefficients 
can be computed in this particular case. 
We have 
\begin{eqnarray*}
a_1 
&=& E(X) \\
&=&0,
\end{eqnarray*}
since, by assumption, $X$ has a mean equal to zero. 
Moreover, 
\begin{eqnarray*}
a_k 
&=&\frac{E\left(X He_{k-1}(X)\right)}{(k-1)!} \\
&=&\frac{E\left(He_1(X) He_{k-1}(X)\right)}{(k-1)!},
\end{eqnarray*}
since the first Hermite polynomial is $He_1(x)=x$. 
The equation \ref{eq-exppipj1} then implies $a_k=0$, 
for $k>2$. 
Moreover,
\begin{eqnarray*}
a_2
&=&\frac{E\left(He_1(X)^2\right)}{V(He_1(X))} \\
&=&\frac{V(He_1(X))}{0!} \\
&=&\frac{0!}{1} \\
&=&1,
\end{eqnarray*}
where the previous equation comes from the equation \ref{eq-exppipj2}. 
This immediately leads to the equation \ref{eq-examplepcdecompX}.
\end{example}

\begin{example}
(\emph{The square function}) 
Consider the function 
\begin{eqnarray*}
g(x) = x^2,
\end{eqnarray*}
for any $x\in\RR$. 
Consider the random variable $g(X)=X$, where 
$X$ is a standard normal random variable. 
From the table \ref{fig-hermitepoly1-5}, we see that
\begin{eqnarray*}
x^2
&=&x^2-1+1 \\
&=&He_2(x)+He_0(x).
\end{eqnarray*}
Therefore, the exact polynomial chaos decomposition of 
$g$ is 
\begin{eqnarray*}
g(x) = He_0(x)+He_2(x).
\end{eqnarray*}
\end{example}

\begin{example}
(\emph{Decomposition of $x^n$})
We can compute the decomposition of $x^n$, where $n$ is a positive 
integer. 
For example, we have
\begin{eqnarray*}
x^5 = 15He_1(x)+10He_3(x)+He_5(x).
\end{eqnarray*}
In the figure \ref{fig-xndecomp2}, we present the coefficients $a_k$ in the 
decomposition:
\begin{eqnarray*}
x^n = \sum_{k\geq 0} a_k He_k(x).
\end{eqnarray*}
\end{example}

\begin{figure}
\begin{center}
\begin{tabular}{lllllllllll}
\hline
$k$&$a_0$&$a_1$&$a_2$&$a_3$&$a_4$&$a_5$&$a_6$&$a_7$&$a_8$&$a_9$\\
\hline
$x^0$&1&&&&&&&&&\\
$x^1$&&1&&&&&&&&\\
$x^2$&1&&1&&&&&&&\\
$x^3$&&3&&1&&&&&&\\
$x^4$&3&&6&&1&&&&&\\
$x^5$&&15&&10&&1&&&&\\
$x^6$&15&&45&&15&&1&&&\\
$x^7$&&105&&105&&21&&1&&\\
$x^8$&105&&420&&210&&28&&1&\\
$x^9$&&945&&1260&&378&&36&&1\\
\hline
\end{tabular}
\end{center}
\caption{Coefficients $a_k$ in the decomposition of $x^n$ into Hermite polynomials, for $n=0,1,...,9$.}
\label{fig-xndecomp2}
\end{figure}

\begin{example}
(\emph{Transformation})
TODO : X normal $\mu,\sigma$ and the standardization of $X$.
\end{example}

\begin{example}
(\emph{Some approximate decompositions})
We compute the approximate polynomial chaos decomposition of several functions, 
where $X$ is a standard normal random variable. 
In order to perform the numerical integration involved in the 
integral \ref{eq-pcexamples9}, we have used the \scifun{intg} function,
with an absolute tolerance equal to \scivar{1.e-8}, 
and numerical bounds equal to \scivar{-10} and \scivar{+10}. 
We compute the coefficients up to the degree 14. 

\begin{figure}
\begin{center}
\begin{tabular}{lll}
\hline
     & $\cos(x)$ & $\sin(x)$ \\
$a_0$   &    0.6065307&            \\
$a_1$   &              &  0.6065307\\
$a_2$   &  -0.3032653&             \\
$a_3$   &              &-0.1010884 \\
$a_4$   &    0.0252721&            \\
$a_5$   &              &  0.0050544\\
$a_6$   &  -0.0008424&             \\
$a_7$   &             &-0.0001203  \\
$a_8$   &    0.0000150&            \\
$a_9$   &              &  0.0000017\\
$a_{10}$&  -0.0000002&             \\
$a_{11}$&              &-1.519D-08 \\
$a_{12}$&    1.266D-09&            \\
$a_{13}$&              &  9.740D-11\\
$a_{14}$&  -6.957D-12&             \\
\hline
\end{tabular}
\end{center}
\caption{
Coefficients $a_k$ in the decomposition of several functions 
into Hermite polynomials.
}
\label{fig-appdecomphermite}
\end{figure}

These numerical experiments are consistent with the following 
exact integrals, computed from Wolfram Alpha \cite{WWWWolframAlpha}.

\begin{eqnarray*}
\int_{-\infty}^\infty \cos(x) He_n(x) w(x) dx 
&=&
\left\{
\begin{array}{l}
\end{array}
\right.
\end{eqnarray*}

\end{example}


% integral_(-infinity)^infinity cos(x) H_n(x) exp(-x^2/2) dx 
%%   = sqrt((2 pi)/e)  ~~  1.52034 si n/2 est pair
%%   = -sqrt((2 pi)/e)  ~~  -1.52034 si n/2 est impair

% integral_(-infinity)^infinity sin(x) H_n(x) exp(-x^2/2) dx 
%    = sqrt((2 pi)/e)  ~~  1.52034 si (n-1)/2 est pair
%    = -sqrt((2 pi)/e)  ~~  -1.52034 si (n-1)/2 est impair

% Sn= integral_(-infinity)^infinity exp(x) H_n(x) exp(-x^2/2) dx 
%   =sqrt(2 e pi)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Generalized polynomial chaos}
\label{section-gpc}

In 2002, Xiu and Karniadakis \cite{Xiu2002,Xiu2003} introduced the 
generalized polynomial chaos. 
They notice that the Hermite polynomials are quite effective to solve some 
stochastic differential equations involving Gaussian as well as non Gaussian random variables, 
e.g. log-normal random variables. 
However, they emphasize that, for general non-Gaussian random inputs, the convergence 
rate is not fast. 
This is why they introduce the generalized polynomial chaos, where each 
probability distribution function is associated to a family of orthogonal 
polynomials. 

The involved orthogonal polynomials are constructed from limit relationships 
from the hypergeometric orthogonal polynomials. 
This leads to a classification which allows to create a tree of 
relations between the polynomials, called the Askey scheme.
The mapping from the distribution to the corresponding orthogonal polynomial 
is presented in the figure \ref{fig-mappdfortho}.

\begin{figure}
\begin{center}
\begin{tabular}{llll}
\hline
& Distribution & Polynomial & Support \\
\hline
Continuous   & Normal & Hermite & $(-\infty,\infty)$\\
Distribution & Gamma (Exponential) & Laguerre & $[0,\infty)$\\
             & Beta & Jacobi & $[a,b]$\\
             & Uniform & Legendre & $[a,b]$\\
\hline
Discrete     & Poisson & Chalier & $\{0,1,2,...\}$\\
Distribution & Binomial & Krawtchouk & $\{0,1,2,...,N\}$\\
             & Negative Binomial & Meixner & $\{0,1,2,...\}$\\
             & Hypergeometric & Hahn & $\{0,1,2,...,N\}$\\
\hline
\end{tabular}
\end{center}
\caption{
Map from a distribution function to the associated 
orthogonal polynomials \cite{Lucor2008}.
}
\label{fig-mappdfortho}
\end{figure}

The finite decomposition is
\begin{eqnarray}
\label{eq-gpc1}
g(\bX) \approx \sum_{\balpha\in\nJ_{p,d}} a_\balpha \Psi_\balpha(\bX),
\end{eqnarray}
where $\bX=(X_1,X_2,...,X_p)$ are independent random variables and 
$\Psi_k$ is the tensor product of orthogonal polynomials $\phi$, 
so that 
\begin{eqnarray}
\label{eq-gpc2}
\Psi_k(\bx)=\prod_{i=1}^p \phi_{\alpha_i^{(k)}}(x_i)
\end{eqnarray}
for $k=1,2,...,P_d^p$.

\begin{example}
Assume that $X_1$ is a standard normal random variable, 
$X_2$ is a uniform random variable in the interval $[-1,1]$, 
and assume that $X_1$ and $X_2$ are independent. 
Therefore, the generalized polynomial chaos associated with 
the random variable $\bX=(X_1,X_2)$ involves the Hermite orthogonal 
polynomials (for $X_1$) and the Legendre orthogonal polynomials 
(for $X_2$). 
\end{example}

\begin{example}
(\emph{Transformation})
TODO : X U(0,1) and G(X)=exp(X).
\end{example}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Notes and references}

The book \cite{LeMaitreKnio2010} and the thesis \cite{LeMaitre2005} 
present spectral methods, including polynomial chaos. 

In the figure \ref{fig-xndecomp2}, we present the coefficients $a_k$ in the decomposition 
of $x^n$ into Hermite polynomials, for $n=0,1,...,9$.
This table is similar to the table 22.12 in the chapter 22 of \cite{abramowitz+stegun}. 
But our table presents the coefficients for the probabilist's Hermite polynomials 
$He_n$, while the table 22.12 presents the coefficients of the physicist's Hermite polynomials $H_n$. 
