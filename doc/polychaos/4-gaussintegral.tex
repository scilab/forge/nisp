% Copyright (C) 2013 - 2015 - Michael Baudin
%
% This file must be used under the terms of the 
% Creative Commons Attribution-ShareAlike 3.0 Unported License :
% http://creativecommons.org/licenses/by-sa/3.0/

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\appendix 
\section{Integrals}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Gaussian integral}
\label{sec-gaussinteg}

\begin{proposition}
(\emph{Gaussian integral})
\label{prop-gaussinteg}
\begin{eqnarray}
\label{eq-gaussinteg0}
\int_{-\infty}^{+\infty} \exp(-x^2/2) dx =\sqrt{2 \pi}.
\end{eqnarray}
\end{proposition}

\begin{proof}
We have,
\begin{eqnarray*}
\left( \int_{-\infty}^{+\infty} \exp(-x^2) dx \right)^2
&=&\left( \int_{-\infty}^{+\infty} \exp(-x^2) dx \right) \left( \int_{-\infty}^{+\infty} \exp(-x^2) dx \right) \\
&=&\left( \int_{-\infty}^{+\infty} \exp(-x^2) dx \right) \left( \int_{-\infty}^{+\infty} \exp(-y^2) dy \right) \\
&=&\int_{-\infty}^{+\infty} \int_{-\infty}^{+\infty} \exp\left(-x^2-y^2\right) dxdy.
\end{eqnarray*}
Let us consider the polar change of variable:
\begin{eqnarray*}
x&=&r\cos(\theta),\\
y&=&r\sin(\theta)
\end{eqnarray*}
where $r\in\RR$ and $\theta\in[0,2\pi]$.
Its Jacobian is $J(r,\theta)=r$ (see, e.g. \cite{DAVIS84a}). 
Hence, 
\begin{eqnarray*}
\left( \int_{-\infty}^{+\infty} \exp(-x^2) dx \right)^2
&=&\int_0^{+\infty} \int_{0}^{2\pi} \exp\left(-r^2\right) r drd\theta \\
&=&2\pi \int_0^{+\infty} r \exp\left(-r^2\right) dr.
\end{eqnarray*}
Consider the change of variable $s=-r^2$. 
We have $ds=2r dr$, which implies 
\begin{eqnarray*}
\left( \int_{-\infty}^{+\infty} \exp(-x^2) dx \right)^2
&=&\pi \int_{-\infty}^0 \exp\left(-s\right) ds \\
&=&\pi (\exp(0)-0) \\
&=&\pi.
\end{eqnarray*}
Hence, 
\begin{eqnarray}
\label{eq-gaussinteg1}
\int_\RR \exp(-x^2) dx =\sqrt{\pi}.
\end{eqnarray}
Now consider the change of variable $y=\sqrt{2} x$. 
We have $dx=dy/\sqrt{2}$, which implies
\begin{eqnarray*}
\int_\RR \exp(-x^2) dx = \frac{1}{\sqrt{2}} \int_\RR \exp(-y^2/2) dy.
\end{eqnarray*}
We plug the equation \ref{eq-gaussinteg1} into the previous equality 
and get \ref{eq-gaussinteg0}.
\end{proof}

The previous proof does not take into account the improper integrals 
which appear during the computation. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Weighted Gaussian integral of powers of x}
\label{sec-wintx2}

\begin{proposition}
\label{prop-wintinodd}
For any odd $n$, 
\begin{eqnarray}
\label{eq-wintx}
\int_{-\infty}^{+\infty} x^n \exp(-x^2/2) dx =0,
\end{eqnarray}
\end{proposition}

\begin{proof}
If $n$ is odd, the function $x^n$ is antysymmetric, since 
$(-x)^n=-x^n$ for any $x\in\RR$. 
On the other hand, the function $\exp(-x^2/2)$ is symmetric, since 
$\exp(-(-x)^2/2)=\exp(-x^2/2)$. 
We consider the change of variable $y=-x$ and get:
\begin{eqnarray*}
\int_{-\infty}^0 x^n \exp(-x^2/2) dx 
= \int_{0}^{+\infty} (-y)^n \exp(-(-y)^2/2) dy \\
= - \int_{0}^{+\infty} y^n \exp(-y^2/2) dy.
\end{eqnarray*}
This implies
\begin{eqnarray*}
\int_{-\infty}^{+\infty} x^n \exp(-x^2/2) dx 
&=& \int_{-\infty}^0 x^n \exp(-x^2/2) dx +\int_0^{+\infty} x^n \exp(-x^2/2) dx  \\
&=& 0.
\end{eqnarray*}
and concludes the proof.
\end{proof}

\begin{proposition}
\label{prop-wintineven}
For any even $n$, we have
\begin{eqnarray}
\label{eq-wintin}
\int_{-\infty}^{+\infty} x^n \exp(-x^2/2) dx
=1\cdot 3\cdot 5 \cdot ... \cdot (n-1) \sqrt{2\pi}.
\end{eqnarray}
\end{proposition}

\begin{proof}
Let us denote by $I_n$ the integral
\begin{eqnarray}
\label{eq-wintin0}
I_n = \int_{-\infty}^{+\infty} x^n \exp(-x^2/2) dx.
\end{eqnarray}
We are going to prove the equality
\begin{eqnarray}
\label{eq-wintin2}
I_{n+2} = (n+1) I_n.
\end{eqnarray}
Integrating by part, we get
\begin{eqnarray*}
I_n&=& \left[\frac{x^{n+1}}{n+1} \exp(-x^2/2)\right]_{-\infty}^{+\infty} 
    + \int_{-\infty}^{+\infty} \frac{x^{n+1}}{n+1} x \exp(-x^2/2) dx.
\end{eqnarray*}
On the other hand, 
\begin{eqnarray*}
\lim_{x\rightarrow +\infty} x^n \exp(-x^2/2) = 0,
\end{eqnarray*}
for any integer $n$.
Hence, 
\begin{eqnarray*}
\left[\frac{x^{n+1}}{n+1} \exp(-x^2/2)\right]_{-\infty}^{+\infty} = 0.
\end{eqnarray*}
This implies
\begin{eqnarray*}
I_n 
&=& \int_{-\infty}^{+\infty} \frac{x^{n+2}}{n+1} \exp(-x^2/2) dx \\
&=& \frac{1}{n+1} I_{n+2}.
\end{eqnarray*}
Moreover, we from from proposition \ref{prop-gaussinteg} that 
$I_0=\sqrt{2\pi}$, which concludes the proof.
\end{proof}

Consider the Gaussian weight function $w(x)=\exp(-x^2/2)$. 
The consequence of the propositions \ref{prop-wintinodd} and \ref{prop-wintineven} 
is that a monomial $x^n$ is in $L_w^2(\RR)$, for any integer $n$. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{A Legendre integral}
\label{sec-legcos}

The goal of this section is to prove the following proposition. 


\begin{proposition}
\label{prop-legcos1}
For any integer $n$, 
\begin{eqnarray}
\label{eq-legcos1}
\int_{-1}^1 (x^2-1)^n dx = (-1)^n \frac{(n!)^2 2^{2n+1}}{(2n+1)!}.
\end{eqnarray}
\end{proposition}

\begin{proof}
Consider the change of variable $x=\sin(\theta)$ for $\theta\in[-\pi/2,\pi/2]$. 
Therefore $dx =\cos(\theta)d\theta$. 
Moreover, 
\begin{eqnarray*}
x^2-1 
&=& \sin(\theta)^2 - 1 \\
&=& - \cos(\theta)^2.
\end{eqnarray*}
Hence, 
\begin{eqnarray*}
\int_{-1}^1 (x^2-1)^n dx = 
(-1)^n \int_{-\pi/2}^{\pi/2} \cos(\theta)^{2n+1} d\theta.
\end{eqnarray*}

Let $I_n$ denote the integral :
\begin{eqnarray*}
I_n = \int_{-\pi/2}^{\pi/2} \cos(\theta)^{2n+1} d\theta.
\end{eqnarray*}

We are going to prove that 
\begin{eqnarray}
\label{eq-legcos3}
I_n =\frac{(n!)^2 2^{2n+1}}{(2n+1)!},
\end{eqnarray}
which will immediately lead to the equation \ref{eq-legcos1}.

In order to do this, we are going to derive a recurrence formula for 
$I_n$. 
Integrating by part, we get :
\begin{eqnarray*}
I_n
&=& \int_{-\pi/2}^{\pi/2} \cos(\theta)^{2n}  \cos(\theta) d\theta \\
&=& 
\left[ \cos(\theta)^{2n}  \sin(\theta) \right]_{-\pi/2}^{\pi/2}
+ \int_{-\pi/2}^{\pi/2} 2n  \cos(\theta)^{2n-1}  \sin(\theta) \sin(\theta) d\theta\\
&=& 2n  \int_{-\pi/2}^{\pi/2} \cos(\theta)^{2n-1}  \sin(\theta)^2 d\theta,
\end{eqnarray*}
since the $\sin$ function is zero at $\theta=-\pi/2$ and $\theta=\pi/2$.
This implies :
\begin{eqnarray*}
I_n
&=& 2n  \int_{-\pi/2}^{\pi/2} \cos(\theta)^{2n-1}  (1-\cos(\theta)^2) d\theta \\
&=& 2n  \int_{-\pi/2}^{\pi/2} \cos(\theta)^{2n-1}  d\theta 
- 2n  \int_{-\pi/2}^{\pi/2} \cos(\theta)^{2n+1}  d\theta \\
&=& 2n  I_{n-1}   - 2n  I_n.
\end{eqnarray*}
We move $I_n$ from the right to the left hand side and get :
\begin{eqnarray*}
(2n+1) I_n = 2n  I_{n-1}.
\end{eqnarray*}
Therefore, 
\begin{eqnarray}
\label{eq-legcos4}
I_n = \frac{2n}{2n+1}  I_{n-1},
\end{eqnarray}
for $n\geq 0$.

The first term in the recurrence is 
\begin{eqnarray*}
I_0 
&=& \int_{-\pi/2}^{\pi/2} \cos(\theta) d\theta \\
&=& \left[\sin(\theta) \right]_{-\pi/2}^{\pi/2} \\
&=& 2.
\end{eqnarray*}

We can now prove that the equation \ref{eq-legcos3} is true, by 
recurrence on $n$. 
Assuming that $0!=1$, the previous equation shows that the 
equation \ref{eq-legcos3} is true for $n=0$. 

Assume that the recurrence is correct for $n-1$. 
This implies :
\begin{eqnarray*}
I_{n-1} =\frac{((n-1)!)^2 2^{2n-1}}{(2n-1)!}.
\end{eqnarray*}
The equation \ref{eq-legcos4} then implies :
\begin{eqnarray*}
I_n 
&=& \frac{2n}{2n+1}  \frac{((n-1)!)^2 2^{2n-1}}{(2n-1)!} \\
&=& \frac{n^2}{n(2n+1)}  \frac{((n-1)!)^2 2^{2n}}{(2n-1)!} \\
&=& \frac{(n!)^2 2^{2n}}{(2n+1)!},
\end{eqnarray*}
which shows that the recurrence is true for $n$ and concludes the proof.
\end{proof}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{A Laguerre integral}
\label{sec-laguint}

The goal of this section is to prove the following proposition. 

\begin{proposition}
\label{prop-laguint}
We have 
$$
\int_{0}^\infty x^n e^{-x} dx = n!,
$$
for $n\geq 0$.
\end{proposition}

\begin{proof}
Let 
$$
I_n = \int_{0}^\infty x^n e^{-x} dx,
$$
for $n=0,1,...$.
Obviously, 
$$
I_0= \int_{0}^\infty e^{-x} dx = 1.
$$
Integrating by part, we get :
\begin{eqnarray*}
I_{n+1}
&=& \int_{0}^\infty x^{n+1} e^{-x} dx \\
&=& - \left[x^{n+1} e^{-x} \right] + (n+1) \int_{0}^\infty x^n e^{-x} dx \\
&=& (n+1)I_n.
\end{eqnarray*}
By induction on $n$, we get : 
$$
I_n = n!.
$$
\end{proof}
