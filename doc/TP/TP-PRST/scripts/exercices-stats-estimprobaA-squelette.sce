mu=2;
sigma=3;
seuil=1.e4;
// Calcul exact
pfExacte=distfun_logncdf(TODO);
mprintf("Pf (exact):%e\n",pfExacte);
// Estimation Monte-Carlo
Nsample=100000;
X=distfun_lognrnd(TODO);
i=find(X>seuil);
nfail=size(i,"*");
mprintf("Nombre de depassements:%d\n",nfail);
pf=TODO;
mprintf("Pf (estimation):%e\n",pf);
