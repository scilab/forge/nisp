// Copyright (C) 2013-2014 - Michael Baudin
//
// This file must be used under the terms of the 
// GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Regle des 3 sigma
p1=distfun_normcdf(1,0,1)-distfun_normcdf(-1,0,1)
p2=distfun_normcdf(2,0,1)-distfun_normcdf(-2,0,1)
p3=distfun_normcdf(3,0,1)-distfun_normcdf(-3,0,1)
