// Copyright (C) 2013-2014 - Michael Baudin
//
// This file must be used under the terms of the 
// GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Estimation d'un quantile
// Experience D
// Estimation d'un quantile en queue haute.
// X est uniforme dans [0,1].
//
al=0.05; // 1-0.95
mprintf("Variable Uniforme [0,1]\n");
mprintf("Quantile a 1-%f\n",al);
// Calcul exact
xExact=distfun_unifinv(al,0,1,%f);
mprintf("x (exact):%e\n",xExact);
// Estimation Monte-Carlo
Nsample=200;
nRepeat=10000;
X=distfun_unifrnd(0,1,nRepeat,Nsample);
X=gsort(X,"c","d");
i=floor(Nsample*al);
x=X(:,i);
//
h=scf();
histo(x,[],%t);
plot([xExact,xExact],[0,30],"r-");
xtitle("10000 quantiles a 95% sur 200 realisations",..
"Quantile","Frequence");
legend(["Data","Exact"]);
