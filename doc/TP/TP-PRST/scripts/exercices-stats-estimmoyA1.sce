// Copyright (C) 2013-2014 - Michael Baudin
//
// This file must be used under the terms of the 
// GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Estimation de la moyenne

// Experience A1
// Estimation de la moyenne empirique 
// d'une variable X de loi exponentielle.
//
// mu : moyenne de la variable exponentielle X
// n : nombre de realisations de X
// M : moyenne de la variable exponentielle (exacte)
// V : variance de la variable exponentielle (exacte)
// X : matrice de taille n-par-1, 
//     realisations de la variable aleatoire
// Mn : moyenne empirique de X
//
mu=12;
mprintf("mu=%f\n",mu);
// 1. Calculer la moyenne, la variance
M=mu; // Moyenne
V=mu^2; // Variance
// 2. Utiliser distfun_expstat
[M,V] = distfun_expstat(mu);
// 3. Generer n realisations
n=10;
X=distfun_exprnd(mu,[n,1]);
// 4. Estimer la moyenne empirique
Mn=mean(X);
mprintf("E(X)=%f, V(X)=%f\n",M,V);
mprintf("E(Mn)=%f, V(Mn)=%f\n",M,V/n);
mprintf("Mean(X)=%f\n",Mn);
