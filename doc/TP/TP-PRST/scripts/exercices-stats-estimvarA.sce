// Copyright (C) 2013-2014 - Michael Baudin
//
// This file must be used under the terms of the 
// GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Estimation de la variance

// Experience A
// Calcul de la variance non biaisee
mu=5;
[M,V] = distfun_expstat(mu);
Nsample=10000;
mprintf("Variable Exponentielle (mu=%f)\n",mu);
for n=[2 4 8 16]
    mprintf("n=%d\n",n);
    mprintf("E(Sn)=%f\n",V);
    for i=1:5
        X=distfun_exprnd(mu,Nsample,n);
        Sn=variance(X,"c");
        mprintf("#%d, Mean(Sn)=%f\n",i, mean(Sn));
    end
end
