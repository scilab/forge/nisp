// Copyright (C) 2013-2014 - Michael Baudin
//
// This file must be used under the terms of the 
// GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Intervalle de confiance de la moyenne d'une variable normale
// Experience B (variance inconnue)
Sn=variance(X,"r",1);
z=distfun_tinv(al,Nsample-1,%f);
delta=z*sqrt(Sn/(Nsample-1));
low=Mn-delta;
up=Mn+delta;
mprintf("Int. Conf.:[%f,%f]\n",low,up)
