n = 100; // taille echantillon
mu = 2;
sigma = 1;
mux=distfun_lognstat(TODO); // esperance de X
X = distfun_lognrnd(TODO); // Echantillon X
Mn = mean(X); // moyenne empirique
Sn2 = variance(X,"r",1); // variance empirique (biaisee)
level=0.05; // =1-0.95
al=level/2;
// Quand n n'est pas tres grand:
q = distfun_tinv(TODO);
// Quand n est grand:
// q = distfun_norminv(TODO);
delta = TODO;
low=Mn-delta;
up=Mn+delta;
mprintf("Moyenne exacte = %f\n",mux);
mprintf("Moyenne empirique = %f\n",Mn);
mprintf("Intervalle a 0.95%%: [%f,%f]\n",low,up);
