// Copyright (C) 2013-2014 - Michael Baudin
//
// This file must be used under the terms of the 
// GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Loi Binomiale
N=20;
pr=0.1;
x=1;
// Avec factorial:
c=factorial(N)/factorial(x)/factorial(N-x)
P=c*pr^x*(1-pr)^(N-x);
mprintf("P(X=1) (factorial)=%f\n",P)
// Avec nchoosek:
P=specfun_nchoosek(N,x)*pr^x*(1-pr)^(N-x)
mprintf("P(X=1) (nchoosek)=%f\n",P)
// Avec binopdf:
P=distfun_binopdf(x,N,pr);
mprintf("P(X=1) (binopdf)=%f\n",P)
//
// Graphique de la distribution
scf();
N1 = 20;
x = 0:N1;
y1 = distfun_binopdf(x,N1,0.5);
plot(x,y1,"bo-")
N2 = 20;
x = 0:N2;
y2 = distfun_binopdf(x,N2,0.7);
plot(x,y2,"go--")
N3 = 40;
x = 0:N3;
y3 = distfun_binopdf(x,N3,0.5);
plot(x,y3,"ro:")
legend(["pr=0.5, N=20","pr=0.7, N=20","pr=0.5, N=40"]);
xtitle("Binomial PDF","x","P(x)")
