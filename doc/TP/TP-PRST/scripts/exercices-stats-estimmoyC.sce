// Copyright (C) 2013-2014 - Michael Baudin
//
// This file must be used under the terms of the 
// GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Estimation de la moyenne

// Experience C
// Distribution de la moyenne empirique d'une variable normale.
mu=0;
sigma=1;
Nsample=1000;
x=linspace(-4,4,100);
h=scf();
//
k=0;
for n=[1,2,4,8]
    k=k+1;
    X = distfun_normrnd(mu,sigma,Nsample,n);
    Mn=mean(X,"c");
    subplot(2,2,k);
    histo(Mn,[],%t)
    y=distfun_normpdf(x,mu,sigma/sqrt(n));
    plot(x,y,"r-");
    xtitle("n="+string(k),"M","Frequency");
    legend(["Data","PDF"]);
end
//
h.children(1).data_bounds(:,2)=[0;1.2];
h.children(2).data_bounds(:,2)=[0;1.2];
h.children(3).data_bounds(:,2)=[0;1.2];
h.children(4).data_bounds(:,2)=[0;1.2];

