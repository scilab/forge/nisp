mu=2;
sigma=1;
n=100;
Nsample=10000;
X = distfun_lognrnd(TODO);
Mn = mean(X,"r");
Sn2 = variance(X,"r",%nan); // variance empirique (biaisée)
delta = TODO
low=Mn-delta;
up=Mn+delta;
x=linspace(5,25,50);
scf();
histo(low,x,%t,1);
histo(up,x,%t,2);
plot([mux,mux],[0,0.3],"r-");
legend(["Lower Bound","Upper Bound","E(X)"]);
xtitle("Invervalle de confiance a 95% - X~Log-Normale",..
"Mean","Frequency")
// Calcul de P(I contains mux)
i=find(mux>low&mux<up);
nInBounds=size(i,"*");
pInBounds=nInBounds/Nsample;
mprintf("P(I contains E(X))=%f\n",pInBounds);
