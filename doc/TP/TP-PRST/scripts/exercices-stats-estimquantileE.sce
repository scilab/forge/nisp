// Copyright (C) 2013-2014 - Michael Baudin
//
// This file must be used under the terms of the 
// GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Estimation d'un quantile
// Experience E
// Estimation d'un quantile en queue haute.
// X est normale(4,7)
//
al=0.05; // 1-0.95
mprintf("Quantile a 1-%f\n",al);
mu=4;
sigma=7;
// Calcul exact
xExact=distfun_norminv(al,mu,sigma,%f);
mprintf("x (exact):%e\n",xExact);
// Estimation Monte-Carlo
Nsample=200;
nRepeat=10000;
X=distfun_normrnd(mu,sigma,nRepeat,Nsample);
X=gsort(X,"c","d");
i=floor(Nsample*al);
x=X(:,i);
//
// Distribution of the sample quantile
mprintf("Moyenne Empirique(X)=%f\n",mean(x))
mprintf("E(X)=%f\n",xExact)
mprintf("Variance Empirique(X)=%f\n",variance(x))
y=distfun_normpdf(xExact,mu,sigma);
V=al*(1-al)/(y^2)/Nsample;
mprintf("V(X)=%f\n",V)
//
t=linspace(12,19,20);
h=scf();
histo(x,[],%t);
plot([xExact,xExact],[0,0.4]);
s=linspace(xExact-3*sqrt(V),xExact+3*sqrt(V),100);
y=distfun_normpdf(s,xExact,sqrt(V));
plot(s,y,"r--")
xtitle("10000 quantiles a 95% sur 200 realisations",..
"Quantile","Frequence");
legend(["Data","Exact","Asymp. Normal"]);
