// Copyright (C) 2013-2014 - Michael Baudin
//
// This file must be used under the terms of the 
// GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// QQ-Plot
// Experience G (Variable Normale s1>s2)
n=10000;
x1=distfun_normrnd(0,1,n,1);
x2=distfun_normrnd(0,2,n,1);
p=linspace(0.1,0.9,100)';
q1=quantile(x1,p);
q2=quantile(x2,p);
//
scf();
subplot(1,2,1)
qqplot(q1,q2,"bo");
plot([q1(25),q1(75)],[q2(25),q2(75)],"r-")
xtitle("n=10 000","Normal(0,1)","Normal(0,2)");
subplot(1,2,2)
histo(x1,[],%t,1);
histo(x2,[],%t,2);
e=gce();
e.children.line_style=2;
xtitle("n=10 000","X","Frequency");
legend(["Normal(0,1)","Normal(0,2)"]);

