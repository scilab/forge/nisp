// Copyright (C) 2013-2014 - Michael Baudin
//
// This file must be used under the terms of the 
// GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Lien entre la loi de Poisson et la loi normale
lambda=[4. 16. 32. 10000.];
ny=2;
nx=2;
scf();
for i=1:nx
    for j=1:ny
        ij=(i-1)*ny + j;
        subplot(ny,nx,ij)
        mu=lambda(ij);
        sigma=sqrt(lambda(ij));
        xmin=max(mu-3*sigma,0);
        xmax=mu+3*sigma;
        x=linspace(xmin,xmax,100);
        xpoi=unique(floor(x));
        y=distfun_poisspdf(xpoi,lambda(ij));
        plot(xpoi,y,"ro");
        y=distfun_normpdf(x,mu,sigma);
        plot(x,y,"b-");
        xtitle("lambda="+string(lambda(ij)));
        legend(["Poisson","Normal"]);
    end
end
