N = 1000;
a = 6;
b = 13;
// Esperance:
m = TODO
// Variance:
v = TODO
[M,V]=distfun_unifstat(TODO)
R = distfun_unifrnd(TODO);
mean(R)
variance(R)
// Graphique
a = 6;
b = 13;
data = distfun_unifrnd(TODO);
scf();
histo(TODO)
x = linspace(a-1,b+1,1000);
y = distfun_unifpdf(TODO);
plot(TODO)
xtitle("Uniform random numbers","X","Density");
legend(["Empirical","PDF"]);
