// Copyright (C) 2013-2014 - Michael Baudin
//
// This file must be used under the terms of the 
// GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Theoreme limite central
a=-4;
b=2;
[M,V]=distfun_unifstat(a,b);
N=10000;
scf();
// 
i=0;
for k=[1,2,4,8]
    i=i+1;
    subplot(2,2,i);
    R=distfun_unifrnd(a,b,N,k);
    S=(sum(R,"c")-k*M)/(sqrt(k*V));
    histo(S,[],%t);
    x=linspace(-3,3,100);
    y=distfun_normpdf(x,0,1);
    plot(x,y,"b-")
    xtitle("k="+string(k),"x","Density")
    legend(["Data","Normal(0,1)"]);
end
