// Copyright (C) 2013-2014 - Michael Baudin
//
// This file must be used under the terms of the 
// GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// QQ-Plot
// Experience D (Groupes francais et europeens)
idataset=23;
[x,txt]=getdata(idataset);
// Retire la colonne #1 (CA)
x(:,1)=[];
n=size(x,"r");
m=size(x,"c");
mu=mean(x,"r"); 
sigma= sqrt(variance(x,"r"));
p=(1:n)'/(n+1);
nbclasses=ceil(log2(n)+1);
strleg=["Nb. Salaries (*1000)","Revenu Net (MF)"];
scf();
k=0;
for i=1:m
    y=distfun_norminv(p,mu(i),sigma(i));
    //
    k=k+1;
    subplot(m,2,k)
    qqplot(y,x(:,i),"b*-");
    plot(x(:,i),x(:,i),"r-")
    strtitle=msprintf("%s",strleg(i));
    xtitle(strtitle,"Normal Quantile","Data Quantile");
    //
    k=k+1;
    subplot(m,2,k)
    histo(x(:,i),[],%t);
    xtitle(strtitle,"X","Frequency");
end
