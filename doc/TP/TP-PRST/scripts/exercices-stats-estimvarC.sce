// Copyright (C) 2013-2014 - Michael Baudin
//
// This file must be used under the terms of the 
// GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Estimation de la variance

// Experience C
// Difference entre la variance biaisee et la 
// variance non biaisee
mu=1;
Nsample=1000;
Sn=[];
Snb=[];
X=distfun_exprnd(mu,Nsample,1);
[M,V] = distfun_expstat(mu)
for n=2:Nsample
    Sn(n)=variance(X(1:n),"r"); // Non Biaisee
    Snb(n)=variance(X(1:n),"r",%nan); // Biaisee
end
h=scf();
plot(1:Nsample,Sn',"r--");
plot(1:Nsample,Snb',"b-");
plot([1,Nsample],[V,V],"k-");
h.children.log_flags="lnn";
xtitle("Difference V. biaisee - non-biaisee",..
"n","Variance empirique");
legend(["V. non biaisee","V. biaisee","Var(X)"],"in_upper_left");
