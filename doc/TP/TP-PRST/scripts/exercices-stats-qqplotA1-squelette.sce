function [y,p] = quantileEmpirique(x)
    n = length(x);
    y = gsort(x,"g","i");
    p = [1:n] / (n+1);
endfunction
n=50; // taille de l'echantillon
mu=1; 
sigma= 3;
x=distfun_normrnd(TODO);
[x,p]=quantileEmpirique(x);
y=distfun_norminv(TODO);
scf();
plot(x,y,"bo");
plot([x(n/4),x(3*n/4)],[y(n/4),y(3*n/4)],"r-");
xtitle("QQ Plot","Data Quantile","Normal Quantile");
