// Copyright (C) 2013-2014 - Michael Baudin
//
// This file must be used under the terms of the 
// GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Loi Normale
// Plot the PDF (with exp)
mu = 5;
sigma = 7;
scf();
x = linspace(mu-3*sigma,mu+3*sigma,1000);
y = exp(-(x-mu)^2/(2*sigma^2))/(sigma*sqrt(2*%pi));
plot(x,y,"r-")
xtitle("Densite de probabilite Normale - mu=5, sigma=7",..
"x","f(x)");

// Plot the PDF (with distfun_normpdf)
mu = 5;
sigma = 7;
scf();
x = linspace(mu-3*sigma,mu+3*sigma,1000);
y = distfun_normpdf ( x , mu , sigma );
plot(x,y,"r-")
xtitle("Densite de probabilite Normale - mu=5, sigma=7",..
"x","f(x)");

// Plot the CDF
mu = 5;
sigma = 7;
scf();
x = linspace(mu-3*sigma,mu+3*sigma,1000);
p = distfun_normcdf ( x , mu , sigma );
plot(x,p,"b-")
xtitle("Fonction Repartition Normale - mu=5, sigma=7",..
"x","$P(X\leq x)$");
