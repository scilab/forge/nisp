// Copyright (C) 2008-2009 - INRIA - Michael Baudin
// Copyright (C) 2009 - Digiteo - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Show a Latin Hypercube Sampling Design

mprintf("Creating a Normale variable ...\n")
vu1 = randvar_new("Normale",1.0,0.5);
mprintf("Creating a Uniform variable ...\n")
vu2 = randvar_new("Uniforme",2.0,3.0);

mprintf("Creating a set of random variables ...\n")
srv = setrandvar_new ( );
setrandvar_addrandvar ( srv , vu1 );
setrandvar_addrandvar ( srv , vu2 );

mprintf("Building Lhs sampling with 1000 experiments ...\n")
setrandvar_buildsample ( srv , "Lhs" , 1000 );

sampling = setrandvar_getsample ( srv );

mprintf("Plotting sampling\n")
scf();
plot(sampling(:,1),sampling(:,2));
my_handle.children.children.children.line_mode = "off";
my_handle.children.children.children.mark_mode = "on";
my_handle.children.children.children.mark_size = 2;
my_handle.children.title.text = "Latin Hypercube Sampling";
my_handle.children.x_label.text = "Variable #1 : Normale,1.0,0.5";
my_handle.children.y_label.text = "Variable #2 : Uniforme,2.0,3.0";

mprintf("Plotting variable #1\n")
scf();
histplot ( 50 , sampling(:,1))
my_handle.children.title.text = "Variable #1 : Normale,1.0,0.5";

mprintf("Plotting variable #2\n")
scf();
histplot ( 50 , sampling(:,2))
my_handle.children.title.text = "Variable #2 : Uniforme,2.0,3.0";

for ivar = 1:2
  m = mean(sampling(:,ivar));
  mprintf("Variable #%d, Mean : %f\n",ivar,m);
  v = variance(sampling(:,ivar));
  mprintf("Variable #%d, Variance : %f\n",ivar,v);
end

randvar_destroy(vu1)
randvar_destroy(vu2)
setrandvar_destroy(srv)

//
// Load this script into the editor
//
filename = 'demo_setrandvar_lhs.sce';
dname = get_absolute_file_path(filename);
editor ( dname + filename );

