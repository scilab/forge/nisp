// Copyright (C) 2009 - CEA - Jean-Marc Martinez
// Copyright (C) 2009-2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU 
// Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Compute the first order sensitivity indices of 
// the ishigami function.

// 1. Define the function
function y = ishigami (x)
    // Returns the output y of the product x1 * x2.
    // Parameters
    // x: a n-by-nx matrix of doubles, where n is the 
    //    number of experiments, and nx=2.
    // y: a n-by-1 matrix of doubles
    a=7.
    b=0.1
    s1=sin(x(:,1))
    s2=sin(x(:,2))
    x34 = x(:,3).^4
    y(:,1) = s1 + a*s2.^2 + b*x34.*s1
endfunction

// 2. Compute exact indices
a=7.;
b=0.1;
V1=(1+b*%pi**4/5)**2/2
V2=a**2/8;
V13=b**2*%pi**8*8/225
VY=V1+V2+V13
S1exact=V1/VY
S2exact=V2/VY
S3exact=0.

// 3. Create the uncertain parameters
rvu1 = randvar_new("Uniforme",-%pi,%pi);
rvu2 = randvar_new("Uniforme",-%pi,%pi);
rvu3 = randvar_new("Uniforme",-%pi,%pi);
srvu = setrandvar_new();
setrandvar_addrandvar ( srvu, rvu1);
setrandvar_addrandvar ( srvu, rvu2);
setrandvar_addrandvar ( srvu, rvu3);

// 4. Create a sample A
n = 10000;
setrandvar_buildsample(srvu,"MonteCarlo",n);
A = setrandvar_getsample(srvu);
// Create a sample B
setrandvar_buildsample(srvu,"MonteCarlo",n);
B = setrandvar_getsample(srvu);

// 5. Perform the experiments in A
ya = ishigami(A);

// 6. Compute the first order sensitivity index for X1
C = B;
C(1:n,1)=A(1:n,1);
yc = ishigami(C);
s1 = corrcoef(ya,yc);
mprintf("S1 : %f (expected = %f)\n", s1, S1exact);

// 7. Compute the first order sensitivity index for X2
C = B;
C(1:n,2)=A(1:n,2);
yc = ishigami(C);
s2 = corrcoef(ya,yc);
mprintf("S2 : %f (expected = %f)\n", s2, S2exact);

// 8. Compute the first order sensitivity index for X3
C = B;
C(1:n,3)=A(1:n,3);
yc = ishigami(C);
s3 = corrcoef(ya,yc);
mprintf("S3 : %f (expected = %f)\n", s3, S3exact);

// Clean-up
nisp_destroyall();
