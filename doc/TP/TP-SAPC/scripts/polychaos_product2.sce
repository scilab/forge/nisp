// Copyright (C) 2008-2011 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU 
// Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html
//

// Test the chaos polynomial decomposition on the 
// product x1*x2 function.

function y = Exemple (x)
  // Returns the output y of the product x1 * x2.
  // Parameters
  // x: a np-by-nx matrix of doubles, where np is 
  //    the number of experiments, and nx=2.
  // y: a np-by-1 matrix of doubles
  y(:,1) = x(:,1) .* x(:,2);
endfunction
// First variable
// Normal
mu1 = 1.5;
sigma1 = 0.5;
// Second variable
// Normal
mu2 = 3.5;
sigma2 = 2.5;

// 1. Two stochastic (normalized) variables
vx1 = randvar_new("Normale");
vx2 = randvar_new("Normale");
// 2. A collection of stoch. variables.
srvx = setrandvar_new();
setrandvar_addrandvar ( srvx,vx1);
setrandvar_addrandvar ( srvx,vx2);
// 3. Two uncertain parameters
vu1 = randvar_new("Normale",mu1,sigma1);
vu2 = randvar_new("Normale",mu2,sigma2);
// 4. A collection of uncertain parameters
srvu = setrandvar_new();
setrandvar_addrandvar ( srvu,vu1);
setrandvar_addrandvar ( srvu,vu2);
// 5. Create the Design Of Experiments
degre = 2;
setrandvar_buildsample(srvx,"Quadrature",degre);
setrandvar_buildsample( srvu , srvx );
// 6. Create the polynomial
ny = 1;
pc = polychaos_new ( srvx , ny );
np = setrandvar_getsize(srvx);
mprintf("Number of experiments : %d\n",np);
polychaos_setsizetarget(pc,np);
// 7. Perform the DOE
inputdata = setrandvar_getsample(srvu);
outputdata = Exemple(inputdata);
polychaos_settarget(pc,outputdata);
// 8. Compute the coefficients of the polynomial expansion
polychaos_setdegree(pc,degre);
polychaos_computeexp(pc,srvx,"Integration");
// 9. Get the sensitivity indices
average = polychaos_getmean(pc);
Ey_expectation= mu1*mu2;
var = polychaos_getvariance(pc);
Vy_expectation = mu2^2*sigma1^2 + mu1^2*sigma2^2 + ..
    sigma1^2*sigma2^2;
mprintf("Mean    = %f (expectation = %f)\n",..
    average,Ey_expectation);
mprintf("Variance    = %f (expectation = %f)\n",..
    var,Vy_expectation);
mprintf("First order sensitivity index\n");
S1 = polychaos_getindexfirst(pc,1);
S1_expectation = ( mu2^2*sigma1^2 ) / Vy_expectation;
mprintf("    Variable X1 = %f (expectation = %f)\n",..
    S1,S1_expectation);
re = abs(S1- S1_expectation)/S1_expectation;
mprintf("        Relative Error = %f\n", re);
S2 = polychaos_getindexfirst(pc,2);
S2_expectation = ( mu1^2*sigma2^2 ) / Vy_expectation;
mprintf("    Variable X2 = %f (expectation = %f)\n",..
    S2,S2_expectation);
re = abs(S2- S2_expectation)/S2_expectation;
mprintf("        Relative Error = %f\n", re);

mprintf("Total sensitivity index\n");
ST1 = polychaos_getindextotal(pc,1);
mprintf("    Variable X1 = %f\n",ST1);
ST2 = polychaos_getindextotal(pc,2);
mprintf("    Variable X2 = %f\n",ST2);
// Clean-up
nisp_destroyall();
