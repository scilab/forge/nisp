// Copyright (C) 2013-2014 - Michael Baudin
// Copyright (C) 2009 - CEA - Jean-Marc Martinez
// Copyright (C) 2009-2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU 
// Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Parameters
sigma1=1;
sigma2=2;
sigma3=3;
sigma4=4;
// 1. Indices SRC exacts
varY=sigma1**2+sigma2**2+sigma3**2+sigma4**2;
SRCexact(1)=sigma1**2/varY;
SRCexact(2)=sigma2**2/varY;
SRCexact(3)=sigma3**2/varY;
SRCexact(4)=sigma4**2/varY;
mprintf("SRC1=%f\n",SRCexact(1))
mprintf("SRC2=%f\n",SRCexact(2))
mprintf("SRC3=%f\n",SRCexact(3))
mprintf("SRC4=%f\n",SRCexact(4))
mprintf("SUM=%f\n",sum(SRCexact))
// 2. Definir la fonction
function y = Exemple (x)
    // Returns the output y of an affine function.
    // Parameters
    // x: a np-by-nx matrix of doubles, where np is 
    //    the number of experiments, and nx=2.
    // y: a np-by-1 matrix of doubles
    y(:) = x(:,1) + x(:,2) + x(:,3) + x(:,4)
endfunction
// 3. Create the random variables rvu1,...,rvu4
rvu1 = randvar_new("Normale",0,sigma1);
rvu2 = randvar_new("Normale",0,sigma2);
rvu3 = randvar_new("Normale",0,sigma3);
rvu4 = randvar_new("Normale",0,sigma4);
// 4. Create the set of random variables srvu
srvu = setrandvar_new();
setrandvar_addrandvar ( srvu, rvu1);
setrandvar_addrandvar ( srvu, rvu2);
setrandvar_addrandvar ( srvu, rvu3);
setrandvar_addrandvar ( srvu, rvu4);
// 5. Create a sampling by a 
// Latin Hypercube Sampling with size 5000.
nbshots = 5000;
setrandvar_buildsample(srvu, "Lhs",nbshots);
sampling = setrandvar_getsample(srvu);
// 6. Perform the experiments.
y=Exemple(sampling);
// 7. Scatter plots : y depending on X_i
scf();
for k=1:4
    subplot(2,2,k)
    plot(sampling(:,k),y,"rx");
    xistr="X"+string(k);
    xtitle("Scatter plot for "+xistr,xistr,"Y");
end
// 8. Estimate the SRC indices
rho1 = corrcoef ( sampling(:,1) , y );
SRC(1) = rho1^2;
mprintf("SRC_1=%.5f (expected=%.5f)\n",SRC(1),SRCexact(1));
//
rho2 = corrcoef ( sampling(:,2) , y );
SRC(2) = rho2^2;
mprintf("SRC_2=%.5f (expected=%.5f)\n",SRC(2),SRCexact(2));
//
rho3 = corrcoef ( sampling(:,3) , y );
SRC(3) = rho3^2;
mprintf("SRC_3=%.5f (expected=%.5f)\n",SRC(3),SRCexact(3));
//
rho4 = corrcoef ( sampling(:,4) , y );
SRC(4) = rho4^2;
mprintf("SRC_4=%.5f (expected=%.5f)\n",SRC(4),SRCexact(4));
// 9. Compute the sum of SRC indices
mprintf("SUM=%.5f (expected=%.5f)\n",sum(SRC),1.);
// 10. Clean-up
nisp_destroyall();
