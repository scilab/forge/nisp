nisp_initseed ( 0 );
rv1 = randvar_new(TODO);
rv2 = randvar_new(TODO);
srv = setrandvar_new();
setrandvar_addrandvar(TODO);
setrandvar_addrandvar(TODO);
np = 1000;
name = "MonteCarlo";
setrandvar_buildsample(TODO);
sampling = setrandvar_getsample(srv);
// Check sampling of random variable #1
m = mean(TODO);
mprintf("Mean of variable #1 : %f (expected : 1.0)\n", m);
// Check sampling of random variable #2
m = mean(TODO);
mprintf("Mean of variable #2 : %f (expected : 1.75)\n", m);
nisp_destroyall();
