// 1. Definir la fonction ishigami
function y = ishigami (x)
  y=TODO
endfunction

// 2. Compute exact indices
a=TODO
b=TODO
EY = TODO
V1=TODO
V2=TODO
V13=TODO
VY=TODO
S1exact=TODO
S2exact=TODO
S3exact=TODO

// 3. Create a group of stochastic variables
nx = 3;
srvx = setrandvar_new(TODO);

// 4. Create a group of uncertain variables
rvu1 = randvar_new(TODO);
rvu2 = randvar_new(TODO);
rvu3 = randvar_new(TODO);
srvu = setrandvar_new();
setrandvar_addrandvar (TODO);
setrandvar_addrandvar (TODO);
setrandvar_addrandvar (TODO);

// 5. Create a Petras sampling
degre = 9;
setrandvar_buildsample(srvx,TODO);
setrandvar_buildsample(srvu,srvx);

// 6. Create the chaos polynomial
pc = polychaos_new(srvx,TODO);
polychaos_setdegree(pc,TODO);

// 7. Perform the experiments
np = setrandvar_getsize(srvu);
polychaos_setsizetarget(pc,np);
inputdata = setrandvar_getsample(srvu);
TODO = ishigami(TODO);
polychaos_settarget(pc,outputdata);

// 8. Compute the coefficients by integration
polychaos_computeexp(pc,srvx,TODO);

// 9. Get the sensitivity indices
average = polychaos_getmean(pc);
var = polychaos_getvariance(pc);

mprintf("Mean        = %f (expected=%f)\n",..
	average,EY);
mprintf("Variance    = %f (expected=%f)\n",var,VY);
disp (" First order sensitivity indice : "); 
disp ( polychaos_getindexfirst (pc )')
disp (" Total sensitivity indices: "); 
disp ( polychaos_getindextotal (pc )')
