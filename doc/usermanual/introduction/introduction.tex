% Copyright (C) 2008-2010 - INRIA - Michael Baudin
% Copyright (C) 2008-2011 - CEA - Jean-Marc Martinez
%
% This file must be used under the terms of the 
% Creative Commons Attribution-ShareAlike 3.0 Unported License :
% http://creativecommons.org/licenses/by-sa/3.0/

%% Introduction
\section{Introduction}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{The OPUS project}

The goal of this toolbox is to provide a tool to manage uncertainties
in simulated models. This toolbox is based on the NISP library, where 
NISP stands for "Non-Intrusive Spectral Projection".
This work has been realized in the context of the OPUS project, 
\begin{center}
\url{http://opus-project.fr}
\end{center}
"Open-Source Platform for Uncertainty treatments in Simulation", 
funded by ANR, the french "Agence Nationale pour la Recherche":
\begin{center}
\url{http://www.agence-nationale-recherche.fr}
\end{center}
The toolbox is released under the Lesser General Public Licence 
(LGPL), as all components of the OPUS project.

This module was presented in the "42\`emes Journ\'ees de Statistique, du 
24 au 28 mai 2010" \cite{NISPScilab-JDS2010}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{The NISP library}

The NISP library is based on a set of 3 C++ classes so that 
it provides an object-oriented framework for uncertainty analysis.
The Scilab toolbox provides a pseudo-object oriented interface to 
this library, so that the two approaches are consistent.
The NISP library is release under the LGPL licence.

The NISP library provides three tools, which are detailed below.
\begin{itemize}
\item The "randvar" class allows to manage random variables, specified 
by their distribution law and their parameters. Once a random variable 
is created, one can generate random numbers from the associated 
law.
\item The "setrandvar" class allows to manage a collection of random 
variables. This collection is associated with a sampling method, such as MonteCarlo,
Sobol, Quadrature, etc... It is possible to build the sample and to get 
it back so that the experiments can be performed.
\item The "polychaos" class allows to manage a polynomial representation of the 
simulated model. One such object must be associated with a set of experiments
which have been performed. This set may be read from a data file. The 
object is linked with a collection of random variables. Then the 
coefficients of the polynomial can be computed by integration (quadrature). 
Once done, the mean, the variance and the Sobol indices can be directly 
computed from the coefficients.
\end{itemize}

The figure \ref{NISP-methodology} presents the NISP methodology.
The process requires that the user has a numerical solver, which 
has the form $Y=f(X)$, where $X$ are input uncertain parameters
and $Y$ are output random variables.
The method is based on the following steps.
\begin{itemize}
\item We begin by defining normalized random variables $\bxi$. For example, 
we may use a random variables in the interval $[0,1]$ or 
a Normal random variable with mean 0 and variance 1.
This choice allows to define the basis for the polynomial chaos, denoted by 
$\{\Psi_k\}_{k\geq 0}$. Depending on the type of random variable, the 
polynomials $\{\Psi_k\}_{k\geq 0}$ are based on Hermite, Legendre or Laguerre 
polynomials.
\item We can now define a Design Of Experiments (DOE) and, with random variable transformations rules, 
we get the physical uncertain parameters $X$. Several types of DOE are available:
Monte-Carlo, Latin Hypercube Sampling, etc... If $N$ experiments are required,
the DOE define the collection of normalized random variables $\{\bxi_i\}_{i=1,N}$.
Transformation rules allows to compute the uncertain parameters 
$\{X_i\}_{i=1,N}$, which are the input of the numerical solver $f$.
\item We can now perform the simulations, that is compute the collection of 
outputs $\{Y_i\}_{i=1,N}$ where $Y_i = f(X_i)$.
\item The variables $Y$ are then projected on the polynomial basis and the coefficients
$y_k$ are computed by integration or regression.
\end{itemize}

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=0.5\textwidth]{introduction/nisp-method.pdf}
\end{center}
\caption{The NISP methodology}
\label{NISP-methodology}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{The NISP module}

The NISP toolbox is available under the following operating systems:
\begin{itemize}
\item Linux 32 bits,
\item Linux 64 bits,
\item Windows 32 bits,
\item Mac OS X.
\end{itemize}

The following list presents the features provided by the NISP toolbox.
\begin{itemize}
\item Manage various types of random variables:
  \begin{itemize}
  \item uniform,
  \item normal,
  \item exponential,
  \item log-normal.
  \end{itemize}
\item Generate random numbers from a given random variable,
\item Transform an outcome from a given random variable into another,
\item Manage various Design of Experiments for sets of random variables,
  \begin{itemize}
  \item Monte-Carlo,
  \item Sobol,
  \item Latin Hypercube Sampling,
  \item various samplings based on Smolyak designs.
  \end{itemize}
\item Manage polynomial chaos expansion and get specific outputs, including
  \begin{itemize}
  \item mean,
  \item variance,
  \item quantile,
  \item correlation, 
  \item etc...
  \end{itemize}
\item Generate the C source code which computes the output of the polynomial chaos expansion.
\end{itemize}

This User's Manual completes the online help provided with the toolbox, 
but does not replace it. The goal of this document is to provide both a 
global overview of the toolbox and to give some details about 
its implementation. The detailed calling sequence of each function is 
provided by the online help and will not be reproduced in this 
document.
The inline help is presented in the figure \ref{NISP-inline-help}.

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=14cm]{introduction/inline-help.png}
\end{center}
\caption{The NISP inline help.}
\label{NISP-inline-help}
\end{figure}

For example, in order to access to the help associated with the 
\scifun{randvar} class, we type the following statements in the 
Scilab console.
\lstset{language=scilabscript}
\begin{lstlisting}
help randvar
\end{lstlisting}
The previous statements opens the Help Browser and displays the 
helps page presented in figure 

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=0.5\textwidth]{introduction/help-randvar.png}
\end{center}
\caption{The online help of the randvar function.}
\label{NISP-helprandvar}
\end{figure}

Several demonstration scripts are provided with the toolbox and are 
presented in the figure \ref{NISP-demos}. These demonstrations 
are available under the "?" question mark in the menu of the 
Scilab console.

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=0.5\textwidth]{introduction/nisp_demos.png}
\end{center}
\caption{Demonstrations provided with the NISP toolbox.}
\label{NISP-demos}
\end{figure}

Finally, the unit tests provided with the toolbox cover all the features of the 
toolbox. When we want to know how to use a particular feature and do not 
find the information, we can search in the unit tests which often provide the 
answer. 
See in the section \ref{sec-archi} for details on the internal structure of the toolbox.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Installing the toolbox from ATOMS}

There are two possible ways of installing the NISP toolbox 
in Scilab:
\begin{itemize}
\item use the ATOMS system and get a binary version of the toolbox,
\item build the toolbox from the sources.
\end{itemize}

In this section, we present the method to install NISP from ATOMS. 
The installation of the toolbox from the sources in presented in appendix, 
in section \ref{sec-installsrc}.

The ATOMS component is the Scilab tool which allows to search,
download, install and load toolboxes. ATOMS comes with Scilab v5.2. 
The Scilab-NISP toolbox has been packaged and is provided mainly by the ATOMS component.
The toolbox is provided in binary form, depending on the user's 
operating system. 
The Scilab-NISP toolbox is available for the following platforms:
\begin{itemize}
\item Windows 32 bits, 64 bits,
\item Linux 32 bits, 64 bits,
\item Mac OS X.
\end{itemize}
The ATOMS component allows to use a toolbox based on compiled 
source code, without having a compiler installed in the system.

Installing the Scilab-NISP toolbox from ATOMS requires to 
run the \scifun{atomsInstall} function, then to restart Scilab.

In the following Scilab session, we use the \scifun{atomsInstall()}
function to download and install the binary version of the toolbox 
corresponding to the current operating system. 
\lstset{language=scilabsession}
\begin{lstlisting}
-->atomsInstall ( "NISP" )
ans  =
!NISP  2.1  allusers  D:\Programs\SC3623~1\contrib\NISP\2.1  I  !
\end{lstlisting}
The \scivar{"allusers"} option of the \scifun{atomsInstall} function can be 
used to install the toolbox for all the users of this computer.
Then we restart Scilab, and the NISP toolbox is automaticall loaded.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Configuration functions}

In this section, we present functions which allow to configure the NISP
toolbox. 

The \scifun{nisp\_*} functions allows to configure the global behaviour
of the toolbox. These functions allows to startup and shutdown the toolbox
and initialize the seed of the random number generator.
They are presented in the figure \ref{fig-nisp-methodsoutline}.

\begin{figure}[h]
\begin{center}
\begin{tabular}{|ll|}
\hline
\textbf{Public functions} &\\
\hline
\scifun{level=nisp\_verboselevelget()} & Returns the current verbose level. \\
\scifun{nisp\_verboselevelset(level)} & Sets the value of the verbose level. \\
\scifun{nisp\_initseed(seed)} & Sets the seed of the uniform \\
                              & NISP random number generator. \\
\scifun{nisp\_destroyall} & Destroy all current NISP objects. \\
\scifun{nisp\_getpath} & Returns the path to the current module. \\
\scifun{nisp\_printall} & Prints all current NISP objects. \\
\hline
\textbf{Private functions} &\\
\hline
\scifun{nisp\_startup()} & Starts up the NISP toolbox. \\
\scifun{nisp\_shutdown()} & Shuts down the NISP toolbox. \\
\hline
\end{tabular}
\end{center}
\caption{Outline of the configuration methods.}
\label{fig-nisp-methodsoutline}
\end{figure}

The \scifun{nisp\_initseed ( seed )} is useful when we want to 
have reproductible results. 
It allows to set the seed of the generator at 
a particular value, so that the sequence of uniform pseudo-random numbers is 
deterministic. 
When the toolbox is started up, the seed is automatically 
set to 0, which allows to get the same results from session to session.

The user has no need to explicitely call the \scifun{nisp\_startup()}
and \scifun{nisp\_shutdown()} functions. Indeed, these functions are 
called automatically by the \emph{etc/NISP.start} and \emph{etc/NISP.quit}
scripts, located in the toolbox directory structure.
See the section \ref{sec-archi} for details on this topic.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Thanks}

Many thanks to Allan Cornet.


