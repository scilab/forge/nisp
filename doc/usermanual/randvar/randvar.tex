% Copyright (C) 2012-2013 - Michael Baudin
% Copyright (C) 2008-2010 - INRIA - Michael Baudin
%
% This file must be used under the terms of the 
% Creative Commons Attribution-ShareAlike 3.0 Unported License :
% http://creativecommons.org/licenses/by-sa/3.0/

\section{The \scifun{randvar} class}

In this section, we present the \scifun{randvar} class, which allows 
to define a random variable, and to generate random numbers from a 
given distribution function.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{The distribution functions}

In this section, we present the distribution functions provided by the 
\scifun{randvar} class. 
We especially present the Log-normal distribution function.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Overview}

The table \ref{fig-randvar-distributionsfunctions} gives the list of 
distribution functions which are available with the \scifun{randvar} class \cite{pelat2006}.

\begin{figure}
\begin{center}
\begin{footnotesize}
\begin{tabular}{|l|l|l|l|}
\hline
Name & $f(x)$ & $E(X)$ & $V(X)$ \\
\hline
"Normale" & $\frac{1}{2\sigma \sqrt{2 \pi}} \exp \left(-\frac{1}{2} \frac{(x-\mu)^2}{\sigma^2}\right)$ & $\mu$ & $\sigma^2$\\
\hline
"Uniforme" & $
\left\{\begin{array}{ll}\frac{1}{b-a}, &\textrm{ if } x\in[a,b] \\
0 &\textrm{ if } x\notin [a,b]\end{array}\right.
$ & $\frac{b+a}{2}$ & $\frac{(b-a)^2}{12}$\\
\hline
"Exponentielle" & $
\left\{\begin{array}{ll}\lambda \exp\left(-\lambda x\right), &\textrm{ if } x>0 \\
0 &\textrm{ if } x\leq 0\end{array}\right.$ 
& $\frac{1}{a}$ 
& $\frac{1}{a^2}$\\
\hline
"LogNormale" & $
\left\{\begin{array}{ll}\frac{1}{\sigma x \sqrt{2 \pi}} \exp\left(-\frac{1}{2} \frac{(\ln(x) - \mu)^2}{\sigma^2}\right), 
&\textrm{ if } x>0 \\
0 &\textrm{ if } x\leq 0\end{array}\right.$ 
& $\exp\left(\mu+\frac{\sigma^2}{2}\right)$ 
& $\left(\exp(\sigma^2)-1\right)\exp\left(2\mu+\sigma^2\right)$\\
\hline
"LogUniforme" & $
\left\{
\begin{array}{ll}
\frac{1}{x} \frac{1}{b-a}, &\textrm{ if } x\in[\exp(a),\exp(b)] \\
0 &\textrm{ otherwise.}
\end{array}
\right.
$ & $\frac{\exp(b)-\exp(a)}{b-a}$ & $\frac{1}{2} \frac{\exp(b)^2-\exp(a)^2}{b-a} - E(x)^2$ \\
\hline
\end{tabular}
\end{footnotesize}
\end{center}
\caption{Distributions functions of the \scifun{randvar} class. -- The expected value is denoted by $E(X)$ and the 
variance is denoted by $V(X)$.}
\label{fig-randvar-distributionsfunctions}
\end{figure}

Each distribution function has zero, one or two parameters.
One random variable can be specified by giving explicitly 
its parameters or by using default parameters. 
The parameters for all distribution functions are presented in the 
figure \ref{fig-randvar-distributionsdefault}, 
which also presents the conditions which must be satisfied by 
the parameters.

\begin{figure}
\begin{center}
\begin{tabular}{|l|l|l|l|l|}
\hline
Name & Parameter \#1 : $a$ & Parameter \#2 : $b$ & Conditions\\
\hline
"Normale" & $\mu = 0$ & $\sigma = 1$ & $\sigma>0$\\
\hline
"Uniforme" & $a=0$ & $b=1$ & $a< b$\\
\hline
"Exponentielle" & $a = 1$ & - & - \\
\hline
"LogNormale" & $\mu=0$ & $\sigma=1$ & $\sigma>0$\\
\hline
"LogUniforme" & $a=0$ & $b=1$ & $a<b$\\
\hline
\end{tabular}
\end{center}
\caption{Default parameters for distributions functions.}
\label{fig-randvar-distributionsdefault}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{The Log-Normal distribution}

A log-normal distributed random variable 
is so that its logarithm is normally distributed. 
In other words, if $X$ is a random variable with a normal 
distribution, then $Y = \exp(X)$ has a log-normal distribution.

The "LogNormale" law is defined by the 
expected value $\mu$ and the standard deviation $\sigma$ of the underlying Normal 
random variable. 
In other words, the random variable $\log(X)$ has mean $\mu$ 
and variance $\sigma^2$.
The expected value and the variance of the Log Normal law are given by 
\begin{eqnarray}
E(X) &=& \exp\left(\mu + \frac{1}{2} \sigma^2\right)\\
V(X) &=& \left(\exp(\sigma^2) - 1\right)\exp\left(2\mu + \sigma^2\right).
\end{eqnarray}

We can invert the previous equations and get
\begin{eqnarray}
\mu &=& \ln(E(X)) - \frac{1}{2} \ln \left( 1 + \frac{V(X)}{E(X)^2} \right)\\
\sigma^2 &=& \ln \left( 1 + \frac{V(X)}{E(X)^2}\right).
\end{eqnarray}
In particular, the expected value $\mu$ of with the Normal random variable satisfies the equation 
\begin{eqnarray}
\mu = \ln(E(X)) - \sigma^2.
\end{eqnarray}

\emph{Caution !} These are the parameters for NISP v2.5. 
Earlier versions of the toolbox used other parameters. 
See the changelog.txt file in your toolbox to see how to update 
your scripts.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{The Log-Uniform distribution}

A log-uniform distributed random variable 
is so that its logarithm has a uniform distribution. 
In other words, if $X$ is a random variable with a uniform 
distribution, then $Y = \exp(X)$ has a log-uniform distribution.

The "LogUniforme" law is defined by the 
minimum $a$ and the maximum $b$ of the underlying Uniform  
random variable. 
In other words, the random variable $\log(X)$ has minimum $a$ 
and maximum $b$.

If X is a LogUniforme random variable with minimum $A$ and maximum 
$B$, then:
\begin{eqnarray}
a&=&\log(A) \\
b&=&\log(B)
\end{eqnarray}

\emph{Caution !} These are the parameters for NISP v2.5. 
Earlier versions of the toolbox used other parameters. 
See the changelog.txt file in your toolbox to see how to update 
your scripts.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsubsection{Uniform random number generation}

In this section, we present the generation of uniform random numbers.

Since v2.5, the library uses the Mersenne-Twister pseudo-random number 
generator \cite{Matsumoto1998}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Methods}

In this section, we give an overview of the methods which are available in
the \scifun{randvar} class. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsubsection{Overview}

The figure \ref{fig-randvar-methodsoutline} presents the methods available in the \scifun{randvar} class.
The inline help contains the detailed calling sequence for each function and will 
not be repeated here.

\begin{figure}[htbp]
\begin{center}
\begin{tabular}{|l|}
\hline
\textbf{Constructors}\\
\scifun{rv = randvar\_new ( type [, options])} \\
\hline
\textbf{Methods}\\
\scifun{value = randvar\_getvalue ( rv [, options] )} \\
\scifun{randvar\_getlog ( rv )} \\
\hline
\textbf{Destructor}\\
\scifun{randvar\_destroy ( rv )} \\
\hline
\textbf{Static methods}\\
\scifun{rvlist = randvar\_tokens ()} \\
\scifun{nbrv = randvar\_size ()} \\
\hline
\end{tabular}
\end{center}
\caption{Outline of the methods of the \scifun{randvar} class.}
\label{fig-randvar-methodsoutline}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsubsection{The Oriented-Object system}
\label{section-nisp-oo-system}

In this section, we present the token system which allows to 
emulate an oriented-object programming with Scilab. We also 
present the naming convention we used to create the names 
of the functions.

The \scifun{randvar} class provides the following functions.
\begin{itemize}
\item The constructor function \scifun{randvar\_new} allows to create 
a new random variable and returns a \emph{token} \scivar{rv}.
\item The method \scifun{randvar\_getvalue} takes the token \scivar{rv}
as its first argument. In fact, all methods takes as their first argument the 
object on which they apply.
\item The destructor \scifun{randvar\_destroy} allows to delete 
the current object from the memory of the library.
\item The static methods \scifun{randvar\_tokens} and 
\scifun{randvar\_size} allows to quiery the current object
which are in use. More specifically, the \scifun{randvar\_size}
function returns the number of current randvar objects and the 
\scifun{randvar\_tokens} returns the list of current randvar
objects.
\end{itemize}

In the following Scilab sessions, we present these ideas 
with practical uses of the toolbox.

Assume that we start Scilab and that the toolbox is automatically
loaded. At startup, there are no objects, so that the 
\scifun{randvar\_size} function returns 0 and the 
\scifun{randvar\_tokens} function returns an empty matrix.
\lstset{language=scilabscript}
\begin{lstlisting}
-->nb = randvar_size()
 nb  =
    0.  
-->tokenmatrix = randvar_tokens()
 tokenmatrix  =
     []
\end{lstlisting}

We now create 3 new random variables, based on the Uniform distribution 
function. We store the tokens in the variables \scivar{vu1}, \scivar{vu2}
and \scivar{vu3}. These variables are regular Scilab double precision floating
point numbers. Each value is a token which represents a random variable 
stored in the toolbox memory space.
\lstset{language=scilabscript}
\begin{lstlisting}
-->vu1 = randvar_new("Uniforme")
 vu1  =
    0.  
-->vu2 = randvar_new("Uniforme")
 vu2  =
    1.  
-->vu3 = randvar_new("Uniforme")
 vu3  =
    2.  
\end{lstlisting}

There are now 3 objects in current use, as indicated by the 
following statements. The \scivar{tokenmatrix} is a row matrix
containing regular double precision floating point numbers.
\lstset{language=scilabscript}
\begin{lstlisting}
-->nb = randvar_size()
 nb  =
    3.  
-->tokenmatrix = randvar_tokens()
 tokenmatrix  =
    0.    1.    2.  
\end{lstlisting}

We assume that we have now made our job with the random variables, so that it 
is time to destroy the random variables. We call the 
\scifun{randvar\_destroy} functions, which destroys the variables.
\lstset{language=scilabscript}
\begin{lstlisting}
-->randvar_destroy(vu1);
-->randvar_destroy(vu2);
-->randvar_destroy(vu3);
\end{lstlisting}
We can finally check that there are no random variables left in the 
memory space.
\lstset{language=scilabscript}
\begin{lstlisting}
-->nb = randvar_size()
 nb  =
    0.  
-->tokenmatrix = randvar_tokens()
 tokenmatrix  =
     []
\end{lstlisting}

Scilab is a wonderful tool to experiment algorithms
and make simulations. 
It happens sometimes that we are 
managing many variables at the same time and it may happen
that, at some point, we are lost. 
The static methods provides 
tools to be able to recover from such a situation 
without closing our Scilab session.

In the following session, we create two random variables.
\lstset{language=scilabscript}
\begin{lstlisting}
-->vu1 = randvar_new("Uniforme")
 vu1  =
    3.  
-->vu2 = randvar_new("Uniforme")
 vu2  =
    4.  
\end{lstlisting}
Assume now that we have lost the token 
associated with the variable \scivar{vu2}.
We can easily simulate this situation, by using the 
\scifun{clear} function, which destroys a variable from
Scilab's memory space.
\lstset{language=scilabscript}
\begin{lstlisting}
-->clear vu2
-->randvar_getvalue(vu2)
                   !--error 4 
Undefined variable: vu2
\end{lstlisting}
It is now impossible to generate values from the variable \scivar{vu2}.
Moreover, it may be difficult to know exactly what went wrong 
and what exact variable is lost.
At any time, we can use the \scifun{randvar\_tokens} function
in order to get the list of current variables. Deleting these variables 
allows to clean the memory space properly, without memory loss.
\lstset{language=scilabscript}
\begin{lstlisting}
-->randvar_tokens()
 ans  =
    3.    4.  
-->randvar_destroy(3)
 ans  =
    3.  
-->randvar_destroy(4)
 ans  =
    4.  
-->randvar_tokens()
 ans  =
     []
\end{lstlisting}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Examples}

In this section, we present to examples of use of the 
\scifun{randvar} class. The first example presents the simulation
of a Normal random variable and the generation of 1000 random variables.
The second example presents the transformation of a Uniform outcome 
into a LogUniform outcome.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsubsection{A sample session}

We present a sample Scilab session, where the 
\scifun{randvar} class is used to generate samples from the Normale law.

In the following Scilab session, we create a Normale random variable
and compute samples from this law. 
The \scifun{nisp\_initseed}
function is used to initialize the seed for the uniform random variable 
generator. 
Then we use the \scifun{randvar\_new} function to create 
a new random variable from the Normale law with mean 1. and standard 
deviation 0.5. 
The main loop allows to compute 1000 samples 
from this law, based on calls to the \scifun{randvar\_getvalue} 
function. 
Once the samples are computed, we use the Scilab function \scifun{mean}
to check that the mean is close to 1 (which is the expected value of the 
Normale law, when the number of samples is infinite). 
Finally, we use the \scifun{randvar\_destroy} function
to destroy our random variable.
Once done, we plot the empirical distribution function of this sample, 
with 50 classes.
\lstset{language=scilabscript}
\begin{lstlisting}
nisp_initseed ( 0 );
mu = 1.0;
sigma = 0.5;
rv = randvar_new("Normale" , mu , sigma);
nbshots = 1000;
values = zeros(nbshots);
for i=1:nbshots
  values(i) = randvar_getvalue(rv);
end
mymean = mean (values);
mysigma = st_deviation(values);
myvariance = variance (values);
mprintf("Mean : %f (expected = %f)\n", mymean, mu);
mprintf("Std. dev. : %f (expected = %f)\n", mysigma, sigma);
mprintf("Variance : %f (expected = %f)\n", myvariance, sigma^2);
randvar_destroy(rv);
histplot(50,values)
xtitle("Histogram of X","X","P(x)")
\end{lstlisting}

The previous script produces the following output.
\lstset{language=scilabscript}
\begin{lstlisting}
Mean : 0.988194 (expected = 1.000000)
Std. dev. : 0.505186 (expected = 0.500000)
Variance : 0.255213 (expected = 0.250000)
\end{lstlisting}

The previous script also produces the figure \ref{fig-randvar-demonormalpdf}.

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=14cm]{randvar/demorandvar_normalpdf.pdf}
\end{center}
\caption{The histogram of a Normal random variable with 1000 samples.}
\label{fig-randvar-demonormalpdf}
\end{figure}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsubsection{Variable transformations}

In this section, we present the transformation of a random variable 
with given distribution into a variable with another distribution. 
Then we present some of the many the transformations which are provided by the 
library.

Any outcome from a random variable $x$ can be transformed into the 
outcome of another variable $y$. 
This can be done by computing $u=F_X(x)$, where $F_X$ is 
the cumulated distribution function of $X$. 
This transforms the random variable $X$ into the random variable $U$, 
which has a uniform distribution in $[0,1]$. 
Then we invert the CDF, by computing $y=F_Y^{-1}(u)$, where 
$F_Y$ is the cumulated distribution function of $Y$. 

We now present some examples of the function
\scifun{randvar\_getvalue ( rv , rv2 , value2 )}, which performs 
this transformation. 
The statement 
\lstset{language=scilabscript}
\begin{lstlisting}
value = randvar_getvalue ( rv , rv2 , value2 )
\end{lstlisting}
returns a random value from the distribution function
of the random variable \scivar{rv} by transformation of \scivar{value2} from the distribution function 
of random variable \scivar{rv2}.

In the following session, we transform a uniform random variable sample 
into a LogUniform variable sample. 
We begin to create a random variable 
\scivar{rv} from a LogUniform law and parameters $a=10$, $b=20$. 
Then we create a second random variable \scivar{rv2} from 
a Uniforme law and parameters $a=2$, $b=3$.
The main loop is based on the transformation of a sample computed 
from \scivar{rv2} into a sample from \scivar{rv}.
The \scifun{mean} allows to check that the transformed 
samples have an mean value which corresponds to the random 
variable \scivar{rv}.

\lstset{language=scilabscript}
\begin{lstlisting}
nisp_initseed ( 0 );
a = 10.0;
b = 20.0;
rv = randvar_new ( "LogUniforme" , a , b );
rv2 = randvar_new ( "Uniforme" , 2 , 3 );
nbshots = 1000;
valuesLou = zeros(nbshots);
for i=1:nbshots
  valuesUni(i) = randvar_getvalue( rv2 );
  valuesLou(i) = randvar_getvalue( rv , rv2 , valuesUni(i) );
end
computed = mean (valuesLou);
mu = (b-a)/(log(b)-log(a));
expected = mu; 
mprintf("Expectation=%.5f (expected=%.5f)\n",computed,expected);
//
scf();
histplot(50,valuesUni);
xtitle("Empirical histogram - Uniform variable","X","P(X)");
scf();
histplot(50,valuesLou);
xtitle("Empirical histogram - Log-Uniform variable","X","P(X)");
randvar_destroy(rv);
randvar_destroy(rv2);
\end{lstlisting}

The previous script produces the following output.
\lstset{language=scilabscript}
\begin{lstlisting}
Expectation=14.63075 (expected=14.42695)
\end{lstlisting}

The previous script also produces the figures \ref{fig-randvar-uniform} and 
\ref{fig-randvar-loguniform}.

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=14cm]{randvar/randvar_uni.pdf}
\end{center}
\caption{The histogram of a Uniform random variable with 1000 samples.}
\label{fig-randvar-uniform}
\end{figure}

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=14cm]{randvar/randvar_loguni.pdf}
\end{center}
\caption{The histogram of a Log-Uniform random variable with 1000 samples.}
\label{fig-randvar-loguniform}
\end{figure}

