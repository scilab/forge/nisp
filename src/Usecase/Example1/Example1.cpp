/*!
  \file   Example1.C
  \brief  Polynome de Chaos - Quadrature
*/
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include "nisp_pc.h" // classes Nisp

using namespace std;
// Modele mathematique : y = x1 * x2
void modele(double x[], double y[]) {
  y[0]=x[0]*x[1];
}

/*!
  \fn void Example1(int degre)
  \brief Polynome de Chaos - Quadrature - Indices de Sobol
  \param degre : degre du polyn�me
*/
void Example1(int degre) {
  // Definition des variables physiques incertaines
  SetRandomVariable * gu = new SetRandomVariable();            // groupe de variables incertaines
  RandomVariable * rv1 = new RandomVariable("Normale",1.,0.5);
  RandomVariable * rv2 = new RandomVariable("Uniforme",1.,2.5);
  gu->AddRandomVariable(rv1);  // normale, moyenne = 1., ecart type = 0.5
  gu->AddRandomVariable(rv2); // uniforme, min = 1, max = 2.5

  // Definition des variables stochastiques : variable aleatoire standardisee
  SetRandomVariable * gx = new SetRandomVariable();             // groupe de variables stochastiques
  RandomVariable * sv1 = new RandomVariable("Normale");
  RandomVariable * sv2 = new RandomVariable("Uniforme");
  gx->AddRandomVariable(sv1);         // normale, moyenne 0., ecart-type 1
  gx->AddRandomVariable(sv2);        // uniforme, min = 0, max = 1
  
  // Calcul du plan d'experiences numeriques par Quadrature
  gx->BuildSample("Quadrature",degre);    // calcul des points et poids par quadrature
  gu->BuildSample(gx);                    // calcul du plan d'experiences numeriques

  // Polynome de chaos
  PolynomialChaos * pc = new PolynomialChaos(gx); // hermite x legendre
  int nx = pc->GetDimensionInput();  // nx=2
  int ny = pc->GetDimensionOutput(); // ny=1 (valeur par defaut lors de la construction dun PC)

  // Realisation du plan d'experiences numeriques
  int np=gu->GetSize();  // taille du plan d'experiences
  pc->SetSizeTarget(np); // allocation memoire 
  double * input = new double [nx];
  double * output = new double [ny];
  for(int k=1;k<=np;k++) {
    for(int i=0;i<nx;i++) input[i]=gu->GetSample(k,i+1);  // on recupere les entrees
    modele(input,output);                               // appel au modele numerique
    for(int j=0;j<ny;j++) pc->SetTarget(k,j+1,output[j]); // on transmet au PC les sorties calculees
  }

  // Calcul du chaos polynomial
  pc->SetDegree(degre);
  pc->ComputeChaosExpansion(gx,"Integration");

  // Edition des resultats
  cout << "Mean     = " << pc->GetMean() << endl;
  cout << "Variance = " << pc->GetVariance() << endl;
  cout << "Indice de sensibilite du 1er ordre"<<endl;
  cout << "    Variable X1 = " << pc->GetIndiceFirstOrder(1)<<endl;
  cout << "    Variable X2 = " << pc->GetIndiceFirstOrder(2)<<endl;
  cout << "Indice de sensibilite Totale"<<endl;
  cout << "    Variable X1 = " << pc->GetIndiceTotalOrder(1)<<endl;
  cout << "    Variable X2 = " << pc->GetIndiceTotalOrder(2)<<endl;
}

int main(int argc, char *argv[]) {
  if(argc != 2) {
    cout << "Erreur !! la commande est de la forme << nisp degre >>" << endl;
    cout << "degre : degre du polynome" << endl;
  }
  else {
    int degre = atoi(argv[1]);
    Example1(degre);
  }
}
