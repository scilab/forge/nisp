/*!
  \file   CDFIshigami.C
  \brief  CDF du polynome
*/
#include <iostream>
#include <fstream>
#include <cmath>     // pour disposer de la macro M_PI = 3.14159...
#include "nisp_pc.h" // les classes Nisp

using namespace std;

/*!
  \fn void CDFIshigami(int np)
  \brief CDF du PC
  \param np : nombre de simulations
*/
void CDFIshigami(int np) {
  // Initialisation de la "graine" aleatoire
  void InitSeed(int sp);
  InitSeed(0);

  PolynomialChaos * pc = new PolynomialChaos((char *) "NispIshigami.dat");
  int order=1;
  pc->BuildSample("Lhs",np,order); // order = 1 on ordonne l'echnatillon 

  // Quantile a 95%
  cout << pc->GetQuantile((double) 0.95) << endl;
  cout << pc->GetInvQuantile(pc->GetQuantile((double) 0.95)) << endl;

  // Visualisation de la cdf
  ofstream sortie((char *) "cdf.dat",ios::out);
  double cdf;
  for(int k=1;k<=pc->gpx->np;k++) {
    cdf=(double) k / (double) np;
    sortie << pc->GetSample(k) << " " << cdf << endl;
  }
  sortie.close();
}

int main(int argc, char *argv[]) {
  if(argc != 2) {
    cout << "Erreur !! la commande est de la forme << nisp np >>" << endl;
    cout << "np : nombre de simulations" << endl;
  }
  else {
    int np = atoi(argv[1]);
    CDFIshigami(np);
  }
}
