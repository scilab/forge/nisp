// Copyright (C) 2012 - Michael Baudin
// Copyright (C) 2009 - INRIA - Michael Baudin
// Copyright (C) 2009 - Digiteo - Michael Baudin
/*!
\file   nisp_va.h
\brief  Class of random variables
*/
#ifndef _NISP_VA_H_
#define _NISP_VA_H_

#ifdef _MSC_VER
#if LIBNISP_EXPORTS 
#define NISP_IMPORTEXPORT __declspec (dllexport)
#else
#define NISP_IMPORTEXPORT __declspec (dllimport)
#endif
#else
#define NISP_IMPORTEXPORT
#endif


#include <string>
#include <vector>
using namespace std;

#undef __BEGIN_DECLS
#undef __END_DECLS
#ifdef __cplusplus
# define __BEGIN_DECLS extern "C" {
# define __END_DECLS }
#else
# define __BEGIN_DECLS /* empty */
# define __END_DECLS /* empty */
#endif

__BEGIN_DECLS

#ifdef _MSC_VER
#pragma warning( disable : 4251 )
#endif


	//! Class of random variables
class NISP_IMPORTEXPORT RandomVariable {
public :
	//! Law of the density probability
	string type;
	//! First parametre of the law
	double a;
	//! Second parametre of the law
	double b;
public:
	//! Constructor from the law
	RandomVariable(string type);
	RandomVariable(char * type);
	//! Constructor from a law with one parameter
	// TODO : remove the string, take an integer as argument, define the possible integers as NISP_LAW_NORMAL, NISP_LAW_EXP, etc...
	RandomVariable(string type, double a);
	RandomVariable(char * name, double x);
	//! Constructor from a law with two parameters
	RandomVariable(string type, double a, double b);
	RandomVariable(char * name, double x, double y);
	//! Destructor
	~RandomVariable();
	//! Sample of the random variable
	double GetValue();
	//TODO : get an array of random variables : double GetValue ( int n , double * value );
	//! Sample from a sample x of another RandomVariable *va
	double GetValue(RandomVariable *va, double x);
	//! Information : type of the law with its parameters
	void GetLog();
	// Check the parameters of the current random variable
	void CheckParameters();
	// Convert the p-value z associated the R.V. mere into a sample of the current R.V.,
	// by inverting the CDF.
	double pdfChange ( RandomVariable *mere, double x);
	// Convert the p-value z into a sample with this probability,
	// by inverting the CDF.
	// z must be in the range [0,1].
	// If not, an error is generated.
	double pdfChange ( double z);
private:
	// Setup the CDF associated with the current R.V.
	void AssignFunction();
	// Set the default parameters to the current random variable
	void SetDefaultParameters();
};

__END_DECLS

#endif /* _NISP_VA_H_ */
