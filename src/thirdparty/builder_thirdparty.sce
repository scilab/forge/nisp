// Copyright (C) 2012 - 2013 - Michael Baudin
// Copyright (C) 2008 - INRIA   - Michael Baudin
// Copyright (C) 2009 - DIGITEO - Pierre MARECHAL
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function nispBuildThirdparty()
    cpp_dir=get_absolute_file_path("builder_thirdparty.sce");
    src_path="c";
    linknames=["thirdparty"];
    files = [
    "mt.cpp"
    "utils.cpp"
    "blas1_d.cpp"
    "dcdflib.cpp"
    "linpack_d.cpp"
    "sobol.cpp"
    "smolyak.cpp"
    ];
    ldflags = "";
    if ( getos() == "Windows" ) then
        cflags = "-DWIN32"+..
		" -DLIBTHIRDPARTY_EXPORTS" +..
		" -I""..\includes""";
    else
        include =  "-I""" + cpp_dir + ..
		["" "../includes"] + """ ";
        cflags   = strcat(include);
    end
    libs     = [];
    tbx_build_src(linknames, files, src_path, ..
	cpp_dir, libs, ldflags, cflags);
endfunction 
nispBuildThirdparty();
clear nispBuildThirdparty
