//
// Copyright (C)  2006-2009 - CEA - Jean-Marc Martinez
// Copyright (C)  2009 - Digiteo - Michael Baudin
//
// This file is released under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html
//
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <vector>
#include <math.h>
#include <algorithm>
#include <stdlib.h> // pour random


// From Thirdparty
#include "dcdflib.h"
#include "smolyak.h"
#include "linpack_d.h"
#include "mt.h"

// From NISP
#include "nisp_gva.h"
#include "nisp_inv.h"
#include "nisp_math.h"
#include "nisp_msg.h"
#include "nisp_pc.h"
#include "nisp_qua.h"
#include "nisp_util.h"

using namespace std;

double nisp_randuni ();
double nisp_standardexprnd ();
double nisp_standardnormrnd ();

// Generate random numbers according to the given distribution.
double nisp_normrnd(double mean, double std) {
	double z;
	z = nisp_standardnormrnd ();
	return(mean + std * z);
}
double nisp_lognrnd(double mean, double std) {
	double z;
	z = nisp_normrnd(mean,std);
	return(exp(z));
}
double nisp_unifrnd(double a, double b) {
	double u;
	u=nisp_randuni ();
	return(a + (b-a)*u );
}
double nisp_logunifrnd(double a, double b) {
	double u;
	u=nisp_unifrnd(a,b);
	return(exp(u));
}
double nisp_exprnd(double rate)
{
	double r;
	r = nisp_standardexprnd();
	return(r/rate);
}

// Invert the given distribution
double nisp_unifinv(double a, double b, double p)
{
	double x;
	x=a+(b-a)*p;
	return x;
}
double nisp_norminv(double mean, double std, double p)
{
	double x;
	double z;
	z=nisp_standardnorminv(p);
	x=mean+std*z;
	return x;
}
double nisp_expinv(double rate, double p)
{
	double x;
	x=(-1./rate)*log(p);
	return x;
}
double nisp_logninv(double mean, double std, double p)
{
	double x;
	double z;
	z=nisp_standardnorminv(p);
	x=exp(mean+std*z);
	return x;
}
double nisp_logunifinv(double a, double b, double p)
{
	double u;
	double x;
	u = nisp_unifinv(a, b, p);
	x=exp(u);
	return x;
}

// CDF of the given distribution
double nisp_normcdf(double mean, double sigma, double x)
{
	double z;
	double p;
	int ind;
	double xerf;
	z = (x-mean)/sigma;
	xerf = -z/sqrt(2.);
	ind = 0;
	p = error_fc( &ind , &xerf )/2.;
	return p;
}
double nisp_logncdf(double mean, double sigma, double x)
{
	double p;
	p=nisp_normcdf(mean, sigma, log(x));
	return p;
}
double nisp_unifcdf(double a, double b, double x)
{
	double p;
	p=(x-a)/(b-a);
	return p;
}
double nisp_logunifcdf(double a, double b, double x)
{
	double p;
	p = nisp_unifcdf(a, b, log(x));
	return p;
}
double nisp_expcdf(double rate, double x)
{
	double p;
	p=-nisp_expm1 ( -x*rate );
	return p;
}

// Compute exp(x)-1 accurately for small x
double nisp_expm1(double x)
{
	double u;
	double y;
	u = exp(x);
	y = (u-1)*x/log(u);
	return y;
}

// nisp_randuni --
// Generate a random variable in (0,1)
double nisp_randuni () {
	double u;
	u = mtrng_urand();
	return u;
}

// Standard exponential random variable
double nisp_standardexprnd () {
  double dum;
  // TODO : why 1.e-100 ???
  do {
    dum = nisp_randuni ();
  }
  while (dum < 1.e-100);
  return -log(dum);
}

// Generate a standard normal random variable
double nisp_standardnormrnd ()
{
  static int    flip = 0;
  static double y;
  double fac, rho, x1 , x2;
  if  (flip == 0) {
    do {
      x1  = 2.0 * nisp_randuni () - 1.0;
      x2  = 2.0 * nisp_randuni () - 1.0;
      rho = x1 * x1 + x2 * x2;
    }
    while (rho >= 1.0 || rho < 1.e-50);
    // TODO : why 1.e-50 ???
    fac = sqrt(-2.0 * log(rho) / rho);
    y = x1 * fac;
    flip = 1;
    return x2 * fac;
  }
  else 
  { 
	  flip = 0; 
	  return y; 
  }
}


// Utiliser par la methode pc::sobol()
int nisp_puissance2(int n) {
	int i,ip;
	for(ip=1,i=1;i<=n;i++) {
		ip=ip*2;
	}
	return(ip);
}
// Lecture des simulations
void nisp_readtarget2(double **y, int np, int ny, char *fichier) {
	int k,j,nlignes,ncolonnes;
	ifstream entree(fichier,ios::in);
	if(!entree) {
		ostringstream msg;
		msg << "Nisp(nisp_readtarget2) : problem to open the file " << fichier << endl;
		nisp_error ( msg.str() );
		return;
	}
	string str;
	entree >> str >> nlignes;
	entree >> str >> ncolonnes;
	if(nlignes   != np) {
		ostringstream msg;
		msg << "Nisp(nisp_readtarget2) : number of simulations " << nlignes << " != " << np << endl;
		nisp_error ( msg.str() );
		return;
	}
	if(ncolonnes != ny) {
		ostringstream msg;
		msg << "Nisp(nisp_readtarget2) : number of output " << ncolonnes << " != " << ny << endl;
		nisp_error ( msg.str() );
		return;
	}
	for(k=1;k<=nlignes;k++) {
		for(j=1;j<=ncolonnes;j++) {
			entree >> y[k][j];
		}
	}
	entree.close();
}
// Utilitaires
int nisp_calculP(int nx, int no) {
	long min, max, num, den, i, p;
	if(nx>no) {
		min=(long) no; 
		max=(long) nx;
	}
	else      {
		min=(long) nx; 
		max=(long) no;
	}
	for(num=1,i=max+1;i<=max+min;i++) {
		num*=i;
	}
	for(den=1,i=2;i<=min;i++) {
		den*=i;
	}
	p=num/den-1;
	return((int) p);
}

// Probleme grandes valeurs
double binomial(int n, int j) {
	int k;
	double num,den;
	for(num=1.,k=n-j+1;k<=n;k++) {
		num=num*k;
	}
	for(den=1.,k=1;k<=j;k++)     {
		den=den*k;
	}
	return(num/den);
}


// Templated version of the Sort.
// From ROOT::TMath::SortImp(Size n1, const Element *a, Index *index, Bool_t down)
//
// Sort the n1 elements of the array a.of Element
// In output the array index contains the indices of the sorted array.
// If down is false sort in increasing order (default is decreasing order).
// This is a translation of the CERNLIB routine sortzv (M101)
// based on the quicksort algorithm.
// NOTE that the array index must be created with a length >= n1
// before calling this function.
//
// See also the declarations at the top of this file.

void dindex(int n1, double *a, int *index, int down) {
	int i,i1,n,i2,i3,i33,i222,iswap,n2;
	int i22 = 0;
	double ai;
	n = n1;
	if (n <= 0 || !a) return;
	if (n == 1) {index[0] = 0; return;}
	for (i=0;i<n;i++) index[i] = i+1;
	for (i1=2;i1<=n;i1++) {
		i3 = i1;
		i33 = index[i3-1];
		ai  = a[i33-1];
		while(1) {
			i2 = i3/2;
			if (i2 <= 0) break;
			i22 = index[i2-1];
			if (ai <= a[i22-1]) break;
			index[i3-1] = i22;
			i3 = i2;
		}
		index[i3-1] = i33;
	}

	while(1) {
		i3 = index[n-1];
		index[n-1] = index[0];
		ai = a[i3-1];
		n--;
		if(n-1 < 0) {index[0] = i3; break;}
		i1 = 1;
		while(2) {
			i2 = i1+i1;
			if (i2 <= n) i22 = index[i2-1];
			if (i2-n > 0) {index[i1-1] = i3; break;}
			if (i2-n < 0) {
				i222 = index[i2];
				if (a[i22-1] - a[i222-1] < 0) {
					i2++;
					i22 = i222;
				}
			}
			if (ai - a[i22-1] > 0) {index[i1-1] = i3; break;}
			index[i1-1] = i22;
			i1 = i2;
		}
	}
	for (i=0;i<n1;i++) index[i]--;
	if (!down) return;
	n2 = n1/2;
	for (i=0;i<n2;i++) {
		iswap         = index[i];
		index[i]      = index[n1-i-1];
		index[n1-i-1] = iswap;
	}
}
