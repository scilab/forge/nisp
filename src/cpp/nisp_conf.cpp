//
// Copyright (C)  2006-2009 - CEA - Jean-Marc Martinez
// Copyright (C)  2009 - INRIA - Michael Baudin
//
// This file is released under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html
//

#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <cstdlib>

using namespace std;

#include "nisp_conf.h"
#include "nisp_msg.h"
#include "mt.h"

void nisp_msgsetfunction ( void (* f)(char * message) ) {
	nisp_messagefunction = f;
}

void nisp_errorsetfunction ( void (* f)(char * message) ) {
	nisp_errorfunction = f;
}

void nisp_verboselevelset ( int newlevel) {
	if (newlevel == 0) {
		nisp_verboselevel = 0;
	}
	else if (newlevel == 1)
	{
		nisp_verboselevel = 1;
	}
	else
	{
		ostringstream msg;
		msg << "NISP - ERROR" << endl;
		msg << "Unknown verbose level " << newlevel << endl;
		nisp_error ( msg.str() );
	}
}

int nisp_verboselevelget () {
	return nisp_verboselevel;
}

void nisp_initseed ( int seed ) {
	double dseed;
	dseed = seed;
	mtrng_set_state_simple(dseed);
}

