//
// Copyright (C)  2006-2009 - CEA - Jean-Marc Martinez
// Copyright (C)  2009 - INRIA - Michael Baudin
//
// This file is released under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html
//
#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <cstdlib>

using namespace std;

#pragma warning(disable : 4996)
#include "nisp_msg.h"

int nisp_verboselevel = 0;
void (* nisp_messagefunction)(char * message) = NULL;
void (* nisp_errorfunction)(char * message) = NULL;

void nisp_message ( char * message ) {
	if (nisp_messagefunction==NULL) {
		cout << message;
	} 
	else 
	{
		nisp_messagefunction(message);
	}
}

void nisp_message ( const string & str ) {
	char * cstr;
	cstr = new char [str.size()+1];
	strcpy (cstr, str.c_str());
	nisp_message ( cstr );
	delete [] cstr;
}

void nisp_verbosemessage ( char * message ) {
	if (nisp_verboselevel==1)
	{
		nisp_message ( message );
	}
}

void nisp_verbosemessage ( const string& str ) {
	char * cstr;
	cstr = new char [str.size()+1];
	strcpy (cstr, str.c_str());
	nisp_verbosemessage ( cstr );
	delete [] cstr;
}


// TODO : generate exceptions instead
// TODO : manage multi-thread issues by global variable nisp_errorfunction
void nisp_error ( char * message ) {
	if (nisp_errorfunction==NULL) {
		nisp_message ( "NISP : Error\n" );
		nisp_message ( message );
		exit(1);
	} 
	else 
	{
		nisp_errorfunction ( message );
	}
}

void nisp_error ( const string & str ) {
	char * cstr;
	cstr = new char [str.size()+1];
	strcpy (cstr, str.c_str());
	nisp_error ( cstr );
	delete [] cstr;
}

