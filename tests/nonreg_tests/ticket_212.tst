// Copyright (C) 2011 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// <-- Non-regression test for bug 212 -->
//
// <-- URL -->
// http://forge.scilab.org/index.php/p/nisp/issues/212/
//
// <-- Short Description -->
// The polychaos_readtarget function is undefined.

path = nisp_getpath();
filename=fullfile(path,"tests","nonreg_tests","PlanStochastique.txt");
srvx = setrandvar_new(filename);
np   = setrandvar_getsize(srvx);
pc   = polychaos_new(srvx,1);
degre = 2;
polychaos_setdegree(pc, degre);
polychaos_setsizetarget(pc, np);
repfile=fullfile(path,"tests","nonreg_tests","Reponses.txt");
polychaos_readtarget(pc, repfile);
//
setrandvar_destroy(srvx);
polychaos_destroy(pc);


