// Copyright (C) 2011 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// <-- Non-regression test for bug 222 -->
//
// <-- URL -->
// http://forge.scilab.org/index.php/p/nisp/issues/222/
//
// <-- Short Description -->
// The polychaos_getindexfirst function does not work.

function y = Ishigami (x)
  a = 7; b = 0.1;
  y(1,1) = sin(x(1)) + a * sin(x(2))**2 + b * x(3)**4 * sin(x(1));
  y(1,2) = 2. * y(1);
endfunction

srvu = setrandvar_new();
for i=1:3
  vu(i) = randvar_new("Uniforme", -%pi, +%pi);
  setrandvar_addrandvar ( srvu, vu(i));
end
nx = setrandvar_getdimension ( srvu ); 

srvx = setrandvar_new(nx);
ny = 2;
pc = polychaos_new ( srvx , ny );

degre = 6;
setrandvar_buildsample(srvx,"Petras",degre);
np = setrandvar_getsize(srvx);

planu = setrandvar_buildsample( srvu , srvx );

np = setrandvar_getsize(srvx);
polychaos_setsizetarget(pc, np);
for k=1:np
  inputdata = planu(k,:);
  outputdata = Ishigami(inputdata);
  polychaos_settarget(pc,k,outputdata);
end

polychaos_setdegree(pc,degre);
polychaos_computeexp(pc,srvx,"Integration");

//
computed = polychaos_getindexfirst ( pc );
expected = [
    3.17876952D-01    3.17876952D-01  
    4.39610570D-01    4.39610570D-01  
    1.27213542D-06    1.27213542D-06  
    ];
assert_checkalmostequal ( computed, expected, [] , 1.e-5 );
//
setrandvar_destroy(srvu);
setrandvar_destroy(srvx);
polychaos_destroy(pc);


