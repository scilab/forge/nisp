// Copyright (C) 2013 - Michael Baudin
// Copyright (C) 2011 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html


//
// <-- JVM NOT MANDATORY -->
// <-- ENGLISH IMPOSED -->

//
y = nisp_product ([1 2]);
assert_checkequal ( y , 2 );
//
x=[
1 2 3
4 5 6
7 8 9
10 11 12
];
y = nisp_product (x);
expected = [
6
120
504
1320
];
assert_checkequal ( y , expected );
//
mu1 = 1.5;
sigma1 = 0.5;
mu2 = 3.5;
sigma2 = 2.5;
np = 10000;
// Define a sampling for x1.
x1 = distfun_normrnd(mu1,sigma1,np,1);
// Define a sampling for x2.
x2 = distfun_normrnd(mu2,sigma2,np,1);
// Merge the two samplings
x = [x1 x2];
// Perform the experiments
y = nisp_product (x);
assert_checkalmostequal ( mean(y) , mu1*mu2 , 1.e-1 );

