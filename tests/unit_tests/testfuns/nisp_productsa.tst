// Copyright (C) 2013 - Michael Baudin
// Copyright (C) 2011 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html


//
// <-- JVM NOT MANDATORY -->
// <-- ENGLISH IMPOSED -->

mu = [1.5 3.5];
sigma = [0.5 2.5];
exact = nisp_productsa ( mu , sigma );
expected.expectation = 5.25;
expected.var = 18.6875;
expected.S = [0.1638796 0.7525084]';
expected.ST = [0.2474916 0.8361204]';
assert_checkalmostequal ( exact.expectation , expected.expectation , 1.e-5 );
assert_checkalmostequal ( exact.var , expected.var , 1.e-5 );
assert_checkalmostequal ( exact.S , expected.S , 1.e-5 );
assert_checkalmostequal ( exact.ST , expected.ST , 1.e-5 );

//
// Another case.
// mu2=0 implies S1=0.
// But Y is not independent of X1.
// First variable
// Normal
mu1 = 0.1;
sigma1 = 0.5;
// Second variable
// Normal
mu2 = 0;
sigma2 = 2.5;
exact = nisp_productsa ( [mu1,mu2]' , [sigma1,sigma2]' );
expected.expectation = 0;
expected.var = 1.625;
expected.S = [0 0.0384615]';
expected.ST = [0.9615385 1.]';
assert_checkalmostequal ( exact.expectation , expected.expectation , 1.e-5 );
assert_checkalmostequal ( exact.var , expected.var , 1.e-5 );
assert_checkalmostequal ( exact.S , expected.S , 1.e-5 );
assert_checkalmostequal ( exact.ST , expected.ST , 1.e-5 );
//
mu=[4 2 1];
sigma=[1 2 4];
exact = nisp_productsa(mu,sigma);
expected.expectation = 8;
expected.var = 2248;
expected.S = [0.0017794 0.0284698 0.4555160]';
expected.ST = [0.0604982 0.5142349 0.9679715]';
assert_checkalmostequal ( exact.expectation , expected.expectation , 1.e-5 );
assert_checkalmostequal ( exact.var , expected.var , 1.e-5 );
assert_checkalmostequal ( exact.S , expected.S , [],1.e-5 );
assert_checkalmostequal ( exact.ST , expected.ST , [],1.e-5 );
