
// Copyright (C) 2011 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html


//
// <-- JVM NOT MANDATORY -->
// <-- ENGLISH IMPOSED -->


a=7.;
b=0.1;
//
y = nisp_ishigami ( [-1 1 2] , a , b );
expected = 2.7686894;
assert_checkalmostequal ( y , expected , 1.e-7 );
//
np = 10000;
x = distfun_unifrnd(-%pi,%pi,np,3);
y = nisp_ishigami ( x , a , b );
expected = a/2;
assert_checkalmostequal ( mean(y) , expected , 1.e-1 );


