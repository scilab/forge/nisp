// Copyright (C) 2013 - Michael Baudin
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html


//
// Test the NISP support functions for verbose levels
//
level = nisp_verboselevelget();
assert_checkequal ( level , 0 );
nisp_verboselevelset ( 1 );
level = nisp_verboselevelget();
assert_checkequal ( level , 1 );
nisp_verboselevelset ( 0 );
level = nisp_verboselevelget();
assert_checkequal ( level , 0 );

// Test wrong verbose level
instr="nisp_verboselevelset ( -1 )";
errmsg = ["NISP: Error at the library level:";"NISP - ERROR";"Unknown verbose level -1"];
assert_checkerror(instr,errmsg);
