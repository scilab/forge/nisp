// Copyright (C) 2012 - Michael Baudin

//
// This file must be used under the terms of the GNU LGPL license.
// 

// <-- NOT FIXED -->

path = nisp_getpath();
demospath = fullfile(path,"demos");
// Demos in NISP
demosscript = "nisp.dem.gateway.sce";
exec(fullfile(demospath,demosscript),-1);
ndemos = size(subdemolist,"r");
for i = 1 : ndemos
	mprintf("Demo #%d: %s: exec(%s)\n",i,subdemolist(i,1),subdemolist(i,2));
	exec(subdemolist(i,2),-1);
end
// Demos in randvar
demosscript = "randvar.dem.gateway.sce";
exec(fullfile(demospath,"randvar",demosscript),-1);
ndemos = size(subdemolist,"r");
for i = 1 : ndemos
	mprintf("Demo #%d: %s: exec(%s)\n",i,subdemolist(i,1),subdemolist(i,2));
	exec(subdemolist(i,2),-1);
end
// Demos in setrandvar
demosscript = "setrandvar.dem.gateway.sce";
exec(fullfile(demospath,"setrandvar",demosscript),-1);
ndemos = size(subdemolist,"r");
for i = 1 : ndemos
	mprintf("Demo #%d: %s: exec(%s)\n",i,subdemolist(i,1),subdemolist(i,2));
	exec(subdemolist(i,2),-1);
end
