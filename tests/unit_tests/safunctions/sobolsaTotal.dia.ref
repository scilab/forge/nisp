// Copyright (C) 2014 - Michael Baudin
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
// Copyright (C) 2010 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html
//
// <-- JVM NOT MANDATORY -->
// <-- ENGLISH IMPOSED -->
// Compute the first order sensitivity indices of the ishigami function.
// Three random variables uniform in [-pi,pi].
function y = ishigami (x)
    a=7.
    b=0.1
    s1=sin(x(:,1))
    s2=sin(x(:,2))
    x34 = x(:,3).^4
    y(:,1) = s1 + a.*s2.^2 + b.*x34.*s1
endfunction
function x = myrandgen ( m , i )
    x = distfun_unifrnd(-%pi,%pi,m,1)
endfunction
distfun_seedset(0);
a=7.;
b=0.1;
exact = nisp_ishigamisa ( a , b );
//
// Get total sensitivity indices.
nx = 3;
distfun_seedset(0);
[st, nbevalf]=nisp_sobolsaTotal(ishigami,nx,myrandgen);
assert_checkalmostequal ( st(1), exact.ST1 , 1.e-2 );
assert_checkalmostequal ( st(2), exact.ST2 , 1.e-2 );
assert_checkalmostequal ( st(3), exact.ST3 , [] , 1.e-2 );
assert_checkequal ( nbevalf , 40000 );
//
// With extra-arguments in f
distfun_seedset(0);
[st, nbevalf]=nisp_sobolsaTotal(list(nisp_ishigami,a,b),nx,myrandgen);
assert_checkalmostequal ( st(1), exact.ST1 , 1.e-2 );
assert_checkalmostequal ( st(2), exact.ST2 , 1.e-2 );
assert_checkalmostequal ( st(3), exact.ST3 , [] , 1.e-2 );
assert_checkequal ( nbevalf , 40000 );
//
// With confidence intervals
distfun_seedset(0);
[st, nbevalf,stmin,stmax]=nisp_sobolsaTotal(list(nisp_ishigami,a,b),nx,myrandgen);
assert_checkalmostequal ( st(1), exact.ST1 , 1.e-2 );
assert_checkalmostequal ( st(2), exact.ST2 , 1.e-2 );
assert_checkalmostequal ( st(3), exact.ST3 , [] , 1.e-2 );
assert_checkequal ( nbevalf , 40000 );
assert_checkalmostequal(stmax,[0.5689569,0.4593470,0.2510420]',1.e-2);
assert_checkalmostequal(stmin,[0.5375823,0.4321912,0.2343278]',1.e-2);
////////////////////////////////////////////////////////////////
//
// Multiple output : ny=2
function checkIshigaminy2ST(s,a1,b1,a2,b2)
    // Check total sensitivity indices for y1
    exact1 = nisp_ishigamisa ( a1 , b1 );
    SE1=[exact1.ST1;exact1.ST2;exact1.ST3];
    assert_checkalmostequal ( st(:,1), SE1 , [] , 1.e-1 );
    // Check sensitivity indices for y2
    exact2 = nisp_ishigamisa ( a2 , b2 );
    SE2=[exact2.ST1;exact2.ST2;exact2.ST3];
    assert_checkalmostequal ( st(:,2), SE2 , [] , 1.e-1 );
endfunction
function y=ishigaminy2(x,a1,b1,a2,b2)
    y1=nisp_ishigami(x,a1,b1)
    y2=nisp_ishigami(x,a2,b2)
    y=[y1,y2]
endfunction
n = 10000;
nx = 3;
a1=7.;
b1=0.1;
a2=1.;
b2=1.;
myfuncny1=list(ishigaminy2,a1,b1,a2,b2);
st=nisp_sobolsaTotal(myfuncny1, nx, myrandgen, n);
checkIshigaminy2ST(st,a1,b1,a2,b2);
// 
// Check confidence intervals
[st,nbevalf,stmin,stmax]=nisp_sobolsaTotal(myfuncny1, nx, myrandgen, n);
checkIshigaminy2ST(st,a1,b1,a2,b2)
assert_checkequal(nbevalf , 40000 );
assert_checktrue(stmin>=0);
assert_checktrue(st>=stmin);
assert_checktrue(st<=stmax);
assert_checktrue(stmax<=1);
ny=2;
assert_checkequal(size(st),[nx,ny]);
assert_checkequal(size(stmin),[nx,ny]);
assert_checkequal(size(stmax),[nx,ny]);
//
////////////////////////////////////////////////////////////////
