// Copyright (C) 2014 - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

//
// <-- ENGLISH IMPOSED -->

// Confidence interval at 95%
rho=0.3;
n=100;
[rhomin,rhomax] = nisp_cicorrcoef(rho,n);
assert_checkalmostequal(rhomax,0.468794214523);
assert_checkalmostequal(rhomin,0.110067667541);

// Confidence interval at 99%
rho=0.3;
n=100;
c=1-0.99;
[rhomin,rhomax] = nisp_cicorrcoef(rho,n,c);
assert_checkalmostequal(rhomax,0.516133980034);
assert_checkalmostequal(rhomin,0.047946970944);
//
// Test with rho=1
rho=1.;
n=100;
[rhomin,rhomax] = nisp_cicorrcoef(rho,n);
assert_checkequal(rhomin,1);
assert_checkequal(rhomax,1);

//
// Variation of the C.I. to rho and n
n=100;
c=1-0.95;
rho=linspace(0,1)';
[rhomin10,rhomax10] = nisp_cicorrcoef(rho,10,c);
[rhomin100,rhomax100] = nisp_cicorrcoef(rho,100,c);
[rhomin1000,rhomax1000] = nisp_cicorrcoef(rho,1000,c);
plot(rho,rhomin10,"g-")
plot(rho,rhomin100,"b-")
plot(rho,rhomin1000,"r-")
plot(rho,rhomax10,"g-")
plot(rho,rhomax100,"b-")
plot(rho,rhomax1000,"r-")
xlabel("rho");
ylabel("95% Confidence Interval");
title("Sensitivity of the C.I. to rho and n");
legend(["n=10","n=100","n=1000"],"in_lower_right");
