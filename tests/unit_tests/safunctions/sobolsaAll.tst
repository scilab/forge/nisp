// Copyright (C) 2014 - Michael Baudin
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
// Copyright (C) 2010 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// <-- JVM NOT MANDATORY -->
// <-- ENGLISH IMPOSED -->

// Compute the first order sensitivity indices of the ishigami function.
// Three random variables uniform in [-pi,pi].
function y = ishigami (x)
    a=7.
    b=0.1
    s1=sin(x(:,1))
    s2=sin(x(:,2))
    x34 = x(:,3).^4
    y(:,1) = s1 + a.*s2.^2 + b.*x34.*s1
endfunction
function x = myrandgen ( m , i )
    x = distfun_unifrnd(-%pi,%pi,m,1)
endfunction
distfun_seedset(0);
a=7.;
b=0.1;
exact = nisp_ishigamisa ( a , b );
//
// Get all sensitivity indices.
nx = 3;
distfun_seedset(0);
[ si , nbevalf , mi ] = nisp_sobolsaAll ( ishigami , nx , myrandgen );
assert_checkequal ( nbevalf , 70000 );
miexpected = [
1,0,0;
0,1,0;   
0,0,1;   
1,1,0;   
1,0,1;   
0,1,1;   
1,1,1
];
assert_checkequal ( mi , miexpected );
siexpected = [
exact.S1
exact.S2
exact.S3
exact.S12
exact.S13
exact.S23
exact.S123
];
assert_checkalmostequal ( si, siexpected , [] , 1.e-1 );
assert_checkalmostequal ( sum(si) , 1 , [],0.1 );

////////////////////////////////////////////////////////////////
//
// Multiple output : ny=2
function y=ishigaminy2(x,a1,b1,a2,b2)
    y1=nisp_ishigami(x,a1,b1)
    y2=nisp_ishigami(x,a2,b2)
    y=[y1,y2]
endfunction

function checkIshigaminy2S(si,a1,b1,a2,b2)
    // Check sensitivity indices for y1
    exact = nisp_ishigamisa ( a1 , b1 );
    siexpected = [
    exact.S1
    exact.S2
    exact.S3
    exact.S12
    exact.S13
    exact.S23
    exact.S123
    ];
    assert_checkalmostequal ( si(:,1), siexpected , [] , 1.e-1 );
    // Check sensitivity indices for y2
    exact = nisp_ishigamisa ( a2 , b2 );
    siexpected = [
    exact.S1
    exact.S2
    exact.S3
    exact.S12
    exact.S13
    exact.S23
    exact.S123
    ];
    assert_checkalmostequal ( si(:,2), siexpected , [] , 1.e-1 );
endfunction

n = 10000;
nx = 3;
a1=7.;
b1=0.1;
a2=1.;
b2=1.;
myfuncny1=list(ishigaminy2,a1,b1,a2,b2);
[ si , nbevalf , mi ]=nisp_sobolsaAll(myfuncny1, nx, myrandgen, n);
assert_checkequal ( nbevalf , 70000 );
miexpected = [
1,0,0;
0,1,0;   
0,0,1;   
1,1,0;   
1,0,1;   
0,1,1;   
1,1,1
];
assert_checkequal ( mi , miexpected );
checkIshigaminy2S(si,a1,b1,a2,b2);
ny=2;
assert_checkequal(size(si),[7,ny]);
assert_checkalmostequal ( sum(si,"r") , [1,1] , [],0.1 );

//
////////////////////////////////////////////////////////////////
