
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
// Copyright (C) 2010 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html


// Load several functions require for the test.
path = nisp_getpath();
testpath = fullfile(path,"tests","unit_tests","polychaos");
exec(fullfile(testpath,"createMyPolychaos.sci"));
exec(fullfile(testpath,"cleanupMyPolychaos.sci"));



// Test polychaos_settarget ( pc , k , j , output )
// Test output = polychaos_gettarget ( pc , k , j )
rv1 = randvar_new("Normale");
rv2 = randvar_new("Uniforme");
srv = setrandvar_new ();
setrandvar_addrandvar ( srv , rv1 );
setrandvar_addrandvar ( srv , rv2 );
ny = 10;
pc = polychaos_new ( srv , ny );
np = 3;
polychaos_setsizetarget ( pc , np );
for k = 1:np
  for j = 1:ny
    expected = j + k;
    polychaos_settarget ( pc , k , j , expected );
    computed = polychaos_gettarget ( pc , k , j );
    assert_checkalmostequal ( computed , expected , %eps );
  end
end
polychaos_destroy ( pc );
setrandvar_destroy ( srv );
randvar_destroy ( rv1 );
randvar_destroy ( rv2 );

// Test polychaos_settarget ( pc , k , output ) with output as vector 
// Test output = polychaos_gettarget ( pc , k )
rv1 = randvar_new("Normale");
rv2 = randvar_new("Uniforme");
srv = setrandvar_new ();
setrandvar_addrandvar ( srv , rv1 );
setrandvar_addrandvar ( srv , rv2 );
ny = 10;
pc = polychaos_new ( srv , ny );
np = 3;
polychaos_setsizetarget ( pc , np );
for k = 1:np
  expected = k + (1:ny);
  polychaos_settarget ( pc , k , expected );
  computed = polychaos_gettarget ( pc , k );
  assert_checkalmostequal ( computed , expected , %eps );
end
polychaos_destroy ( pc );
setrandvar_destroy ( srv );
randvar_destroy ( rv1 );
randvar_destroy ( rv2 );

// Test polychaos_settarget ( pc , output ) with output as matrix 
// Test output = polychaos_gettarget ( pc )
rv1 = randvar_new("Normale");
rv2 = randvar_new("Uniforme");
srv = setrandvar_new ();
setrandvar_addrandvar ( srv , rv1 );
setrandvar_addrandvar ( srv , rv2 );
ny = 10;
pc = polychaos_new ( srv , ny );
np = 3;
polychaos_setsizetarget ( pc , np );
expected = (1:np).' * (1:ny);
polychaos_settarget ( pc , expected );
computed = polychaos_gettarget ( pc );
assert_checkalmostequal ( computed , expected , %eps );
polychaos_destroy ( pc );
setrandvar_destroy ( srv );
randvar_destroy ( rv1 );
randvar_destroy ( rv2 );


