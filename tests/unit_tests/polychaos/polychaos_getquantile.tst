
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
// Copyright (C) 2010 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Load several functions require for the test.
path = nisp_getpath();
testpath = fullfile(path,"tests","unit_tests","polychaos");
exec(fullfile(testpath,"createMyPolychaos2.sci"));
exec(fullfile(testpath,"cleanupMyPolychaos.sci"));

// Test polychaos_getquantile ( pc , alpha , ovar )
// Test polychaos_getquantile ( pc , alpha );

nisp_initseed ( 0 );
[ pc , srvx , rvx1 , rvx2 , srvu , vu1 , vu2 ] = createMyPolychaos2();
polychaos_computeexp(pc,srvx,"Integration");
np = 10000;
alpha = 0.8;
expected = 4.1457215;
//mu=2;
//sigma=sqrt(0.5^2+2.5^2);
//q=distfun_norminv(0.8,mu,sigma) // 4.1457215
//
// QmcSobol
polychaos_buildsample ( pc , "QmcSobol" , np );
computed = polychaos_getquantile ( pc , alpha , 1 );
assert_checkalmostequal ( computed , expected , 1.e-1 );
computed = polychaos_getquantile ( pc , alpha );
assert_checkalmostequal ( computed , expected , 1.e-1 );
//
// MonteCarlo
polychaos_buildsample ( pc , "MonteCarlo" , np );
computed = polychaos_getquantile ( pc , alpha , 1 );
assert_checkalmostequal ( computed , expected , 1.e-1 );
computed = polychaos_getquantile ( pc , alpha );
assert_checkalmostequal ( computed , expected , 1.e-1 );
//
// Lhs
polychaos_buildsample ( pc , "Lhs" , np );
computed = polychaos_getquantile ( pc , alpha , 1 );
assert_checkalmostequal ( computed , expected , 1.e-1 );
computed = polychaos_getquantile ( pc , alpha );
assert_checkalmostequal ( computed , expected , 1.e-1 );
//
cleanupMyPolychaos ( pc , srvx , rvx1 , rvx2 , srvu , vu1 , vu2 );
//

