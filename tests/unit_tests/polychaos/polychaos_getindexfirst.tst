
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
// Copyright (C) 2010 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html


// Load several functions require for the test.
path = nisp_getpath();
testpath = fullfile(path,"tests","unit_tests","polychaos");
exec(fullfile(testpath,"createMyPolychaos.sci"));
exec(fullfile(testpath,"cleanupMyPolychaos.sci"));




// Test polychaos_getindexfirst ( pc , ivar , ovar )
// Test polychaos_getindexfirst ( pc , ivar )
// Test polychaos_getindexfirst ( pc )
// TODO : test polychaos_getindexfirst with ny>1
[ pc , srvx , rvx1 , rvx2 , srvu , vu1 , vu2 ] = createMyPolychaos();
polychaos_computeexp(pc,srvx,"Integration");
computed = polychaos_getindexfirst ( pc , 1 , 1 );
expected = 0.7656250;
// TODO : is 1.e-7 correct ?
assert_checkalmostequal ( computed , expected , 1.e-7 );
computed = polychaos_getindexfirst ( pc , 1 );
expected = 0.7656250;
assert_checkalmostequal ( computed , expected , 1.e-7 );
computed = polychaos_getindexfirst ( pc );
expected = [
0.765625
0.1875
];
assert_checkalmostequal ( computed , expected , 1.e-7 );
cleanupMyPolychaos ( pc , srvx , rvx1 , rvx2 , srvu , vu1 , vu2 );


