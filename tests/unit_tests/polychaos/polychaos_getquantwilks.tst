
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
// Copyright (C) 2010 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html


// Load several functions require for the test.
path = nisp_getpath();
testpath = fullfile(path,"tests","unit_tests","polychaos");
exec(fullfile(testpath,"createMyPolychaos.sci"));
exec(fullfile(testpath,"cleanupMyPolychaos.sci"));


// Test polychaos_getquantwilks ( pc , walpha , wbeta , ovar )
// Test polychaos_getquantwilks ( pc , walpha , wbeta );
nisp_initseed ( 0 );
[ pc , srvx , rvx1 , rvx2 , srvu , vu1 , vu2 ] = createMyPolychaos();
polychaos_computeexp(pc,srvx,"Integration");
np = 10000;
polychaos_buildsample ( pc , "MonteCarlo" , np );
walpha = 0.5;
wbeta = 0.75;
computed = polychaos_getquantwilks ( pc , walpha , wbeta , 1 );
expected = 1.6400955;
assert_checkalmostequal ( computed , expected , 1.e-1 );
computed = polychaos_getquantwilks ( pc , walpha , wbeta );
expected = 1.6400955;
assert_checkalmostequal ( computed , expected , 1.e-1 );
cleanupMyPolychaos ( pc , srvx , rvx1 , rvx2 , srvu , vu1 , vu2 );


