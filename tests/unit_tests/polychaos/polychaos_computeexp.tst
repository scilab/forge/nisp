
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
// Copyright (C) 2010 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html



// Load several functions require for the test.
path = nisp_getpath();
testpath = fullfile(path,"tests","unit_tests","polychaos");
exec(fullfile(testpath,"createMyPolychaos.sci"));
exec(fullfile(testpath,"cleanupMyPolychaos.sci"));


function y = Exemple (x)
y = x(:,1) + x(:,2);
endfunction

function pc = buildPC(samplingMethod,samplingNp,expansionMethod,expansionDegre)
    rv1 = randvar_new("Normale",1.0,0.5);
    rv2 = randvar_new("Normale",1.0,2.5);
    srv = setrandvar_new();
    setrandvar_addrandvar ( srv,rv1);
    setrandvar_addrandvar ( srv,rv2);
    rvx1 = randvar_new("Normale");
    rvx2 = randvar_new("Normale");
    srv2 = setrandvar_new();
    setrandvar_addrandvar ( srv2,rvx1);
    setrandvar_addrandvar ( srv2,rvx2);
    ny = 1;
    pc = polychaos_new ( srv2 , ny );
    setrandvar_buildsample(srv2,samplingMethod,samplingNp);
    inputdata=setrandvar_buildsample( srv , srv2 );
    np = setrandvar_getsize(srv);
    polychaos_setsizetarget(pc,np);
    nx = polychaos_getdiminput(pc);
    ny = polychaos_getdimoutput(pc);
    outputdata = Exemple(inputdata);
    polychaos_settarget(pc,outputdata);
    polychaos_setdegree(pc,expansionDegre);
    polychaos_computeexp(pc,srv2,expansionMethod);
endfunction


//
meanexact=2;
varexact=0.5^2+2.5^2;
sexact=[0.5^2/6.5 2.5^2/6.5]';

// Test polychaos_computeexp ( pc , srv2 , "Integration");
//
// Regression : Quadrature
//
pc = buildPC("Quadrature",2,"Integration",2);
// polychaos_sasummary(pc)
m=polychaos_getmean(pc);
assert_checkalmostequal(m,meanexact);
v=polychaos_getvariance(pc);
assert_checkalmostequal(v,varexact);
s=polychaos_getindexfirst(pc);
assert_checkalmostequal(s,sexact);
nisp_destroyall();
//
// Regression : Petras
// Petras does not perform well here.
//
pc = buildPC("Petras",10,"Integration",2);
// polychaos_sasummary(pc)
m=polychaos_getmean(pc);
assert_checkalmostequal(m,meanexact);
v=polychaos_getvariance(pc);
assert_checkalmostequal(v,varexact,1.e-2);
s=polychaos_getindexfirst(pc);
assert_checkalmostequal(s,sexact,5.e-2);
nisp_destroyall();
//
// Regression : SmolyakGauss
//
pc = buildPC("SmolyakGauss",2,"Integration",2);
// polychaos_sasummary(pc)
m=polychaos_getmean(pc);
assert_checkalmostequal(m,meanexact);
v=polychaos_getvariance(pc);
assert_checkalmostequal(v,varexact);
s=polychaos_getindexfirst(pc);
assert_checkalmostequal(s,sexact);
nisp_destroyall();
//
// Regression : SmolyakTrapeze
// SmolyakTrapeze does not perform well here.
//
pc = buildPC("SmolyakTrapeze",2,"Integration",2);
// polychaos_sasummary(pc)
m=polychaos_getmean(pc);
assert_checkalmostequal(m,meanexact,1);
v=polychaos_getvariance(pc);
assert_checkalmostequal(v,varexact,1);
s=polychaos_getindexfirst(pc);
assert_checkalmostequal(s,sexact,1.);
nisp_destroyall();
//
// Regression : SmolyakClenshawCurtis
// SmolyakClenshawCurtis does not perform well here.
//
pc = buildPC("SmolyakClenshawCurtis",2,"Integration",2);
// polychaos_sasummary(pc)
m=polychaos_getmean(pc);
assert_checkalmostequal(m,meanexact,1);
v=polychaos_getvariance(pc);
assert_checkalmostequal(v,varexact,1);
s=polychaos_getindexfirst(pc);
assert_checkalmostequal(s,sexact,1.);
nisp_destroyall();
//
// Regression : SmolyakFejer
// SmolyakFejer does not perform well here.
//
pc = buildPC("SmolyakFejer",2,"Integration",2);
// polychaos_sasummary(pc)
m=polychaos_getmean(pc);
assert_checkalmostequal(m,meanexact,1);
v=polychaos_getvariance(pc);
assert_checkalmostequal(v,varexact,1);
s=polychaos_getindexfirst(pc);
assert_checkalmostequal(s,sexact,1.);
nisp_destroyall();
//
// Integration : Monte-Carlo
//
pc = buildPC("MonteCarlo",1000,"Regression",2);
// polychaos_sasummary(pc)
m=polychaos_getmean(pc);
assert_checkalmostequal(m,meanexact);
v=polychaos_getvariance(pc);
assert_checkalmostequal(v,varexact);
s=polychaos_getindexfirst(pc);
assert_checkalmostequal(s,sexact);
nisp_destroyall();
//
// Integration : Lhs
//
pc = buildPC("Lhs",1000,"Regression",2);
// polychaos_sasummary(pc)
m=polychaos_getmean(pc);
assert_checkalmostequal(m,meanexact);
v=polychaos_getvariance(pc);
assert_checkalmostequal(v,varexact);
s=polychaos_getindexfirst(pc);
assert_checkalmostequal(s,sexact);
nisp_destroyall();
//
// Integration : QmcSobol
//
pc = buildPC("QmcSobol",1000,"Regression",2);
// polychaos_sasummary(pc)
m=polychaos_getmean(pc);
assert_checkalmostequal(m,meanexact);
v=polychaos_getvariance(pc);
assert_checkalmostequal(v,varexact);
s=polychaos_getindexfirst(pc);
assert_checkalmostequal(s,sexact);
nisp_destroyall();

// Test polychaos_computeexp ( token , pc , invalue , varopt ) 
// TODO : test this !
rv1 = randvar_new("Normale",1.0,0.5);
rv2 = randvar_new("Uniforme",1.0,2.5);
srvx = setrandvar_new();
setrandvar_addrandvar ( srvx,rv1);
setrandvar_addrandvar ( srvx,rv2);
rvx1 = randvar_new("Normale");
rvx2 = randvar_new("Uniforme");
srv2 = setrandvar_new();
setrandvar_addrandvar ( srv2,rvx1);
setrandvar_addrandvar ( srv2,rvx2);
ny = 1;
pc = polychaos_new ( srv2 , ny );
pc2 = polychaos_new ( srv2 , ny );
//polychaos_computeexp ( pc , pc2 , [4.0 5.0] , [1 0]);
// TODO : what to assert ?
polychaos_destroy(pc);
polychaos_destroy(pc2);
randvar_destroy(rv1);
randvar_destroy(rv2);
randvar_destroy(rvx1);
randvar_destroy(rvx2);

