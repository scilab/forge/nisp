
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
// Copyright (C) 2010 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html



// Load several functions require for the test.
path = nisp_getpath();
testpath = fullfile(path,"tests","unit_tests","polychaos");
exec(fullfile(testpath,"createMyPolychaos.sci"));
exec(fullfile(testpath,"cleanupMyPolychaos.sci"));



// Test polychaos_getsample ( pc , k , ovar )
// Test polychaos_getsample ( pc , k )
// Test polychaos_getsample ( pc )
nisp_initseed ( 0 );
[ pc , srvx , rvx1 , rvx2 , srvu , vu1 , vu2 ] = createMyPolychaos();
polychaos_computeexp(pc,srvx,"Integration");
np = 100;
polychaos_buildsample ( pc , "MonteCarlo" , np );
for k=1:np
  ovar = 1;
  sampling(k) = polychaos_getsample ( pc , k , 1 );
end
computed = mean(sampling);
expected = 1.75;
assert_checkalmostequal ( computed , expected , 1.e-1 );
for k=1:np
  ovar = 1;
  sampling(k) = polychaos_getsample ( pc , k );
end
computed = mean(sampling);
expected = 1.75;
assert_checkalmostequal ( computed , expected , 1.e-1 );
sampling = polychaos_getsample ( pc );
computed = mean(sampling);
expected = 1.75;
assert_checkalmostequal ( computed , expected , 1.e-1 );
cleanupMyPolychaos ( pc , srvx , rvx1 , rvx2 , srvu , vu1 , vu2 );

