
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
// Copyright (C) 2010 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html


// Load several functions require for the test.
path = nisp_getpath();
testpath = fullfile(path,"tests","unit_tests","polychaos");
exec(fullfile(testpath,"createMyPolychaos.sci"));
exec(fullfile(testpath,"cleanupMyPolychaos.sci"));





// Test polychaos_getmean ( pc , ovar )
// Test polychaos_getmean ( pc )
[ pc , srvx , rvx1 , rvx2 , srvu , vu1 , vu2 ] = createMyPolychaos();
polychaos_computeexp(pc,srvx,"Integration");
computed = polychaos_getmean ( pc , 1 );
expected = 1.75;
// TODO : is 1.e-8 correct ?
assert_checkalmostequal ( computed , expected , 1.e-8 );
computed = polychaos_getmean ( pc );
expected = 1.75;
assert_checkalmostequal ( computed , expected , 1.e-8 );
cleanupMyPolychaos ( pc , srvx , rvx1 , rvx2 , srvu , vu1 , vu2 );



