
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
// Copyright (C) 2010 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html



// Load several functions require for the test.
path = nisp_getpath();
testpath = fullfile(path,"tests","unit_tests","polychaos");
exec(fullfile(testpath,"createMyPolychaos.sci"));
exec(fullfile(testpath,"cleanupMyPolychaos.sci"));


// Test polychaos_getanova ( pc )
// Test polychaos_getanova ( pc , r )
// Test polychaos_getanovaord ( pc )
// Test polychaos_getanovaord ( pc , threshold )
// Test polychaos_getanovaord ( pc , threshold , r )
// Test polychaos_getanovaordco ( pc )
// Test polychaos_getanovaordco ( pc , threshold )
// Test polychaos_getanovaordco ( pc , threshold , r )
[ pc , srvx , rvx1 , rvx2 , srvu , vu1 , vu2 ] = createMyPolychaos();
polychaos_computeexp(pc,srvx,"Integration");
polychaos_getanova ( pc );
polychaos_getanova ( pc , 1 );
polychaos_getanovaord ( pc );
polychaos_getanovaord ( pc , 0.1 );
polychaos_getanovaord ( pc , 0.1 , 1 );
polychaos_getanovaordco ( pc );
polychaos_getanovaordco ( pc , 0.1 );
polychaos_getanovaordco ( pc , 0.1 , 1 );
cleanupMyPolychaos ( pc , srvx , rvx1 , rvx2 , srvu , vu1 , vu2 );

