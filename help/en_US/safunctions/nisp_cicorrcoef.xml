<?xml version="1.0" encoding="UTF-8"?>

<!--
 *
 * This help file was generated from nisp_cicorrcoef.sci using help_from_sci().
 *
 -->

<refentry version="5.0-subset Scilab" xml:id="nisp_cicorrcoef" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:scilab="http://www.scilab.org"
          xmlns:db="http://docbook.org/ns/docbook">

  <refnamediv>
    <refname>nisp_cicorrcoef</refname>
    <refpurpose>Confidence interval for a correlation coefficient</refpurpose>
  </refnamediv>


<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   [rhomin,rhomax] = nisp_cicorrcoef(rho,n)
   [rhomin,rhomax] = nisp_cicorrcoef(rho,n,c)
   
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>rho:</term>
      <listitem><para> a nx-by-ny matrix of doubles, the correlation coefficient. Should generally be in [0,1], but this is not mandatory.</para></listitem></varlistentry>
   <varlistentry><term>n:</term>
      <listitem><para> a 1-by-1 matrix of doubles, the number of experiments</para></listitem></varlistentry>
   <varlistentry><term>c :</term>
      <listitem><para> a 1-by-1 matrix of doubles (default c = 1-0.95), the level for the confidence interval. Must be in the range [0,0.5].</para></listitem></varlistentry>
   <varlistentry><term>rhomin :</term>
      <listitem><para> a nx-by-ny matrix of doubles, the lower bound of the confidence interval</para></listitem></varlistentry>
   <varlistentry><term>rhomax :</term>
      <listitem><para> a nx-by-ny matrix of doubles, the upper bound of the confidence interval</para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Description</title>
   <para>
Computes a confidence interval at level 1-c for the correlation
coefficient rho, computed with n samples.
   </para>
   <para>
The default value of c corresponds to a 95% confidence interval.
   </para>
   <para>
Mathematical notes
   </para>
   <para>
This method uses the Fisher transformation :
   </para>
   <para>
<latex>
z=\frac{1}{2} \ln\left(\frac{1+\rho}{1-\rho}\right)
</latex>
   </para>
   <para>
Given two samples (Xi,Yi) for i=1,...,n, we denote by R the sample
correlation between Xi and Yi, and suppose that the
pairs (Xi,Yi) are independent.
Suppose that (X, Y) has a bivariate normal distribution with
correlation coefficient rho.
   </para>
   <para>
Then z is approximately normally distributed with mean :
   </para>
   <para>
<latex>
E(Z)=\frac{1}{2} \ln\left(\frac{1+\rho}{1-\rho}\right),
</latex>
   </para>
   <para>
and variance :
   </para>
   <para>
<latex>
V(Z)=\frac{1}{n-3}.
</latex>
   </para>
   <para>
This allows to compute a confidence interval for Z.
The confidence interval for rho is computed from
the inverse transformation :
   </para>
   <para>
<latex>
\rho=\tanh(z)
</latex>
   </para>
   <para>
</para>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
// Confidence interval at 95%
rho=0.3;
n=100;
[rhomin,rhomax] = nisp_cicorrcoef(rho,n)

// Confidence interval at 99%
rho=0.3;
n=100;
c=1-0.99;
[rhomin,rhomax] = nisp_cicorrcoef(rho,n,c)

// See C.I. depending on rho and n
n=100;
c=1-0.95;
rho=linspace(0,1)';
[rhomin10,rhomax10] = nisp_cicorrcoef(rho,10,c);
[rhomin100,rhomax100] = nisp_cicorrcoef(rho,100,c);
[rhomin1000,rhomax1000] = nisp_cicorrcoef(rho,1000,c);
plot(rho,rhomin10,"g-")
plot(rho,rhomin100,"b-")
plot(rho,rhomin1000,"r-")
plot(rho,rhomax10,"g-")
plot(rho,rhomax100,"b-")
plot(rho,rhomax1000,"r-")
xlabel("rho");
ylabel("95% Confidence Interval");
title("Sensitivity of the C.I. to rho and n");
legend(["n=10","n=100","n=1000"],"in_lower_right");

   ]]></programlisting>
</refsection>

<refsection>
   <title>Authors</title>
   <simplelist type="vert">
   <member>Copyright (C) 2014 - Michael Baudin</member>
   </simplelist>
</refsection>

<refsection>
   <title>Bibliography</title>
   <para>http://en.wikipedia.org/wiki/Fisher_transformation</para>
   <para>GdR Ondes &amp; Mascot Num, "Analyse de sensibilite globale par decomposition de la variance", jean-marc.martinez@cea.fr, 13 janvier 2011, Institut Henri Poincare</para>
</refsection>
</refentry>
