<?xml version="1.0" encoding="UTF-8"?>

<!--
 *
 * This help file was generated from nisp_sobolsaAll.sci using help_from_sci().
 *
 -->

<refentry version="5.0-subset Scilab" xml:id="nisp_sobolsaAll" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:scilab="http://www.scilab.org"
          xmlns:db="http://docbook.org/ns/docbook">

  <refnamediv>
    <refname>nisp_sobolsaAll</refname>
    <refpurpose>Compute sensitivity indices by Sobol, Ishigami, Homma.</refpurpose>
  </refnamediv>


<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   si = nisp_sobolsaAll ( func , nx )
   si = nisp_sobolsaAll ( func , nx , randgen )
   si = nisp_sobolsaAll ( func , nx , randgen , n )
   si = nisp_sobolsaAll ( func , nx , randgen , n , inrange)
   [ si , nbevalf ] = nisp_sobolsaAll ( ... )
   [ si , nbevalf , mi ] = nisp_sobolsaAll ( ... )
   
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>func :</term>
      <listitem><para> a function or a list, the name of the function to be evaluated.</para></listitem></varlistentry>
   <varlistentry><term>nx :</term>
      <listitem><para> a 1-by-1 matrix of floating point integers, the number of inputs of the function.</para></listitem></varlistentry>
   <varlistentry><term>randgen :</term>
      <listitem><para> a function or a list, the random number generator. (default = uniform random variables)</para></listitem></varlistentry>
   <varlistentry><term>n :</term>
      <listitem><para> a 1-by-1 matrix of floating point integers (default n=10000), the number of Monte-Carlo experiments, for each sensitivity index</para></listitem></varlistentry>
   <varlistentry><term>inrange :</term>
      <listitem><para> a 1-by-1 matrix of booleans (default inrange = %t), set to true to restrict the sensitivity indices into [0,1].</para></listitem></varlistentry>
   <varlistentry><term>si :</term>
      <listitem><para> a nx-by-ny matrix of doubles, the partial sensitivity indices, where nx is the number of input variables and ny is the number of output variables.</para></listitem></varlistentry>
   <varlistentry><term>nbevalf :</term>
      <listitem><para> a nx-by-1 matrix of doubles, the actual number of function evaluations.</para></listitem></varlistentry>
   <varlistentry><term>mi :</term>
      <listitem><para> a m-by-nx matrix of doubles, the multi-indices of the variables in si, where m=2^nx - 1. Each row in mi represents a group of variables. We have mi(k,i) = 1 if Xi is in the group of variables and 0 if not.</para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Description</title>
   <para>
The algorithm uses the Sobol method to compute the partial
sensitivity indices.
   </para>
   <para>
This method assumes that all the input random variables are
independent.
   </para>
   <para>
On output, the sensitivity indices are forced to be in the range [0,1].
   </para>
   <para>
Any optional input argument equal to the empty matrix will be set to its
default value.
   </para>
   <para>
See "Specifications of func" for the specifications
of the "func" and "randgen" arguments.
   </para>
   <para>
</para>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
// Compute the partial sensitivity indices of the ishigami function.
// Three random variables uniform in [-pi,pi].
function y = ishigami (x)
a=7.
b=0.1
s1=sin(x(:,1))
s2=sin(x(:,2))
x34 = x(:,3).^4
y(:,1) = s1 + a.*s2.^2 + b.*x34.*s1
endfunction
function x = myrandgen ( m , i )
x = distfun_unifrnd(-%pi,%pi,m,1)
endfunction
a=7.;
b=0.1;
exact = nisp_ishigamisa ( a , b )
n = 1000;
nx = 3;
[ si , nbevalf , mi ] = nisp_sobolsaAll ( ishigami , nx , myrandgen )
// The ANOVA decomposition can be seen more easily in
[mi si]
// The sum of si is 1.
sum(si) // expected = 1.

   ]]></programlisting>
</refsection>

<refsection>
   <title>Authors</title>
   <simplelist type="vert">
   <member>Copyright (C) 2014 - Michael Baudin</member>
   <member>Copyright (C) 2011 - Michael Baudin - DIGITEO</member>
   </simplelist>
</refsection>
</refentry>
