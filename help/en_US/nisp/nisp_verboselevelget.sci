// Copyright (C) 2008-2011 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html


function nisp_verboselevelget ( )
  // Returns the current verbose level.
  // 
  // Calling Sequence
  //   level = nisp_verboselevelget ( )
  //
  // Parameters
  // level : a 1-by-1 matrix of floating point integers, the current verbose level
  //
  // Description
  // The default value of the verbose level is 0. 
  // If the verbose level is set to 1, the NISP library may display additionnal message. 
  //
  // In the current implementation, there are no such verbose message, 
  // but this may change in future releases of the library.
  //
  // Examples
  // level = nisp_verboselevelget ( )
  //
  // Authors
  // Copyright (C) 2008-2011 - INRIA - Michael Baudin
  //

endfunction


