// Copyright (C) 2015 - Michael Baudin
// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// Updates the .xml files by deleting existing files and 
// creating them again from the .sci with help_from_sci.
//
cwd = get_absolute_file_path("update_help.sce");
modulename = "nisp";
//
// Generate the static functions help
helpdir = fullfile(cwd,"nisp");
funmat = [
  "nisp_destroyall"
  "nisp_printall"
  "nisp_getpath"
  ];
macrosdir = fullfile(cwd,"..","..","macros");
demosdir = [];
helptbx_helpupdate ( funmat , helpdir , macrosdir , demosdir , modulename , %t );
//
// Generate the SA functions help pages
helpdir = fullfile(cwd,"safunctions");
funmat = [
  "nisp_sobolsaFirst"
  "nisp_sobolsaTotal"
  "nisp_sobolsaAll"
  "nisp_bruteforcesa"
  "nisp_plotsa"
  "nisp_srcsa"
  "nisp_sobolsaInterval"
  "nisp_cicorrcoef"
  ];
macrosdir = fullfile(cwd,"..","..","macros","safunctions");
demosdir = [];
helptbx_helpupdate ( funmat , helpdir , macrosdir , demosdir , modulename , %t );

  
//
// Generate the test functions help pages
helpdir = fullfile(cwd,"testfuns");
funmat = [
  "nisp_ishigami"
  "nisp_ishigamisa"
  "nisp_sum"
  "nisp_sumsa"
  "nisp_product"
  "nisp_productsa"
  ];
macrosdir = fullfile(cwd,"..","..","macros","testfuns");
demosdir = [];
helptbx_helpupdate ( funmat , helpdir , macrosdir , demosdir , modulename , %t );
//
// Generate the static functions help from the pseudo macros
helpdir = fullfile(cwd,"nisp");
funmat = [
  "nisp_startup"
  "nisp_shutdown"
  "nisp_verboselevelset"
  "nisp_verboselevelget"
  "nisp_initseed"
  ];
macrosdir = fullfile(cwd,"nisp");
demosdir = [];
helptbx_helpupdate ( funmat , helpdir , macrosdir , demosdir , modulename , %t );
