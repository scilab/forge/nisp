// Copyright (C) 2013 - Michael Baudin
// Copyright (C) 2011 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

demopath = get_absolute_file_path("nisp.dem.gateway.sce");
subdemolist = [
"Randvar", "randvar/randvar.dem.gateway.sce"; ..
"Setrandvar", "setrandvar/setrandvar.dem.gateway.sce"; ..
"affine_SRC_scatter", "affine_SRC_scatter.sce"; ..
"ishigami_decomposition", "ishigami_decomposition.sce"; ..
"ishigami_hommasaltelli", "ishigami_hommasaltelli.sce"; ..
"polychaos_ishigami", "polychaos_ishigami.sce"; ..
"polychaos_ishigamimetamodel", "polychaos_ishigamimetamodel.sce"; ..
"polychaos_product", "polychaos_product.sce"; ..
"polychaos_product2", "polychaos_product2.sce"; ..
"product_hommasaltelli", "product_hommasaltelli.sce"; ..
"product_naive", "product_naive.sce"; ..
"product_naive2", "product_naive2.sce"; ..
"product_scatter", "product_scatter.sce"; ..
"product_scatter2", "product_scatter2.sce"; ..
"AxialStressedBeam" , "AxialStressedBeam.sce";..
"Laguerre polynomials", "ortho/laguerre.sce"; ..
];
subdemolist(:,2) = demopath + subdemolist(:,2)
