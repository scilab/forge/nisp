// Copyright (C) 2011 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

subdemolist = [
"demo_setrandvar1", "demo_setrandvar1.sce"; ..
"demo_setrandvar_lhs", "demo_setrandvar_lhs.sce"; ..
"demo_setrandvar_lhs2", "demo_setrandvar_lhs2.sce"; ..
"demo_setrandvar_lhsmaxmin", "demo_setrandvar_lhsmaxmin.sce"; ..
"demo_setrandvar_mc", "demo_setrandvar_mc.sce"; ..
"demo_setrandvar_petras", "demo_setrandvar_petras.sce"; ..
"demo_setrandvar_quadrature", "demo_setrandvar_quadrature.sce"; ..
"demo_setrandvar_Sobol", "demo_setrandvar_Sobol.sce"; ..
];


subdemolist(:,2) = demopath + "/setrandvar/" + subdemolist(:,2);

