// Copyright (C) 2009 - CEA - Jean-Marc Martinez
// Copyright (C) 2009-2011 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

//
// Test the chaos polynomial decomposition on the Ishigami function.
// Three uniform variables in [-pi,pi].
// Create a Petras sampling and compute the coefficients of the polynomial 
// by integration.
//
// Requires : Stixbox (histo)
//

// 3 random variable uniform in [-pi,pi]
a=7.;
b=0.1;
exact = nisp_ishigamisa ( a , b );

// 1. Create a group of stochastic variables
nx = 3;
srvx = setrandvar_new( nx );

// 2. Create a group of uncertain variables
rvu1 = randvar_new("Uniforme",-%pi,%pi);
rvu2 = randvar_new("Uniforme",-%pi,%pi);
rvu3 = randvar_new("Uniforme",-%pi,%pi);
srvu = setrandvar_new();
setrandvar_addrandvar ( srvu, rvu1);
setrandvar_addrandvar ( srvu, rvu2);
setrandvar_addrandvar ( srvu, rvu3);

// 3. Create a Petras sampling
degre = 9;
setrandvar_buildsample(srvx,"Petras",degre);
inputdata = setrandvar_buildsample( srvu , srvx );

// 4. Create the chaos polynomial
pc = polychaos_new ( srvx , 1 );
polychaos_setdegree(pc,degre);

// 5. Perform the experiments
np = setrandvar_getsize(srvu);
mprintf("Number of experiments = %d\n",np);
polychaos_setsizetarget(pc,np);
outputdata = nisp_ishigami(inputdata,a,b);
polychaos_settarget(pc,outputdata);

// 6. Compute the coefficients by integration
polychaos_computeexp(pc,srvx,"Integration");

// 7. Get the sensitivity indices
average = polychaos_getmean(pc);
var = polychaos_getvariance(pc);

// Print summary
polychaos_sasummary(pc);

// Consider the group {X1,X2}
groupe = [1 3];
polychaos_setgroupempty ( pc );
polychaos_setgroupaddvar ( pc , groupe(1) );
polychaos_setgroupaddvar ( pc , groupe(2) );
mprintf("Group {X1,X3}\n");
ST13=exact.S1+exact.S3+exact.S13;
mprintf("    ST13 =%f (expected=%f)\n",polychaos_getgroupind(pc),ST13);
mprintf("    S13  =%f (expected=%f)\n",polychaos_getgroupinter(pc),exact.S13);

// Compute the histogram of the output of the chaos polynomial.
polychaos_buildsample(pc,"Lhs",10000);
sample_output = polychaos_getsample(pc);
h = scf();
histo(sample_output,"Normalization","pdf")
xlabel("Y");
ylabel("Frequency");
xtitle("Ishigami - Histogram");
h.children.ticks_format(2)="%.2f";

// Plot the sensitivity indices.
for i=1:nx
  indexfirst(i)=polychaos_getindexfirst(pc,i);
  indextotal(i)=polychaos_getindextotal(pc,i);
end
h = scf();
bar(indextotal,0.2,'blue');
bar(indexfirst,0.15,'yellow');
legend(["Total" "First order"],pos=1);
xtitle("Ishigami - Sensitivity index");
h.children.ticks_format(2)="%.1f";
//
// Clean-up
//
randvar_destroy ( rvu1 );
randvar_destroy ( rvu2 );
randvar_destroy ( rvu3 );
setrandvar_destroy ( srvu );
polychaos_destroy ( pc );
setrandvar_destroy ( srvx );
//
// Load this script into the editor
//
filename = 'polychaos_ishigami.sce';
dname = get_absolute_file_path(filename);
editor ( dname + filename );

