// Copyright (C) 2008-2009 - INRIA - Michael Baudin
// Copyright (C) 2009 - Digiteo - Michael Baudin
//

// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

mprintf("Creating a Normale variable ...\n")
vu1 = randvar_new("Normale",1.0,0.5);
mprintf("Display the variable ...\n")
randvar_getlog(vu1); 
values = randvar_getvalue(vu1); 
mprintf("Generated number : %f\n", values)
mprintf("Generate 1000 numbers and check the mean and standard deviation ...\n")
nbshots = 1000; 
for i=1:nbshots
  values(i) = randvar_getvalue(vu1);
end
computed = mean (values);
mprintf("Mean is : %f (expected = 1.0)\n", computed)
computed = st_deviation (values);
mprintf("Standard deviation is : %f (expected = 0.5)\n", computed)
randvar_destroy(vu1);
//
// Load this script into the editor
//
filename = 'demo_randvar1.sce';
dname = get_absolute_file_path(filename);
editor ( dname + filename );

