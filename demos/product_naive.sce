// Copyright (C) 2009 - CEA - Jean-Marc Martinez
// Copyright (C) 2009-2011 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

//
// Compute the first order sensitivity indices, the naive way.
// Requires d*np^2 function evaluations, where np is the number of 
// experiments and d is the number of variables.
// Here np = 1000 and d = 2.
//

tic();

// First variable
// Normal
mu1 = 1.5;
sigma1 = 0.5;
// Second variable
// Normal
mu2 = 3.5;
sigma2 = 2.5;
exact = nisp_productsa ( [mu1,mu2]' , [sigma1,sigma2]' );

// 
// We apply the definition of the first order sensitivity 
// indice of the first variable is 
// S1 = V(E(y|x1))/V(y)
//
// 1. Compute the variable of y.
//
// Begin with np = 10.
// Serious computation is with np >= 1000.
// More than this cannot be done in practice with this method.
np = 100;
mprintf("Number of experiments : %d\n",1+2*np^2);

// Define a sampling for x1.
x1 = distfun_normrnd(mu1,sigma1,np,1);
// Define a sampling for x2.
x2 = distfun_normrnd(mu2,sigma2,np,1);
// Merge the two samplings
x = [x1 x2];
// Perform the experiments
y = nisp_product (x);
Ey= mean(y);
Vy= variance(y);
mprintf("Expectation y = %.5f (expectation = %.5f)\n", Ey, exact.expectation);
mprintf("Variance y = %.5f (expectation = %.5f)\n", Vy, exact.var);
//
// 2. Compute E(y|x1) for several values of x1.
// Define a sampling for x1.
x1 = distfun_normrnd(mu1,sigma1,np,1);
//
for i = 1 : np
  // For each value of x1, define a sampling for x2.
  x2 = distfun_normrnd(mu2,sigma2,np,1);
  // Merge the current value of x1, i.e. x1(i), 
  // with the sampling for x2.
  x = [x1(i)*ones(np,1) x2];
  // Perform the experiments
  y = nisp_product (x);
  // Store E(y|x1) in Ey_all(i)
  Ey_all(i) = mean(y);
end
//
// Compute S1
S1 = variance(Ey_all)/Vy;
mprintf("S1 = %.5f (expectation = %.5f)\n", S1, exact.S(1));
re = abs(S1- exact.S(1))/exact.S(1);
mprintf("  Relative Error = %f\n", re);
//
// 3. Compute E(y|x2) for several values of x2.
// Define a sampling for x2.
x2 = distfun_normrnd(mu2,sigma2,np,1);
//
for i = 1 : np
  // For each value of x2, define a sampling for x1.
  x1 = distfun_normrnd(mu1,sigma1,np,1);
  // Merge the current value of x2, i.e. x2(i), 
  // with the sampling for x1.
  x = [x1 x2(i)*ones(np,1)];
  // Perform the experiments
  y = nisp_product (x);
  // Store E(y|x2) in Ey_all(i)
  Ey_all(i) = mean(y);
end
//
// Compute S1
S2 = variance(Ey_all)/Vy;
mprintf("S2 = %.5f (expectation = %.5f)\n", S2, exact.S(2));
re = abs(S2- exact.S(2))/exact.S(2);
mprintf("  Relative Error = %f\n", re);

S12 = 1 - S1 - S2;
exactS12=exact.ST(1)-exact.S(1);
mprintf("S12 = %.5f (expectation = %.5f)\n", S12, exactS12);


t = toc();
mprintf("Time = %f (s)\n", t);
//
// Load this script into the editor
//
filename = 'product_naive.sce';
dname = get_absolute_file_path(filename);
editor ( dname + filename );

