// Copyright (C) 2013 - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Reference
// http://en.wikipedia.org/wiki/nisp_laguerrepolynomials

function nisp_demolaguerre()

    mprintf("Check the accuracy of Ln(x), evaluated from the polynomial,\n")
    mprintf("or from the recurrence.\n");
    mprintf("nisp_laguerreeval is more accurate than nisp_laguerrepoly/horner\n")
    //
    // With various x, n
    R=[];
    nmax=100;
    xmax=21;
    i=0;
    j=0;
    for x=1:4:xmax
        i=i+1;
        j=0;
        for n=1:10:nmax
            j=j+1;
            L=nisp_laguerrepoly(n);
            y1=horner(L,x);
            y2=nisp_laguerreeval(x,n);
            d=assert_computedigits(y1,y2,2);
            d=floor(d);
            R(i,j)=d;
        end
    end
    // Create a bubble chart for the data
    h=scf();
    xtitle("Area=Number of digits","x","n");
    bubblematrix(1:4:xmax,1:10:nmax,R,-2,1.);
endfunction
nisp_demolaguerre();
clear nisp_demolaguerre
