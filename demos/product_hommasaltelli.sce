// Copyright (C) 2009 - CEA - Jean-Marc Martinez
// Copyright (C) 2009-2011 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

//
// Compute the first order sensitivity indices of the product x1*x2 function.
// Uses the Sobol, Homma, Saltelli method.
// Two normal variables in [-pi,pi], mu1=2.5, sigma1=0.5, mu2=3.5, sigma2=2.5.
// Uses Monte-Carlo experiments to compute the sensitivity indices.
//

tic();

function y = Exemple (x)
  // Returns the output y of the product x1 * x2.
  // Parameters
  // x: a np-by-nx matrix of doubles, where np is the number of experiments, and nx=2.
  // y: a np-by-1 matrix of doubles
  y(:,1) = x(:,1).* x(:,2)
endfunction

// First variable
// Normal
mu1 = 1.5;
sigma1 = 0.5;
// Second variable
// Normal
mu2 = 3.5;
sigma2 = 2.5;

// Begin with np = 10.
// Serious computation is with np > 100000.
np = 100000;
mprintf("Number of experiments : %d\n",3*np);

//
// 1. We create a first design A
//
// Define a sampling for x1.
x1 = distfun_normrnd(mu1,sigma1,np,1);
// Define a sampling for x2.
x2 = distfun_normrnd(mu2,sigma2,np,1);
// Merge the two samplings
A = [x1 x2];
//
// 2. We create a first design B
//
// Define a sampling for x1.
x1 = distfun_normrnd(mu1,sigma1,np,1);
// Define a sampling for x2.
x2 = distfun_normrnd(mu2,sigma2,np,1);
// Merge the two samplings
B = [x1 x2];

//
// 3. Perform the first design A
//
// Perform the experiments
yA = Exemple (A);
Ey= mean(yA);
Ey_expectation= mu1*mu2;
Vy= variance(yA);
Vy_expectation = mu2^2*sigma1^2 + mu1^2*sigma2^2 + sigma1^2*sigma2^2;
mprintf("Expectation y = %.5f (expectation = %.5f)\n", Ey, Ey_expectation);
mprintf("Variance y = %.5f (expectation = %.5f)\n", Vy, Vy_expectation);

//
// 4. Compute S1
//
C=[A(:,1) B(:,2)];
yC1 = Exemple (C);
S1 = ( (yA'*yC1)/np - Ey^2 ) / Vy;
S1_expectation = ( mu2^2*sigma1^2 ) / Vy_expectation;
mprintf("First order sensitivity / x1 = %.5f (expectation = %.5f)\n", S1, S1_expectation);
re = abs(S1- S1_expectation)/S1_expectation;
mprintf("  Relative Error = %f\n", re);
//
// 5. Compute S2
//
C=[A(:,2) B(:,1)];
yC2 = Exemple (C);
S2 = ( (yA'*yC2)/np - Ey^2 ) / Vy;
S2_expectation = ( mu1^2*sigma2^2 ) / Vy_expectation;
mprintf("First order sensitivity / x2 = %.5f (expectation = %.5f)\n", S2, S2_expectation);
re = abs(S2- S2_expectation)/S2_expectation;
mprintf("  Relative Error = %f\n", re);


t = toc();
mprintf("Time = %f (s)\n", t);
//
// Load this script into the editor
//
filename = 'product_hommasaltelli.sce';
dname = get_absolute_file_path(filename);
editor ( dname + filename );

