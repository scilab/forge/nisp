// Copyright (C) 2009 - CEA - Jean-Marc Martinez
// Copyright (C) 2009-2011 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

//
// Compute the first order sensitivity indices, the naive way.
// Requires np+d*np^2 function evaluations, where np is the number of 
// experiments and d is the number of variables.
// Here np = 1000 and d = 2.
// This version is vectorized and faster, but requires lots of 
// memory.
//

// First variable
// Normal
mu1 = 1.5;
sigma1 = 0.5;
// Second variable
// Normal
mu2 = 3.5;
sigma2 = 2.5;
exact = nisp_productsa ( [mu1,mu2]' , [sigma1,sigma2]' );

function x=myrandgen(n, i,mu1,sigma1,mu2,sigma2)
    if (i==1) then
        x = distfun_normrnd(mu1,sigma1,n,1);
    else
        x = distfun_normrnd(mu2,sigma2,n,1);
    end
endfunction
nx=2;
n=100;
[s,nbevalf]=nisp_bruteforcesa(nisp_product,nx,..
list(myrandgen,mu1,sigma1,mu2,sigma2),n);
mprintf("Number of function evaluations:%d\n",nbevalf);
mprintf("Computed indices:\n")
disp(s)
mprintf("Exact indices\n");
disp(exact.S)
//
// Load this script into the editor
//
filename = 'product_naive2.sce';
dname = get_absolute_file_path(filename);
editor ( dname + filename );

