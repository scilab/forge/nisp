// Copyright (C) 2012 - Michael Baudin
// Copyright (C) 2009 - INRIA - Michael Baudin
// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
function nispBuildGateway()
    sci_gateway_dir=get_absolute_file_path("builder_gateway.sce");
    tbx_builder_gateway_lang("cpp",sci_gateway_dir);
    tbx_build_gateway_loader("cpp",sci_gateway_dir);
    tbx_build_gateway_clean("cpp",sci_gateway_dir);
endfunction 
nispBuildGateway();
clear nispBuildGateway
