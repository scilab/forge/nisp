
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
// Copyright (C) 2009 - Digiteo - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license
// http://www.gnu.org/copyleft/lesser.html

//
// gw_nisp.h
//   Header for the nisp gateway.
//
#ifndef __SCI_GW_NISP_H__
#define __SCI_GW_NISP_H__

extern "C" {

	// Functions providing interfaces to Scilab functions
	int sci_nisp_initseed (char *fname,unsigned long fname_len);
	int sci_nisp_startup (char *fname,unsigned long fname_len);
	int sci_nisp_shutdown (char *fname,unsigned long fname_len);
	int sci_nisp_verboselevelset (char *fname,unsigned long fname_len);
	int sci_nisp_verboselevelget (char *fname,unsigned long fname_len);
	
	// Randvar
	int sci_randvar_tokens (char *fname,unsigned long fname_len);
	int sci_randvar_size (char *fname,unsigned long fname_len);
	int sci_randvar_new (char *fname,unsigned long fname_len);
	int sci_randvar_getvalue (char *fname,unsigned long fname_len);
	int sci_randvar_getlog (char *fname,unsigned long fname_len);
	int sci_randvar_destroy (char *fname,unsigned long fname_len);
	
	// Setrandvar
	int sci_setrandvar_tokens (char *fname,unsigned long fname_len);
	int sci_setrandvar_size (char *fname,unsigned long fname_len);
	int sci_setrandvar_new (char *fname,unsigned long fname_len);
	int sci_setrandvar_freememory (char *fname,unsigned long fname_len);
	int sci_setrandvar_destroy (char *fname,unsigned long fname_len);
	int sci_setrandvar_addrandvar (char *fname,unsigned long fname_len);
	int sci_setrandvar_getlog (char *fname,unsigned long fname_len);
	int sci_setrandvar_getdimension (char *fname,unsigned long fname_len);
	int sci_setrandvar_getsize (char *fname,unsigned long fname_len);
	int sci_setrandvar_getsample (char *fname,unsigned long fname_len);
	int sci_setrandvar_setsample (char *fname,unsigned long fname_len);
	int sci_setrandvar_setsamplesize (char *fname,unsigned long fname_len);
	int sci_setrandvar_setsampletype (char *fname,unsigned long fname_len);
	int sci_setrandvar_save (char *fname,unsigned long fname_len);
	int sci_setrandvar_buildsample (char *fname,unsigned long fname_len);
	
	// Polychaos
	int sci_polychaos_new (char *fname,unsigned long fname_len);
	int sci_polychaos_destroy (char *fname, unsigned long fname_len);
	int sci_polychaos_tokens (char *fname, unsigned long fname_len);
	int sci_polychaos_size (char *fname, unsigned long fname_len);
	int sci_polychaos_setdegree (char *fname,unsigned long fname_len);
	int sci_polychaos_getdegree (char *fname,unsigned long fname_len);
	int sci_polychaos_freememory (char *fname,unsigned long fname_len);
	int sci_polychaos_setdimoutput (char *fname, unsigned long fname_len);
	int sci_polychaos_getdimoutput (char *fname, unsigned long fname_len);
	int sci_polychaos_setsizetarget (char *fname, unsigned long fname_len);
	int sci_polychaos_getsizetarget (char *fname,unsigned long fname_len);
	int sci_polychaos_settarget (char *fname, unsigned long fname_len);
	int sci_polychaos_gettarget (char *fname,unsigned long fname_len);
	int sci_polychaos_freememtarget (char *fname, unsigned long fname_len);
	int sci_polychaos_getdiminput (char *fname, unsigned long fname_len);
	int sci_polychaos_getdimexp (char *fname, unsigned long fname_len);
	int sci_polychaos_getlog (char *fname,unsigned long fname_len);
	int sci_polychaos_computeexp (char *fname,unsigned long fname_len);
	int sci_polychaos_getmean (char *fname,unsigned long fname_len);
	int sci_polychaos_getvariance (char *fname,unsigned long fname_len);
	int sci_polychaos_getcovariance (char *fname,unsigned long fname_len);
	int sci_polychaos_getcorrelation (char *fname,unsigned long fname_len);
	int sci_polychaos_getindexfirst (char *fname, unsigned long fname_len);
	int sci_polychaos_getindextotal (char *fname, unsigned long fname_len);
	int sci_polychaos_getmultind (char *fname,unsigned long fname_len);
	int sci_polychaos_getgroupind (char *fname,unsigned long fname_len);
	int sci_polychaos_getgroupinter (char *fname,unsigned long fname_len);
	int sci_polychaos_getinvquantile (char *fname,unsigned long fname_len);
	int sci_polychaos_buildsample (char *fname,unsigned long fname_len);
	int sci_polychaos_getquantile (char *fname,unsigned long fname_len);
	int sci_polychaos_getquantwilks (char *fname,unsigned long fname_len);
	int sci_polychaos_getsample (char *fname,unsigned long fname_len);
	int sci_polychaos_setgroupempty (char *fname,unsigned long fname_len);
	int sci_polychaos_setgroupaddvar (char *fname,unsigned long fname_len);
	int sci_polychaos_printgroup (char *fname,unsigned long fname_len);
	int sci_polychaos_getgroup (char *fname,unsigned long fname_len);
	int sci_polychaos_computeexp (char *fname,unsigned long fname_len);
	int sci_polychaos_getanova (char *fname,unsigned long fname_len);
	int sci_polychaos_getanovaord (char *fname, unsigned long fname_len);
	int sci_polychaos_getanovaordco (char *fname, unsigned long fname_len);
	int sci_polychaos_realisation (char *fname,unsigned long fname_len);
	int sci_polychaos_save (char *fname,unsigned long fname_len);
	int sci_polychaos_generatecode (char *fname,unsigned long fname_len);
	int sci_polychaos_readtarget (char *fname,unsigned long fname_len);
	int sci_polychaos_compute (char *fname,unsigned long fname_len);

/* ==================================================================== */



}
#endif /* !__SCI_GW_NISP_H__ */
