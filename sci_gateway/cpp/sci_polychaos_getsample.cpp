
// Copyright (C) 2008 - INRIA - Michael Baudin
// Copyright (C) 2009 - Digiteo - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

extern "C" {
#include "api_scilab.h"
#include "Scierror.h"
#include "localization.h"
#include "gw_nisp.h"
}


/* ==================================================================== */

#include "nisp_gwsupport.hxx" 
#include "nisp_PolynomialChaos_map.hxx" 

/* ==================================================================== */
// 
// polychaos_getsample ( token , k , ovar )
//   returns the sample of experiment #k, where 1<= k<= np,
//   for output variable #ovar, where 1<= ovar<= ny
// polychaos_getsample ( token , k )
//   returns the sample of experiment #k, where 1<= k<= np,
//     as a (1) x ny matrix
// polychaos_getsample ( token )
//   returns the sampling as a (np) x ny matrix
//
int sci_polychaos_getsample (char *fname,unsigned long fname_len)
{
	int token;
	double *pdblFinalVar = NULL; //SCILAB return Var
	PolynomialChaos * pc = NULL;
	int ovar;
	int k;
	int ny;
	int np;

	CheckInputArgument(pvApiCtx,1,3) ;
	CheckOutputArgument(pvApiCtx,0,1) ;
	nispgw_gettoken ( fname , 1 , &token,pvApiCtx);
	if ( (Rhs==2) | (Rhs==3) ) {
		nispgw_GetOneInteger ( fname , 2 , &k, pvApiCtx);
	}
	if (Rhs==3) {
		nispgw_GetOneInteger ( fname , 3 , &ovar, pvApiCtx);
	}
	nispgw_token2Polychaos ( fname , 1 , token , &pc );
	if (Rhs==1) {
		// polychaos_getsample ( token )
		ny = pc->GetDimensionOutput();
		np = pc->GetSampleSize( );
		allocMatrixOfDouble(pvApiCtx, Rhs+1, np, ny, &pdblFinalVar);
		for(int j=1;j<=ny;j++) {
			for(int k=1;k<=np;k++) {
				*(pdblFinalVar + (j-1) * np + (k-1)) = pc-> GetSample ( k , j );
			}
		}
	} else if (Rhs==2) {
		// polychaos_getquantile ( token , k )
		ny = pc-> GetDimensionOutput();
		allocMatrixOfDouble(pvApiCtx, Rhs+1, 1, ny, &pdblFinalVar);
		for(int j=1;j<=ny;j++) {
			pdblFinalVar[j-1]= pc-> GetSample ( k , j );
		}
	} else if (Rhs==3) {
		// polychaos_getsample ( token , k , ovar )
		allocMatrixOfDouble(pvApiCtx, Rhs+1, 1, 1, &pdblFinalVar);
		pdblFinalVar[0] = pc-> GetSample ( k , ovar );
	}
	LhsVar(1) = Rhs+1;
	return 0;
}
/* ==================================================================== */

