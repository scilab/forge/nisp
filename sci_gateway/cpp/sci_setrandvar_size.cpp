
// Copyright (C) 2008 - INRIA - Michael Baudin
// Copyright (C) 2009 - Digiteo - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

extern "C" {
#include "api_scilab.h"
#include "Scierror.h"
#include "localization.h"
#include "gw_nisp.h"
}

/* ==================================================================== */

#include "nisp_gwsupport.hxx" 
#include "nisp_SetRandomVariable_map.hxx" 


/* ==================================================================== */
// 
// nb = setrandvar_size () 
//   returns the number of objects currently in use
//
int sci_setrandvar_size (char *fname, unsigned long fname_len)
{
	int size;

	CheckInputArgument(pvApiCtx,0,0) ;
	CheckOutputArgument(pvApiCtx,0,1) ;
	size = nisp_SetRandomVariable_map_size ();
	nispgw_CreateLhsInteger ( 1 , size, pvApiCtx);
	return 0;
}
/* ==================================================================== */

