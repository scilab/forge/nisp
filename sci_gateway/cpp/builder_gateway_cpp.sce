// ====================================================================
// Copyright (C) 2012 - Michael Baudin
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
// Copyright (C) 2009 - Digiteo - Michael Baudin
// This file is released into the public domain
// ====================================================================

function nispBuildGatewayCpp()
    gateway_path = get_absolute_file_path("builder_gateway_cpp.sce");

    libname = "nispgateway";
    namelist = [
    "randvar_new" "sci_randvar_new"
    "randvar_destroy" "sci_randvar_destroy"
    "randvar_size" "sci_randvar_size"
    "randvar_tokens" "sci_randvar_tokens"
    "randvar_getlog" "sci_randvar_getlog"
    "randvar_getvalue" "sci_randvar_getvalue"
    "nisp_startup" "sci_nisp_startup"
    "nisp_shutdown" "sci_nisp_shutdown"
    "nisp_verboselevelset" "sci_nisp_verboselevelset"
    "nisp_verboselevelget" "sci_nisp_verboselevelget"
    "nisp_initseed" "sci_nisp_initseed"
    "setrandvar_new" "sci_setrandvar_new"
    "setrandvar_tokens" "sci_setrandvar_tokens"
    "setrandvar_size" "sci_setrandvar_size"
    "setrandvar_destroy" "sci_setrandvar_destroy"
    "setrandvar_freememory" "sci_setrandvar_freememory"
    "setrandvar_addrandvar" "sci_setrandvar_addrandvar"
    "setrandvar_getlog" "sci_setrandvar_getlog"
    "setrandvar_getdimension" "sci_setrandvar_getdimension"
    "setrandvar_getsize" "sci_setrandvar_getsize"
    "setrandvar_buildsample" "sci_setrandvar_buildsample"
    "polychaos_new" "sci_polychaos_new"
    "polychaos_destroy" "sci_polychaos_destroy"
    "polychaos_tokens" "sci_polychaos_tokens"
    "polychaos_size" "sci_polychaos_size"
    "polychaos_setdegree" "sci_polychaos_setdegree"
    "polychaos_getdegree" "sci_polychaos_getdegree"
    "polychaos_freememory" "sci_polychaos_freememory"
    "polychaos_getdimoutput" "sci_polychaos_getdimoutput"
    "polychaos_setdimoutput" "sci_polychaos_setdimoutput"
    "polychaos_getsizetarget" "sci_polychaos_getsizetarget"
    "polychaos_setsizetarget" "sci_polychaos_setsizetarget"
    "polychaos_freememtarget" "sci_polychaos_freememtarget"
    "polychaos_gettarget" "sci_polychaos_gettarget"
    "polychaos_settarget" "sci_polychaos_settarget"
    "polychaos_getdiminput" "sci_polychaos_getdiminput"
    "polychaos_getdimexp" "sci_polychaos_getdimexp"
    "polychaos_getlog" "sci_polychaos_getlog"
    "polychaos_computeexp" "sci_polychaos_computeexp"
    "polychaos_getmean" "sci_polychaos_getmean"
    "polychaos_getvariance" "sci_polychaos_getvariance"
    "polychaos_getcovariance" "sci_polychaos_getcovariance"
    "polychaos_getcorrelation" "sci_polychaos_getcorrelation"
    "polychaos_getindexfirst" "sci_polychaos_getindexfirst"
    "polychaos_getindextotal" "sci_polychaos_getindextotal"
    "polychaos_getmultind" "sci_polychaos_getmultind"
    "polychaos_printgroup" "sci_polychaos_printgroup"
    "polychaos_getgroup" "sci_polychaos_getgroup"
    "polychaos_getgroupind" "sci_polychaos_getgroupind"
    "polychaos_getgroupinter" "sci_polychaos_getgroupinter"
    "polychaos_getinvquantile" "sci_polychaos_getinvquantile"
    "polychaos_buildsample" "sci_polychaos_buildsample"
    "polychaos_getquantile" "sci_polychaos_getquantile"
    "polychaos_getquantwilks" "sci_polychaos_getquantwilks"
    "polychaos_getsample" "sci_polychaos_getsample"
    "polychaos_readtarget" "sci_polychaos_readtarget"
    "polychaos_setgroupempty" "sci_polychaos_setgroupempty"
    "polychaos_setgroupaddvar" "sci_polychaos_setgroupaddvar"
    "polychaos_getanova" "sci_polychaos_getanova"
    "polychaos_getanovaord" "sci_polychaos_getanovaord"
    "polychaos_getanovaordco" "sci_polychaos_getanovaordco"
    "polychaos_realisation" "sci_polychaos_realisation"
    "polychaos_save" "sci_polychaos_save"
    "polychaos_generatecode" "sci_polychaos_generatecode"
    "polychaos_compute" "sci_polychaos_compute"
    ];
    files = [
    "nisp_gettoken.cpp"  
    "nisp_gwsupport.cpp"
    "nisp_PolynomialChaos_map.cpp"  
    "nisp_RandomVariable_map.cpp"  
    "nisp_SetRandomVariable_map.cpp"  
    "sci_nisp_startup.cpp"
    "sci_nisp_shutdown.cpp"
    "sci_nisp_verboselevelset.cpp"
    "sci_nisp_verboselevelget.cpp"
    "sci_nisp_initseed.cpp"
    "sci_randvar_new.cpp"
    "sci_randvar_destroy.cpp"
    "sci_randvar_size.cpp"
    "sci_randvar_tokens.cpp"
    "sci_randvar_getlog.cpp"
    "sci_randvar_getvalue.cpp"
    "sci_setrandvar_new.cpp"
    "sci_setrandvar_tokens.cpp"
    "sci_setrandvar_size.cpp"
    "sci_setrandvar_destroy.cpp"
    "sci_setrandvar_freememory.cpp"
    "sci_setrandvar_addrandvar.cpp"
    "sci_setrandvar_getlog.cpp"
    "sci_setrandvar_getdimension.cpp"
    "sci_setrandvar_getsize.cpp"
    "sci_setrandvar_buildsample.cpp"
    "sci_polychaos_new.cpp"
    "sci_polychaos_destroy.cpp"
    "sci_polychaos_tokens.cpp"
    "sci_polychaos_size.cpp"
    "sci_polychaos_setdegree.cpp"
    "sci_polychaos_getdegree.cpp"
    "sci_polychaos_freememory.cpp"
    "sci_polychaos_getdimoutput.cpp"
    "sci_polychaos_setdimoutput.cpp"
    "sci_polychaos_getsizetarget.cpp"
    "sci_polychaos_setsizetarget.cpp"
    "sci_polychaos_freememtarget.cpp"
    "sci_polychaos_settarget.cpp"
    "sci_polychaos_gettarget.cpp"
    "sci_polychaos_getdiminput.cpp"
    "sci_polychaos_getdimexp.cpp"
    "sci_polychaos_getlog.cpp"
    "sci_polychaos_computeexp.cpp"
    "sci_polychaos_getmean.cpp"
    "sci_polychaos_getvariance.cpp"
    "sci_polychaos_getcovariance.cpp"
    "sci_polychaos_getcorrelation.cpp"
    "sci_polychaos_getindexfirst.cpp"
    "sci_polychaos_getindextotal.cpp"
    "sci_polychaos_getmultind.cpp"
    "sci_polychaos_printgroup.cpp"
	"sci_polychaos_getgroup.cpp"
    "sci_polychaos_getgroupind.cpp"
    "sci_polychaos_setgroupempty.cpp"
    "sci_polychaos_getgroupinter.cpp"
    "sci_polychaos_getinvquantile.cpp"
    "sci_polychaos_buildsample.cpp"
    "sci_polychaos_getquantile.cpp"
    "sci_polychaos_getquantwilks.cpp"
    "sci_polychaos_getsample.cpp"
    "sci_polychaos_setgroupaddvar.cpp"
    "sci_polychaos_getanova.cpp"
    "sci_polychaos_getanovaord.cpp"
    "sci_polychaos_getanovaordco.cpp"
    "sci_polychaos_realisation.cpp"
    "sci_polychaos_save.cpp"
    "sci_polychaos_generatecode.cpp"
    "sci_polychaos_readtarget.cpp"
    "sci_polychaos_compute.cpp"
    ];
    ldflags = ""

    if ( getos() == "Windows" ) then
        include2 = "../../src/includes";
        include3 = SCI+"/modules/output_stream/includes";
        cflags = "-DWIN32 -I"""+include2+..
        """ -I"""+include3+"""";
    else
        include1 = gateway_path;
        include2 = gateway_path+"../../src/includes";
        include3 = SCI+"/../../include/scilab/localization";
        include4 = SCI+"/../../include/scilab/output_stream";
        include5 = SCI+"/../../include/scilab/core";
        cflags = "-I"""+include1+..
        """ -I"""+include2+..
        """ -I"""+include3+...
        """ -I"""+include4+..
        """ -I"""+include5+"""";
    end
    // Caution : the order matters !
    libs = [
    "../../src/thirdparty/libthirdparty"
    "../../src/cpp/libnisp"
    ];

    tbx_build_gateway(libname, namelist, files, gateway_path, ..
    libs, ldflags, cflags);

endfunction 
nispBuildGatewayCpp();
clear nispBuildGatewayCpp

