
// Copyright (C) 2008 - INRIA - Michael Baudin
// Copyright (C) 2009 - Digiteo - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

extern "C" {
#include "api_scilab.h"
#include "Scierror.h"
#include "localization.h"
#include "gw_nisp.h"
}


/* ==================================================================== */

#include "nisp_gwsupport.hxx" 
#include "nisp_PolynomialChaos_map.hxx" 


/* ==================================================================== */
// 
// output = polychaos_compute ( token , input )
//   computes the output y with size 1-by-ny, depending on the input, with size 1-by-nx.
//   
//
int sci_polychaos_compute (char *fname,unsigned long fname_len)
{
	int nRows, nCols;
	int token;
	double *pdblFinalVar = NULL; //SCILAB return Var
	PolynomialChaos * pc = NULL;
	double * inputvector;
	int nx;
	int ny;
	double *input= NULL;
	double *output= NULL;

	CheckInputArgument(pvApiCtx,2,2) ;
	CheckOutputArgument(pvApiCtx,0,1) ;
	//
	// Get the token
	nispgw_gettoken ( fname , 1 , &token,pvApiCtx);
	nispgw_token2Polychaos ( fname , 1 , token , &pc );
	nx = pc-> GetDimensionInput();
	ny = pc-> GetDimensionOutput();
	//
	// Get and check the input
	nispgw_AssertVarType(fname , 2 , sci_matrix, pvApiCtx);
	int*piAddr;	
	getVarAddressFromPosition(pvApiCtx,2,&piAddr);
	//GetRhsVarMatrixDouble ( 2, &nRows, &nCols, &inputvector);
	getMatrixOfDouble(pvApiCtx,piAddr,&nRows,&nCols,&inputvector);	
	nispgw_AssertNRows ( fname , 2 , nRows , 1 );
	nispgw_AssertNColumns ( fname , 2 , nCols , nx );
	//
	// Allocate a double * and put the input in it.
	input = new double [nx+1];
	for (int i = 1; i<= nx; i++) {
		input[i] = inputvector[i-1];
	}
	//
	// Call ComputeOutput
	output = new double [ny+1];
	pc-> Compute(input,output);
	//
	// Put the result into the output argument
	nRows = 1;
	nCols = ny;
	allocMatrixOfDouble(pvApiCtx,Rhs+1, nRows, nCols, &pdblFinalVar);
	for(int j = 1; j <= ny; j++) {
		*(pdblFinalVar + (j-1)) = output[j];
	}
	LhsVar(1) = Rhs+1;
	return 0;
}
/* ==================================================================== */

