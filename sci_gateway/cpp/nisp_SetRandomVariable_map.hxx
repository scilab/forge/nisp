
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license
// http://www.gnu.org/copyleft/lesser.html

//
// nisp_SetRandomVariable_map.h
//   Header for nisp_SetRandomVariable_map.cpp
//
#ifndef __NISP_SETRANDOMVARIABLE_MAP_H__
#define __NISP_SETRANDOMVARIABLE_MAP_H__


#undef __BEGIN_DECLS
#undef __END_DECLS
#ifdef __cplusplus
# define __BEGIN_DECLS extern "C" {
# define __END_DECLS }
#else
# define __BEGIN_DECLS /* empty */
# define __END_DECLS /* empty */
#endif

#include "nisp_gva.h"

__BEGIN_DECLS

/**
 * Add a new SetRandomVariable in the map
 * @return the token (integer) which identifies the SetRandomVariable
 */
 int nisp_SetRandomVariable_map_add ( SetRandomVariable * srv );

/**
 * Returns the object associated with the given token.
 * @return the SetRandomVariable
 */
 SetRandomVariable * nisp_SetRandomVariable_map_getobject ( int token );

/**
 * Remove a SetRandomVariable from the map.
 * @param[in] token : the token of the current object
 */
 void nisp_SetRandomVariable_map_remove ( int token );

/**
 * Returns the number of Objects in the map
 */
 int nisp_SetRandomVariable_map_size ();

/**
 * Fill in the tokens array the list of tokens which are currently in use.
 */
 void nisp_SetRandomVariable_map_tokens ( int * tokens );

__END_DECLS

#endif /* !__NISP_SETRANDOMVARIABLE_MAP_H__ */