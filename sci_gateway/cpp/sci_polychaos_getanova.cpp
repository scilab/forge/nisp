
// Copyright (C) 2008 - INRIA - Michael Baudin
// Copyright (C) 2009 - Digiteo - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

extern "C" {
#include "api_scilab.h" 
#include "Scierror.h"
#include "localization.h"
#include "gw_nisp.h"
}


/* ==================================================================== */

#include "nisp_gwsupport.hxx" 
#include "nisp_PolynomialChaos_map.hxx" 


/* ==================================================================== */
// 
// polychaos_getanova ( token )
//   prints out the anova
// polychaos_getanova ( token , r )
//   prints out the anova corresponding to r (TODO:  what is r ?)
//
int sci_polychaos_getanova (char *fname, unsigned long fname_len)
{
	int token;
	PolynomialChaos * pc = NULL;
	int r;

	CheckInputArgument(pvApiCtx,1,2) ;
	CheckOutputArgument(pvApiCtx,0,1) ;
	nispgw_gettoken ( fname , 1 , &token, pvApiCtx);
	if (Rhs==2) {
		nispgw_GetOneInteger ( fname , 2 , &r, pvApiCtx);
	}
	nispgw_token2Polychaos ( fname , 1 , token , &pc );
	if (Rhs==1) {
		pc -> GetAnova ( );
	} else if (Rhs==2) {
		pc -> GetAnova ( r );
	}
	nispgw_CreateLhsInteger ( 1 , token, pvApiCtx);
	return 0;
}
/* ==================================================================== */

