// Copyright (C) 2015 - Michael Baudin
// Copyright (C) 2008 - INRIA - Michael Baudin
// Copyright (C) 2009 - Digiteo - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

extern "C" {
#include "api_scilab.h"
#include "Scierror.h"
#include "localization.h"
#include "gw_nisp.h"
#include <stdio.h>
}

/* ==================================================================== */

#include "nisp_gwsupport.hxx" 
#include "nisp_PolynomialChaos_map.hxx" 

/* ==================================================================== */
// 
// sci_polychaos_printgroup ( token )
//   Print the current group of variables
//
int sci_polychaos_printgroup (char *fname,unsigned long fname_len)
{
	int token;
	double *pdblFinalVar = NULL; //SCILAB return Var
	PolynomialChaos * pc = NULL;
	printf("flag1\n");
	CheckInputArgument(pvApiCtx,1,1) ;
	CheckOutputArgument(pvApiCtx,0,1) ;
	printf("flag2\n");
	nispgw_gettoken ( fname , 1 , &token, pvApiCtx);
	printf("flag3");
	nispgw_token2Polychaos ( fname , 1 , token , &pc );
	printf("flag4\n");
	pc->PrintGroup();
	printf("flag5\n");
	//LhsVar(1) = Rhs+1;
	printf("flag6\n");
	return 0;
}
/* ==================================================================== */

