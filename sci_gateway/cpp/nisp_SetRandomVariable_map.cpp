
// Copyright (C) 2008 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html


//
// nisp_SetRandomVariable_map.cpp --
//   A map to manage SetRandomVariable from NISP for the tbxnisp Scilab Toolbox
//

#include <stdlib.h>
#include <map>

#include "nisp_gva.h"

#include "nisp_SetRandomVariable_map.hxx"
#include "nisp_RandomVariable_map.hxx"

using namespace std;
typedef map<int , SetRandomVariable *> nisp_SetRandomVariable_map_type;
nisp_SetRandomVariable_map_type nisp_SetRandomVariable_map;
int SetRandomVariable_counter = 0;

int nisp_SetRandomVariable_map_add ( SetRandomVariable * srv )
{
	int token;

	token = SetRandomVariable_counter;
	SetRandomVariable_counter = SetRandomVariable_counter + 1;
	nisp_SetRandomVariable_map[token] = srv;
	return token;
}

void nisp_SetRandomVariable_map_remove ( int token )
{
	nisp_SetRandomVariable_map_type::iterator it;
	it = nisp_SetRandomVariable_map.find (token);
	nisp_SetRandomVariable_map.erase(it);
}

SetRandomVariable * nisp_SetRandomVariable_map_getobject ( int token )
{
	SetRandomVariable * srv = NULL;
	if ( nisp_SetRandomVariable_map.size()!=0 ) {
		nisp_SetRandomVariable_map_type::iterator it;
		it = nisp_SetRandomVariable_map.find (token);
		if ( it!= nisp_SetRandomVariable_map.end()) {
			srv = it->second;
		}
	}
	return srv;
}

int nisp_SetRandomVariable_map_size ()
{
	return nisp_SetRandomVariable_map.size();
}
void nisp_SetRandomVariable_map_tokens (int * tokens)
{
	int index = 0;
	int token;
	for(nisp_SetRandomVariable_map_type::iterator it = nisp_SetRandomVariable_map.begin(); it != nisp_SetRandomVariable_map.end(); ++it)
	{
		token = it->first;
		tokens[index] = token;
		index ++;
	}
}