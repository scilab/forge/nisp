
// Copyright (C) 2008 - INRIA - Michael Baudin
// Copyright (C) 2009 - Digiteo - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

extern "C" {
#include "api_scilab.h"
#include "Scierror.h"
#include "localization.h"
#include "gw_nisp.h"
}


/* ==================================================================== */

#include "nisp_gwsupport.hxx" 
#include "nisp_PolynomialChaos_map.hxx" 


/* ==================================================================== */
// 
// polychaos_getquantwilks ( token , walpha , wbeta , ovar )
//   returns the Wilks quantile of order alpha for output variable #ovar, where 1<= ovar<= ny
// polychaos_getquantwilks ( token , walpha , wbeta )
//   returns the Wilksquantiles of order alpha for all output variables, 
//     as a (1) x ny matrix
//
int sci_polychaos_getquantwilks (char *fname, unsigned long fname_len)
{
	int nRows, nCols;
	int token;
	double *pdblFinalVar = NULL; //SCILAB return Var
	PolynomialChaos * pc = NULL;
	int ovar;
	double alpha;
	int ny;
	double beta;

	CheckInputArgument(pvApiCtx,3,4) ;
	CheckOutputArgument(pvApiCtx,0,1) ;
	nispgw_gettoken ( fname , 1 , &token, pvApiCtx);
	nispgw_GetOneDouble ( fname , 2 , &alpha, pvApiCtx);
	nispgw_GetOneDouble ( fname , 3 , &beta, pvApiCtx);
	if (Rhs==4) {
		nispgw_GetOneInteger ( fname , 4 , &ovar, pvApiCtx);
	}
	nispgw_token2Polychaos ( fname , 1 , token , &pc );
	if (Rhs==3) {
		// polychaos_getquantwilks ( token , walpha , wbeta )
		ny = pc-> GetDimensionOutput();
		// Returns the output as result
		nRows=1;
		nCols=ny;
		allocMatrixOfDouble(pvApiCtx, Rhs+1, nRows, nCols, &pdblFinalVar);
		pc-> GetQuantileWilks ( alpha , beta , pdblFinalVar );
	} else if (Rhs==4) {
		// polychaos_getquantwilks ( token , walpha , wbeta , ovar )
		// Returns the output as result
		nRows=1;
		nCols=1;
		allocMatrixOfDouble(pvApiCtx,Rhs+1, nRows, nCols, &pdblFinalVar);
		pdblFinalVar[0] = pc-> GetQuantileWilks ( alpha , beta , ovar );
	}
	LhsVar(1) = Rhs+1;
	return 0;
}
/* ==================================================================== */

