
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
// Copyright (C) 2009 - Digiteo - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license
// http://www.gnu.org/copyleft/lesser.html

//
// nispgw_support.hxx
//   Header for the C++ gateway support functions for NISP.
//
#ifndef __SCI_NISPGW_GWSUPPORT_H__
#define __SCI_NISPGW_GWSUPPORT_H__

#ifdef _MSC_VER
#pragma warning( disable : 4251 )
#endif

#include "nisp_va.h"
#include "nisp_gva.h"
#include "nisp_pc.h"

int nispgw_AssertNumberOfRows ( char * fname , int ivar , int expected_nrows , int actual_nrows );
int nispgw_AssertNumberOfColumns ( char * fname , int ivar , int expected_ncols , int actual_ncols );
int nispgw_AssertVariableType ( char * fname , int ivar , int expected_type,void * pvApiCtx );
int nispgw_GetTokenIndex ( char * fname , int ivar , int * token, void* pvApiCtx );
int nispgw_token2RandomVariable( char * fname, int ivar , int token, RandomVariable ** rv);
int nispgw_token2SetRandomVariable( char * fname, int ivar , int token, SetRandomVariable ** srv);
int nispgw_token2PolynomialChaos( char * fname, int ivar , int token, PolynomialChaos ** pc);
int nispgw_GetOneDoubleArgument ( char * fname , int ivar , double * value,void * pvApiCtx );
int nispgw_GetOneIntegerArgument ( char * fname , int ivar , int * value,void* pvApiCtx );
int nispgw_GetOneCharArgument ( char * fname , int ivar , char ** value, void* pvApiCtx );
int nispgw_Double2IntegerArgument ( char * fname , int ivar , double dvalue , int * ivalue );
void nispgw_CreateLhsInteger ( int ivar , int value, void* pvApiCtx );
void nispgw_CreateLhsDouble ( int ivar , double value,void* pvApiCtx );

#define nispgw_AssertNColumns(fname , ivar , expected_ncols , actual_ncols) \
	if ( nispgw_AssertNumberOfColumns(fname , ivar , expected_ncols , actual_ncols) == 0 ) \
{ \
	return 0;\
}

#define nispgw_AssertNRows(fname , ivar , expected_nrows , actual_nrows) \
	if ( nispgw_AssertNumberOfRows(fname , ivar , expected_nrows , actual_nrows) == 0 ) \
{ \
	return 0;\
}

#define nispgw_AssertVarType(fname , ivar , expected_type, pvApiCtx) \
	if ( nispgw_AssertVariableType(fname , ivar , expected_type, pvApiCtx) == 0 ) \
{ \
	return 0;\
}

#define nispgw_gettoken(fname , ivar , token, pvApiCtx ) \
	if ( nispgw_GetTokenIndex(fname , ivar , token,pvApiCtx ) == 0 ) \
{ \
	return 0;\
}

#define nispgw_token2Randvar(fname,ivar,token,rv) \
	if (nispgw_token2RandomVariable(fname,ivar,token,rv)==0) { \
	return 0; \
	}

#define nispgw_token2Setrandvar(fname,ivar,token,srv) \
	if (nispgw_token2SetRandomVariable(fname,ivar,token,srv)==0) { \
return 0; \
	}

#define nispgw_token2Polychaos(fname,ivar,token,pc) \
	if (nispgw_token2PolynomialChaos(fname,ivar,token,pc)==0) { \
	return 0; \
	}

#define nispgw_GetOneDouble(fname,ivar,value,pvApiCtx) \
	if (nispgw_GetOneDoubleArgument(fname,ivar,value, pvApiCtx)==0) { \
	return 0; \
	}

#define nispgw_GetOneInteger(fname,ivar,value, pvApiCtx) \
	if (nispgw_GetOneIntegerArgument(fname,ivar,value, pvApiCtx)==0) { \
	return 0; \
	}

#define nispgw_GetOneChar(fname,ivar,value,pvApiCtx) \
	if (nispgw_GetOneCharArgument(fname,ivar,value,pvApiCtx)==0) { \
	return 0; \
	}

#define nispgw_Double2Integer(fname,ivar,dvalue,ivalue) \
	if (nispgw_Double2IntegerArgument(fname,ivar,dvalue,ivalue)==0) { \
	return 0; \
	}

/* ==================================================================== */



#endif /* !__SCI_NISPGW_GWSUPPORT_H__ */
