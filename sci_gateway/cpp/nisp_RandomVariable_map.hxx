
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
// Copyright (C) 2009 - Digiteo - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license
// http://www.gnu.org/copyleft/lesser.html

//
// nisp_RandomVariable_map.h
//   Header for nisp_RandomVariable_map.cpp
//
#ifndef __NISP_RANDOMVARIABLE_MAP_H__
#define __NISP_RANDOMVARIABLE_MAP_H__

#undef __BEGIN_DECLS
#undef __END_DECLS
#ifdef __cplusplus
# define __BEGIN_DECLS extern "C" {
# define __END_DECLS }
#else
# define __BEGIN_DECLS /* empty */
# define __END_DECLS /* empty */
#endif

#include "nisp_va.h"

__BEGIN_DECLS


 int nisp_RandomVariable_map_add ( RandomVariable * rv );

/**
 * Remove a RandomVariable from the map.
 * @param[in] token : the token of the current object
 */
 void nisp_RandomVariable_map_remove ( int token );

/**
 * Returns the number of Objects in the map
 */
 int nisp_RandomVariable_map_size ();

/**
 * Fill in the tokens array the list of tokens which are currently in use.
 */
 void nisp_RandomVariable_map_tokens (int * tokens);

/**
 * Sets the object corresponding to the token
 */
 RandomVariable * nisp_RandomVariable_map_getobject ( int token );

__END_DECLS

#endif /* !__NISP_RANDOMVARIABLE_MAP_H__ */
