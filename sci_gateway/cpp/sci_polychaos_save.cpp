
// Copyright (C) 2008 - INRIA - Michael Baudin
// Copyright (C) 2009 - Digiteo - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

extern "C" {
#include "api_scilab.h"
#include "Scierror.h"
#include "localization.h"
#include "gw_nisp.h"
}


/* ==================================================================== */

#include "nisp_gwsupport.hxx" 
#include "nisp_PolynomialChaos_map.hxx" 


/* ==================================================================== */
// 
// polychaos_save ( token , file )
//   Saves the polynomial chaos in file
//
int sci_polychaos_save (char *fname, unsigned long fname_len)
{
	int token;
	PolynomialChaos * pc = NULL;
	char * file = NULL;

	CheckInputArgument(pvApiCtx,2,2) ;
	CheckOutputArgument(pvApiCtx,0,1) ;
	nispgw_gettoken ( fname , 1 , &token, pvApiCtx);
	nispgw_GetOneChar ( fname , 2 , &file, pvApiCtx);
	nispgw_token2Polychaos ( fname , 1 , token , &pc );
	pc -> Save ( file );
	nispgw_CreateLhsInteger ( 1 , token, pvApiCtx);
	return 0;
}
/* ==================================================================== */

