
// Copyright (C) 2008 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

//
// nisp_RandomVariable_map.cpp --
//   A map to manage RandomVariable from NISP for the tbxnisp Scilab Toolbox
//
#include <stdlib.h>
#include <map>

#include "nisp_va.h"

#include "nisp_RandomVariable_map.hxx"

using namespace std;
typedef map<int , RandomVariable *> nisp_RandomVariable_map_type;
nisp_RandomVariable_map_type nisp_RandomVariable_map;
int RandomVariable_counter = 0;

int nisp_RandomVariable_map_add ( RandomVariable * rv )
{
	int token;
	token = RandomVariable_counter;
	RandomVariable_counter = RandomVariable_counter + 1;
	nisp_RandomVariable_map[token] = rv;
	return token;
}

void nisp_RandomVariable_map_remove ( int token )
{
	nisp_RandomVariable_map_type::iterator it;
	it = nisp_RandomVariable_map.find (token);
	nisp_RandomVariable_map.erase(it);
}

RandomVariable * nisp_RandomVariable_map_getobject ( int token )
{
	RandomVariable * rv = NULL;
	if ( nisp_RandomVariable_map.size()!=0 ) {
		nisp_RandomVariable_map_type::iterator it;
		it = nisp_RandomVariable_map.find (token);
		if ( it!= nisp_RandomVariable_map.end()) {
			rv = it->second;
		}
	}
	return rv;
}

int nisp_RandomVariable_map_size ()
{
	return nisp_RandomVariable_map.size();
}

void nisp_RandomVariable_map_tokens (int * tokens)
{
	int index = 0;
	int token;
	for(nisp_RandomVariable_map_type::iterator it = nisp_RandomVariable_map.begin(); it != nisp_RandomVariable_map.end(); ++it)
	{
		token = it->first;
		tokens[index] = token;
		index ++;
	}
}