
// Copyright (C) 2008 - INRIA - Michael Baudin
// Copyright (C) 2009 - Digiteo - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

extern "C" {
#include "api_scilab.h"
#include "Scierror.h"
#include "localization.h"
#include "gw_nisp.h"
}

/* ==================================================================== */

#include "nisp_gwsupport.hxx" 
#include "nisp_SetRandomVariable_map.hxx" 


/* ==================================================================== */
// 
// nx = setrandvar_getdimension ( token )
//   returns the number of input random variables.
//
int sci_setrandvar_getdimension (char *fname, unsigned long fname_len)
{
	int token;
	SetRandomVariable * srv = NULL;
	int dim;

	CheckInputArgument(pvApiCtx,1,1) ;
	CheckOutputArgument(pvApiCtx,0,1) ;
	nispgw_gettoken ( fname , 1 , & token, pvApiCtx);
	nispgw_token2Setrandvar ( fname , 1 , token , &srv );
	dim = srv->GetDimension ( );
	nispgw_CreateLhsInteger ( 1 , dim, pvApiCtx);
	return 0;
}
/* ==================================================================== */

