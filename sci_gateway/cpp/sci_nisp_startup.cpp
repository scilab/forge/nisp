
// Copyright (C) 2008 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

extern "C" {
#include "api_scilab.h"
#include "Scierror.h"
#include "localization.h"
#include "sciprint.h"
#include "gw_nisp.h"
}

/* ==================================================================== */

#include "sci_nisp_startup.hxx" 
#include "nisp_conf.h"

int sci_nisp_startup_flag = 0;
int sci_nisp_seed = 0;

// sci_nisp_errorfunction --
//   The error callback used by NISP.
//   Redirect the message to Scilab's error function.
void sci_nisp_errorfunction ( char * message ) {
	Scierror(999,_("NISP: Error at the library level:\n%s\n") , message );
}

// sci_nisp_messagefunction --
//   The error callback used by NISP.
//   Redirect the message to Scilab's error function.
void sci_nisp_messagefunction ( char * message ) {
	sciprint( message );
}

// nisp_startup ( )
//   startup the NISP library
int sci_nisp_startup (char *fname,unsigned long fname_len) {

	CheckInputArgument(pvApiCtx, 0,0) ;
	CheckOutputArgument(pvApiCtx, 0,1) ;

	// Connect the Nisp message function to Scilab's print
	if (sci_nisp_startup_flag==0) {
		nisp_msgsetfunction ( sci_nisp_messagefunction );
		nisp_errorsetfunction ( sci_nisp_errorfunction );
		nisp_initseed ( sci_nisp_seed );
		sci_nisp_startup_flag = 1;
	}
	return 0;
}
