
// Copyright (C) 2008 - INRIA - Michael Baudin
// Copyright (C) 2009 - Digiteo - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

extern "C" {
#include "api_scilab.h"
#include "Scierror.h"
#include "localization.h"
#include "gw_nisp.h"
}


/* ==================================================================== */

#include "nisp_gwsupport.hxx" 
#include "nisp_PolynomialChaos_map.hxx" 
#include "nisp_SetRandomVariable_map.hxx" 


/* ==================================================================== */
// 
// polychaos_generatecode ( token , filename , funname )
//   generates the source code to compute the polynomial chaos into the file filename.
//   The generated function name is funname.
//
int sci_polychaos_generatecode (char *fname, unsigned long fname_len)
{
	int token;
	PolynomialChaos * pc = NULL;
	char * filename = NULL;
	char * funname = NULL;

	CheckInputArgument(pvApiCtx,3,3) ;
	CheckOutputArgument(pvApiCtx,0,1) ;
	nispgw_gettoken ( fname , 1 , &token, pvApiCtx);
	nispgw_GetOneChar ( fname , 2 , &filename, pvApiCtx);
	nispgw_GetOneChar ( fname , 3 , &funname, pvApiCtx);
	nispgw_token2Polychaos ( fname , 1 , token , &pc );
	pc-> GenerateCode ( filename , funname );
	nispgw_CreateLhsInteger ( 1 , token, pvApiCtx);
	return 0;
}
/* ==================================================================== */

