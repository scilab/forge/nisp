
// Copyright (C) 2008 - INRIA - Michael Baudin
// Copyright (C) 2009 - Digiteo - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

extern "C" {
#include "api_scilab.h"
#include "Scierror.h"
#include "localization.h"
#include "gw_nisp.h"
}

/* ==================================================================== */

#include "nisp_gwsupport.hxx" 
#include "nisp_RandomVariable_map.hxx" 
#include "nisp_SetRandomVariable_map.hxx" 


/* ==================================================================== */
// 
// setrandvar_addrandvar ( token , rv )
//   add the random variable to the set
//
int sci_setrandvar_addrandvar (char *fname,unsigned long fname_len)
{
	int nRows, nCols;
	int token;
	int rvtoken;
	SetRandomVariable * srv = NULL;
	RandomVariable * rv = NULL;
	double * mydata = NULL;
	double *pdblFinalVar = NULL; //SCILAB return Var

	CheckInputArgument(pvApiCtx,2,2) ;
	CheckOutputArgument(pvApiCtx,0,1) ;
	nispgw_gettoken ( fname , 1 , & token, pvApiCtx);
	nispgw_gettoken ( fname , 2 , & rvtoken, pvApiCtx);
	// Get the set random variable and the random variable
	nispgw_token2Setrandvar ( fname , 1 , token , &srv );
	nispgw_token2Randvar ( fname , 2 , rvtoken , &rv );
	srv->AddRandomVariable ( rv );
	// Returns the token as the result
	nRows=1;
	nCols=1;
	allocMatrixOfDouble(pvApiCtx,Rhs+1, nRows, nCols, &pdblFinalVar);
	pdblFinalVar[0] = token;
	LhsVar(1) = Rhs+1;
	return 0;
}
/* ==================================================================== */

