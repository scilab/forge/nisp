// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// If inrange is true, set the sensitivy indices into [0,1]
function s = nisp_sensitivityTorange(s,inrange)
    if ( inrange ) then
      s = max(s,0.)
      s = min(s,1.)
	end
endfunction
