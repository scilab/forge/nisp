// Copyright (C) 2013 - 2014 - Michael Baudin
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html


function varargout = nisp_bruteforcesa ( varargin )
    // Compute sensitivity indices by brute force.
    //
    // Calling Sequence
    //   s=nisp_bruteforcesa(func,nx)
    //   s=nisp_bruteforcesa(func,nx,randgen)
    //   s=nisp_bruteforcesa(func,nx,randgen,n)
    //   s=nisp_bruteforcesa(func,nx,randgen,n, inrange)
    //   [s,nbevalf]=nisp_bruteforcesa(...)
    //
    // Parameters
    //   func : a function or a list, the name of the function to be evaluated. 
    //   nx : a 1-by-1 matrix of floating point integers, the number of inputs of the function.
    //   randgen : a function or a list, the random number generator. (default = uniform random variables)
    //   n : a 1-by-1 matrix of floating point integers (default n=10000), the number of Monte-Carlo experiments, for each sensitivity index
    //   inrange : a 1-by-1 matrix of booleans (default inrange = %t), set to true to restrict the sensitivity indices into [0,1].
    //   s : a nx-by-1 matrix of doubles, the first order sensitivity indices
    //   nbevalf : a nx-by-1 matrix of doubles, the actual number of function evaluations.
    //
    // Description
    // The algorithm uses the brute force method to compute the first order 
    // sensitivity indices. 
    // For i=1,...,nx, this corresponds to computing the conditionnal 
    // expectation V(E(y|xi)), by sampling xi with simple Monte-Carlo.
    // This requires the evaluation of func on n**2 inputs. 
    // This method is always a slower than nisp_sobolsaFirst.
    //
    // This function assumes that the input variables are independent. 
    //
    // By design, the algorithm produces sensitivity indices which are 
    // positive.
    // If a computed indice is greater than 1, it is set to 1.
    // It is not guaranteed that the sum of indices in s is 1: 
    // in fact this is rarely the case.
    //
    // Any optional input argument equal to the empty matrix will be set to its 
    // default value.
    //
    // See "Specifications of func" for the specifications 
    // of the "func" and "randgen" arguments.
    // 
    // Examples
    // // Compute the first order sensitivity indices 
    // // of the ishigami function.
    // // Three random variables uniform in [-pi,pi].
    // function y=ishigami(x)
    //     a=7.
    //     b=0.1
    //     s1=sin(x(:,1))
    //     s2=sin(x(:,2))
    //     x34 = x(:,3).^4
    //     y(:,1) = s1 + a.*s2.^2 + b.*x34.*s1
    // endfunction
    // function x = myrandgen ( n, i)
    //     x = distfun_unifrnd(-%pi,%pi,n,1)
    // endfunction
    // a=7.;
    // b=0.1;
    // exact = nisp_ishigamisa(a,b);
    // n = 500;
    // nx = 3;
    // [s,nbevalf]=nisp_bruteforcesa(ishigami,nx,myrandgen,n);
    // [s(1),exact.S1]
    // [s(2),exact.S2]
    // [s(3),exact.S3]
    //
    // // See the number of significant digits
    // // when N increases
    //stacksize("max");
    //imax=11;
    //for i=2:11
    //    n=2^i;
    //    s=nisp_bruteforcesa(ishigami,nx,myrandgen,n);
    //    d(i-1,1)=assert_computedigits(s(1),exact.S1);
    //    d(i-1,2)=assert_computedigits(s(2),exact.S2);
    //    d(i-1,3)=assert_computedigits(s(3),exact.S3);
    //end
    //scf();
    //plot(1:imax-1,d(:,1)',"r-");
    //plot(1:imax-1,d(:,2)',"b-");
    //plot(1:imax-1,d(:,3)',"g-");
    //legend(["S1","S2","S3"]);
    //xtitle("","Log2(N)","Exact Digits");
    //
    // // Repeat the computation N times
    // // See the histogram
    // stacksize("max");
    // n=2^7;
    // nsamples=100;
    // smat=zeros(3,nsamples);
    // for i=1:nsamples
    //     s(:,i)=nisp_bruteforcesa(ishigami,nx,myrandgen,n);
    // end
    // scf();
    // //
    // subplot(1,3,1);
    // histo(s(1,:));
    // plot([exact.S1,exact.S1],[0,10],"r-");
    // xtitle("","S1","Frequency");
    // legend(["Data","Exact"]);
    // //
    // subplot(1,3,2);
    // histo(s(2,:));
    // plot([exact.S2,exact.S2],[0,10],"r-");
    // xtitle("","S2","Frequency");
    // legend(["Data","Exact"]);
    // //
    // subplot(1,3,3);
    // histo(s(3,:));
    // plot([exact.S3,exact.S3],[0,200],"r-");
    // xtitle("","S3","Frequency");
    // legend(["Data","Exact"]);
    //
    // Authors
    // Copyright (C) 2013 - 2014 - Michael Baudin
    // Copyright (C) 2011 - DIGITEO - Michael Baudin

    // Load Internals lib
    path = nisp_getpath (  )
    nispinternallib  = lib(fullfile(path,"macros","internals"))

    [lhs, rhs] = argn();
    apifun_checkrhs ( "nisp_bruteforcesa" , rhs , 2 : 5 )
    apifun_checklhs ( "nisp_bruteforcesa" , lhs , [0 1 2] )
    //
    __brute_func__ = varargin(1)
    nx = varargin(2)
    __brute_randgen__ = apifun_argindefault ( varargin , 3 , nisp_soboldefaultrgdef )
    n = apifun_argindefault ( varargin , 4 , 10000 )
    inrange = apifun_argindefault ( varargin , 5 , %t )
    //
    // Check input arguments
    //
    // Check type
    apifun_checktype ( "nisp_bruteforcesa" , __brute_func__ ,   "func" ,  1 , [ "function" "list" ] )
    apifun_checktype ( "nisp_bruteforcesa" , nx ,      "nx" ,     2 , "constant" )
    apifun_checktype ( "nisp_bruteforcesa" , __brute_randgen__ ,  "randgen" , 3 , [ "function" "list" ] )
    apifun_checktype ( "nisp_bruteforcesa" , n ,  "n" , 4 , "constant" )
    apifun_checktype ( "nisp_bruteforcesa" , inrange ,  "inrange" , 5 , "boolean" )
    //
    // Check size
    apifun_checkscalar ( "nisp_bruteforcesa" , nx ,       "nx" ,       2 )
    apifun_checkscalar ( "nisp_bruteforcesa" , n ,   "n" ,   4 )
    apifun_checkscalar ( "nisp_sobolsaFirst", inrange, "inrange", 5 )
    //
    // Check content
    apifun_checkgreq ( "nisp_bruteforcesa" , nx ,  "nx" ,  2 , 1 )
    apifun_checkgreq ( "nisp_bruteforcesa" , n ,   "n" ,   4 , 1 )
    //
    // Proceed...
    // 1. Compute the variance of y.
    // Define a sampling for x
    x = nisp_getsample ( n , nx , __brute_randgen__ )
    // Perform the experiments
    y = nisp_evalfunc ( x , __brute_func__ );
    ny=size(y,"c")
    Vy= variance(y,"r");
    //
    // 2. Compute E(y|xi) for several values of xi.
    for i = 1 : nx
        // Get the sampling for i
        x=zeros(n^2,nx)
        for j=1:nx
            if (j==i) then
                xj = nisp_getsample ( n , nx , __brute_randgen__ , j )
                xj=xj.*.ones(n,1)
            else
                xj = nisp_getsample ( n^2 , nx , __brute_randgen__ , j )
            end
            x(1:n^2,j)=xj
        end
        // Perform the experiments
        y = nisp_evalfunc ( x , __brute_func__ );
        // Store E(y|xi) in Ey_all(k), for k=1,2,...,n.
        // Each block of n Y-values corresponds to one sample of xi.
        // Reshape y, so that each column correspond to one sample of xi.
        for ll=1:ny
            yll= matrix(y(:,ll),n,n);
            Ey_all = mean(yll,"r")';
            //
            // Compute Si
            s(i,ll) = variance(Ey_all)/Vy(ll);
        end
    end
    nbevalf = n + nx*n^2
    //
    // Put indices into bounds.
    s = nisp_sensitivityTorange(s,inrange)
    //
    // Set outputs
    varargout(1) = s
    varargout(2) = nbevalf
endfunction

