// Copyright (C) 2013 - 2014 - Michael Baudin
// Copyright (C) 2011 - DIGITEO - Michael Baudin
// Copyright (C) 2011 - CEA - Jean-Marc Martinez
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html


function [ smin , smax ] = nisp_sobolsaInterval ( varargin )
    // Compute confidence interval for sensitivity indices
    //
    // Calling Sequence
    // [smin,smax]=nisp_sobolsaInterval(s,n)
    // [smin,smax]=nisp_sobolsaInterval(s,n,inrange)
    // [smin,smax]=nisp_sobolsaInterval(s,n,inrange,c)
    //
    // Parameters
    // s: a nx-by-ny matrix of doubles, the sensitivity indices. Should generally be in [0,1], but this is not mandatory.
    // n: a 1-by-1 matrix of doubles, the number of experiments
    // inrange : a 1-by-1 matrix of booleans (default inrange = %t), set to true to restrict the sensitivity indices into [0,1].
    // c : a 1-by-1 matrix of doubles (default c = 1-0.95), the level for the confidence interval. Must be in the range [0,0.5].
    // smin : a nx-by-ny matrix of doubles, the lower bound of the confidence interval.
    // smax : a nx-by-ny matrix of doubles, the upper bound of the confidence interval.
    //
    // Description
    // Compute confidence interval for sensitivity indices by Sobol, Ishigami, Homma.
    // 
    // This is done by the nisp_cicorrcoef functin, 
    // as published by Martinez (20011).
    //
    // Examples
    // s=[0.2;0.1;0.7];
    // n=1000;
    // [smin,smax]=nisp_sobolsaInterval(s,n)
    //
    // s=[0.2;0.1;0.7];
    // n=1000;
    // [smin,smax]=nisp_sobolsaInterval(s,n)
    // //
    // // nx=3, ny=1 - inrange=%t
    // s=[0.2;0.;0.7];
    // n=1000;
    // inrange=%t;
    // [smin,smax]=nisp_sobolsaInterval(s,n,inrange)
    // //
    // // nx=3, ny=1 - inrange=%f
    // s=[0.2;0.;0.7];
    // n=1000;
    // inrange=%f;
    // [smin,smax]=nisp_sobolsaInterval(s,n,inrange)
    // //
    // // nx=3, ny=1, 99%
    // s=[0.2;0.;0.7];
    // n=1000;
    // c=1.-0.99;
    // [smin,smax]=nisp_sobolsaInterval(s,n,[],c)
    //
    // s=[
    // 0.2 0.9 0.1
    // 0.  0.2 0.3
    // 0.7 0.6 0.5
    // ];
    // n=1000;
    // [smin,smax]=nisp_sobolsaInterval(s,n)
    //
    // Bibliography
    // GdR Ondes & Mascot Num, "Analyse de sensibilite globale par decomposition de la variance", jean-marc.martinez@cea.fr, 13 janvier 2011, Institut Henri Poincare
    //
    // Authors
    // Copyright (C) 2013 - 2014 - Michael Baudin
    // Copyright (C) 2011 - DIGITEO - Michael Baudin
    // Copyright (C) 2011 - CEA - Jean-Marc Martinez

    // Load Internals lib
    path = nisp_getpath (  )
    nispinternallib  = lib(fullfile(path,"macros","internals"))

    [lhs, rhs] = argn();
    apifun_checkrhs ( "nisp_sobolsaInterval" , rhs , 2:4 )
    apifun_checklhs ( "nisp_sobolsaInterval" , lhs , 2:2 )
    //
    s = varargin(1)
    n = varargin(2)
    inrange = apifun_argindefault ( varargin , 3 , %t )
    c = apifun_argindefault ( varargin , 4 , 1-0.95 )
    //
    // Check input arguments
    //
    // Check type
    apifun_checktype ( "nisp_sobolsaInterval" , s , "s" ,  1 , "constant" )
    apifun_checktype ( "nisp_sobolsaInterval" , n , "n" , 2 , "constant" )
    apifun_checktype ( "nisp_sobolsaInterval" , inrange , "inrange" , 3 , "boolean" )
    apifun_checktype ( "nisp_sobolsaInterval" , c , "c" , 4 , "constant" )
    //
    // Check size
    // 
    apifun_checkscalar ( "nisp_sobolsaInterval", n, "n", 2 )
    apifun_checkscalar ( "nisp_sobolsaInterval", inrange, "inrange", 3 )
    apifun_checkscalar ( "nisp_sobolsaInterval", c, "c", 4 )
    //
    // Check content
    // Do not check range of s : might be negative in some cases,
    // depending on the accuracy of the estimate
    apifun_checkgreq ( "nisp_sobolsaInterval" , n ,   "n" , 2 , 1 )
    apifun_checkrange ( "nisp_sobolsaInterval", c ,  "c" , 4, 0., 0.5 )

    // Proceed..
    [smin,smax] = nisp_cicorrcoef(s,n,c)
    smin = nisp_sensitivityTorange(smin,inrange)
    smax = nisp_sensitivityTorange(smax,inrange)
endfunction

