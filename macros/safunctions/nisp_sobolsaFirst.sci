// Copyright (C) 2014 - 2016 - Michael Baudin
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html


function [ s, nbevalf, smin, smax ] = nisp_sobolsaFirst ( varargin )
    // Compute sensitivity indices by Sobol, Ishigami, Homma.
    //
    // Calling Sequence
    //   s = nisp_sobolsaFirst( func, nx )
    //   s = nisp_sobolsaFirst( func, nx, randgen)
    //   s = nisp_sobolsaFirst( func, nx, randgen, n)
    //   s = nisp_sobolsaFirst( func, nx, randgen, n, inrange)
    //   s = nisp_sobolsaFirst( func, nx, randgen, n, inrange, c)
    //   s = nisp_sobolsaFirst( func, nx, randgen, n, inrange, c, method)
    //   [s, nbevalf ] = nisp_sobolsaFirst( ... )
    //   [s, nbevalf, smin] = nisp_sobolsaFirst( ... )
    //   [s, nbevalf, smin, smax] = nisp_sobolsaFirst( ... )
    //
    // Parameters
    //   func : a function or a list, the name of the function to be evaluated. 
    //   nx : a 1-by-1 matrix of floating point integers, the number of inputs of the function.
    //   randgen : a function or a list, the random number generator. (default = uniform random variables)
    //   n : a 1-by-1 matrix of floating point integers (default n=10000), the number of Monte-Carlo experiments, for each sensitivity index
    //   inrange : a 1-by-1 matrix of booleans (default inrange = %t), set to true to restrict the sensitivity indices into [0,1].
    //   c : a 1-by-1 matrix of doubles (default c = 1-0.95), the level for the confidence interval. Must be in the range [0,0.5].
    // method : a string, the method (default="Martinez"). The available methods are "Martinez" and "Saltelli". If the "Saltelli" method is used, the confidence interval is empty.
    //   s : a nx-by-ny matrix of doubles, the first order sensitivity indices, where nx is the number of input variables and ny is the number of output variables.
    //   nbevalf : a 1-by-1 matrix of doubles, the actual number of function evaluations.
    //   smin : a nx-by-ny matrix of doubles, the lower bound of the confidence interval.
    //   smax : a nx-by-ny matrix of doubles, the upper bound of the confidence interval.
    //
    // Description
    // The algorithm uses the Sobol method to compute the first order 
    // sensitivity indices.
    //
    // This method assumes that all the input random variables are 
    // independent.
    //
    // On output, the variable s(i,j) corresponds to the sensitivity 
    // of the j-output for the i-th input, for i=1,...,nx, 
    // j=1,...,ny. 
    // The bounds smin and smax have the same specifications. 
    //
    // On output, if <literal>inrange</literal> is true, then 
    // the sensitivity indices are forced to be in the range [0,1]. 
    // Otherwise, if the exact value is zero, then it may happen that 
    // the estimate is negative.
    // Similarily, if the exact value is one, then it may happen that 
    // the estimate greater than one.
    // The <literal>inrange</literal> option manages this situation.
    //
    // The confidence level 1-c is so that 
    //
    // <latex>
    // P(smin \leq s \leq smax)=1-c.
    // </latex>
    // 
    // This is done by the Fisher transformation of the correlation 
    // coefficient, as published by Martinez, 2005 (see below).
    //
    // Any optional input argument equal to the empty matrix will 
    // be set to its default value.
    //
    // See "Specifications of func" for the specifications 
    // of the "func" and "randgen" arguments.
    //
    // The Saltelli estimator can be sensitive to the order of 
    // magnitude of the output Y of the computer code. 
    // More precisely, the exact sensitivity of f(x) and f(x)+c 
    // are the same. 
    // However, Saltelli's estimator can be completely wrong 
    // if c is large. 
    // Martinez's estimator is not sensitive to the constant c.
    //
    // Examples
    // // Compute the first order sensitivity indices of the ishigami function.
    // // Three random variables uniform in [-pi,pi].
    // function y = ishigami (x)
    //   a=7.
    //   b=0.1
    //   s1=sin(x(:,1))
    //   s2=sin(x(:,2))
    //   x34 = x(:,3).^4
    //   y(:,1) = s1 + a.*s2.^2 + b.*x34.*s1
    // endfunction
    // function x = myrandgen ( m , i )
    //   x = distfun_unifrnd(-%pi,%pi,m,1)
    // endfunction
    // a=7.;
    // b=0.1;
    // exact = nisp_ishigamisa ( a , b )
    // n = 1000;
    // nx = 3;
    // [s,nbevalf]=nisp_sobolsaFirst(ishigami,nx,myrandgen,n)
    //
    // // See the confidence interval.
    // [s,nbevalf,smin,smax]=nisp_sobolsaFirst(ishigami,nx,myrandgen,n)
    //
    // // Configure inrange
    // distfun_seedset(0);
    // // s(3) is zero
    // s=nisp_sobolsaFirst(ishigami,nx,myrandgen,n,%t)
    // // s(3) is negative
    // s=nisp_sobolsaFirst(ishigami,nx,myrandgen,n,%f)
    //
    // // Configure the confidence interval length at 95%
    // [s,nbevalf,smin,smax]=nisp_sobolsaFirst(ishigami,nx,myrandgen,n,[],1-0.95)
    // // Configure the confidence interval length at 99% 
    // [s,nbevalf,smin,smax]=nisp_sobolsaFirst(ishigami,nx,myrandgen,n,[],1-0.99)
    //
    // // See the variability of the sensitivity indices.
    // for k = 1 : 100
    //   [s,nbevalf]=nisp_sobolsaFirst(ishigami,nx,myrandgen,1000);
    //   sall(k,:) = s';
    // end
    // scf();
    // subplot(2,2,1);
    // histo(sall(:,1));
    // xtitle("Variability of the sensitivity index for X1","S1","Frequency");
    // subplot(2,2,2);
    // histo(sall(:,2));
    // xtitle("Variability of the sensitivity index for X2","S2","Frequency");
    // subplot(2,2,3);
    // histo(sall(:,3));
    // xtitle("Variability of the sensitivity index for X3","S3","Frequency");
    //
    // // See the convergence of the sensitivity indices
    // n=10;
    // stacksize("max");
    // for k = 1 : 100
    //   tic();  
    //   [s,nbevalf]=nisp_sobolsaFirst(ishigami,nx,myrandgen,n);
    //   sc(k,:) = s';
    //   t = toc();
    //   mprintf("Run #%d, n=%d, t=%.2f (s)\n",k,n,t);
    //   if ( t > 1 ) then
    //     break
    //   end
    //   n = ceil(1.2*n);
    // end
    // h = scf();
    // subplot(1,2,1);
    // plot(1:k,sc(1:k,1),"bx-");
    // plot(1:k,sc(1:k,2),"ro-");
    // plot(1:k,sc(1:k,3),"g*-");
    // mytitle="Convergence of the sensitivity indices";
    // xtitle(mytitle,"Number of simulations","S");
    // legend(["S1","S2","S3"]);
    // subplot(1,2,2);
    // plot(1:k,abs(sc(1:k,1)-exact.S1),"bx-");
    // plot(1:k,abs(sc(1:k,2)-exact.S2),"ro-");
    // plot(1:k,abs(sc(1:k,3)-exact.S3),"g*-");
    // mytitle="Convergence of the sensitivity indices";
    // xtitle(mytitle,"Number of simulations","|S-exact|");
    // legend(["S1","S2","S3"]);
    // h.children(1).log_flags="lnn";
    //
    // // Example with ny=2 outputs
    // function y=ishigaminy2(x,a1,b1,a2,b2)
    //     y1=nisp_ishigami(x,a1,b1)
    //     y2=nisp_ishigami(x,a2,b2)
    //     y=[y1,y2]
    // endfunction
    // n = 1000;
    // nx = 3;
    // a1=7.;
    // b1=0.1;
    // a2=1.;
    // b2=1.;
    // s=nisp_sobolsaFirst(list(ishigaminy2,a1,b1,a2,b2), ..
    //     nx, myrandgen, n)
    //
    // Authors
    // Copyright (C) 2014 - Michael Baudin
    // Copyright (C) 2011 - DIGITEO - Michael Baudin
    //
    // Bibliography
    // GdR Ondes & Mascot Num, "Analyse de sensibilite globale par decomposition de la variance", jean-marc.martinez@cea.fr, 13 janvier 2011, Institut Henri Poincare
    // "Contribution à l'analyse de sensibilité et à l'analyse discriminante généralisée", 2005, Julien Jacques, Thèse
    // "Making best use of model evaluations to compute sensitivity indices", Saltelli, 2002

    // Load Internals lib
    path = nisp_getpath (  )
    nispinternallib  = lib(fullfile(path,"macros","internals"))

    [lhs, rhs] = argn();
    apifun_checkrhs ( "nisp_sobolsaFirst" , rhs , 2:7 )
    apifun_checklhs ( "nisp_sobolsaFirst" , lhs , 1:4 )
    //
    //
    __sobolfo_func__ = varargin(1)
    nx = varargin(2)
    __sobolfo_randgen__ = apifun_argindefault ( varargin , 3 , nisp_soboldefaultrgdef )
    n = apifun_argindefault ( varargin , 4 , 10000 )
    inrange = apifun_argindefault ( varargin , 5 , %t )
    c = apifun_argindefault ( varargin , 6 , 1-0.95 )
    method = apifun_argindefault ( varargin , 7 , "Martinez" )
    //
    // Check input arguments
    //
    // Check type
    apifun_checktype ( "nisp_sobolsaFirst" , __sobolfo_func__ ,   "func" ,  1 , [ "function" "list" ] )
    apifun_checktype ( "nisp_sobolsaFirst" , nx ,      "nx" ,     2 , "constant" )
    apifun_checktype ( "nisp_sobolsaFirst" , __sobolfo_randgen__ ,  "randgen" , 3 , [ "function" "list" ] )
    apifun_checktype ( "nisp_sobolsaFirst" , n ,  "n" , 4 , "constant" )
    apifun_checktype ( "nisp_sobolsaFirst" , inrange ,  "inrange" , 5 , "boolean" )
    apifun_checktype ( "nisp_sobolsaFirst", c ,   "c" ,  6 , "constant" )
    apifun_checktype ( "nisp_sobolsaFirst", method ,   "method" ,  7 , "string" )
    //
    // Check size
    apifun_checkscalar ( "nisp_sobolsaFirst", nx, "nx", 2 )
    apifun_checkscalar ( "nisp_sobolsaFirst", n, "n", 4 )
    apifun_checkscalar ( "nisp_sobolsaFirst", inrange, "inrange", 5 )
    apifun_checkscalar ( "nisp_sobolsaFirst", c, "c", 6 )
    apifun_checkscalar ( "nisp_sobolsaFirst", method, "method", 7 )
    //
    // Check content
    apifun_checkgreq ( "nisp_sobolsaFirst" , nx ,  "nx" ,  2 , 1 )
    apifun_checkgreq ( "nisp_sobolsaFirst" , n ,   "n" ,   4 , 1 )
    apifun_checkrange ( "nisp_sobolsaFirst", c ,  "c" ,  6, 0., 0.5 )
    apifun_checkoption ( "nisp_sobolsaFirst", method ,  "method" ,  7, ["Martinez","Saltelli"] )
    //
    // Proceed...
    // Create a sample A
    A = nisp_getsample ( n , nx , __sobolfo_randgen__ )
    // Create a sample B
    B = nisp_getsample ( n , nx , __sobolfo_randgen__ )
    // Perform the experiments in A
    ya = nisp_evalfunc ( A , __sobolfo_func__ )
    ny=size(ya,"c")
    //
    // Compute the sensitivity indices
    for i = 1 : nx
        C(:,1:i-1) = B(:,1:i-1);
        C(:,i)=A(:,i);
        C(:,i+1:nx) = B(:,i+1:nx);
        yc = nisp_evalfunc ( C , __sobolfo_func__ )
        for j=1:ny
            if (method=="Martinez") then
                s(i,j) = corrcoef ( ya(:,j) , yc(:,j) );
            elseif (method=="Saltelli")
                s(i,j) = saltelli_estimator ( ya(:,j) , yc(:,j) );
            end
        end
    end
    //
    // Compute the confidence interval
    s = nisp_sensitivityTorange(s,inrange)
    if (method=="Martinez") then
        [ smin , smax ] = nisp_sobolsaInterval ( s, n, inrange, c )
    else
        smin=[]
        smax=[]
    end
    //
    // Compute function evaluations
    nbevalf = n + nx * n
endfunction

function s = saltelli_estimator ( ya, yc )
    n=size(ya,"r")
    ey=mean(ya) // estimate of E(Y)
    vy=sum((ya-ey).^2)/(n-1)
    // This is equation (10), p.282 in Saltelli (2002)
    uj=sum(ya.*yc)/(n-1)
    s=(uj-ey^2)/vy
endfunction
    
