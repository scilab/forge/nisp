// Copyright (C) 2013 - Michael Baudin
// Copyright (C) 2010 - 2011 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html



function y = nisp_sum ( x , a )
    // Returns the value of the sum function
    //
    // Calling Sequence
    //   y = nisp_sum ( x , a )
    //
    // Parameters
    // a: a (nx+1)-by-1 or 1-by-(nx+1) matrix of doubles, where nx is the number of parameters
    // x: a np-by-nx matrix of doubles, where np is the number of experiments.
    // y: a np-by-1 matrix of doubles
    //
    // Description
    //   This function returns the value of the Sum function.
    //
    // This function is defined by 
    //
    // <latex>
    // \begin{eqnarray}
    // Y = a_1 X_1 + ... + a_{nx} X_{nx} + a_{nx+1}
    // \end{eqnarray}
    // </latex>
    //
    // where X(i) are random variables with mean 
    // mu(i) and standard variation sigma(i), 
    // for i=1,2,...,nx.
    //
    // Examples
	// // With p=4 parameters and np=2 experiments.
    // x = [
	//   1 2 3 4
	//   5 6 7 8
	// ];
    // a = [5 6 7 8 9]';
    // y = nisp_sum (x,a)
    // expected = [
	// 79
	// 183
	// ];
    //
    // Authors
    // Copyright (C) 2013 - Michael Baudin
    // Copyright (C) 2008-2011 - INRIA - Michael Baudin
    //
    // Bibliography
    // "Sensitivity analysis in practice", Saltelli, Tarantolla, Compolongo, Ratto, Wiley, 2004
    // "An importance quantification technique in uncertainty analysis for computer models", Ishigami, Homma, 1990, Proceedings of the ISUMA'90. First international symposium on uncertainty modelling and Analysis, University of Maryland, USA, pp. 398-403.

    [lhs, rhs] = argn();
    apifun_checkrhs ( "nisp_sum" , rhs , 2 : 2 )
    apifun_checklhs ( "nisp_sum" , lhs , 0:1 )
    //
    //
    // Check input arguments
    //
    // Check type
    apifun_checktype ( "nisp_sum" , x ,   "x" ,  1 , "constant" )
    apifun_checktype ( "nisp_sum" , a ,   "a" ,  2 , "constant" )
    //
    // Check size
    np = size(x,"r")
    p=size(x,"c")
    apifun_checkdims ( "nisp_sum" , x ,    "x" ,    1 , [np p] )
    apifun_checkvector ( "nisp_sum" , a ,    "a" ,    2 , (p+1) )
    //
    // Check content
    // Nothing to check
    //
    y(:) = x(:,1:p)*a(1:p) + a(p+1)
endfunction

