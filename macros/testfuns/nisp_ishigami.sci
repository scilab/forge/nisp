// Copyright (C) 2009 - CEA - Jean-Marc Martinez
// Copyright (C) 2010 - 2011 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html



function y = nisp_ishigami ( x , a , b )
  // Returns the Ishigami function.
  //
  // Calling Sequence
  //   y = nisp_ishigami ( x , a , b )
  //
  // Parameters
  // a: a 1-by-1 matrix of doubles, the first parameter.
  // b: a 1-by-1 matrix of doubles, the second parameter.
  // x: a np-by-nx matrix of doubles, where np is the number of experiments, and nx=2.
  // y: a np-by-1 matrix of doubles
  //
  // Description
  //   This function returns the Ishigami function.
  //
  // This function is defined by 
  //
  // <latex>
  // \begin{eqnarray}
  // Y = \sin(X_1) + a \sin^2(X_2) + b X_3^4 \sin(X_1)
  // \end{eqnarray}
  // </latex>
  //
  // where X1, X2 and X3 are three uniform random in [-pi,pi].
  //
  // Examples
  // a=7.;
  // b=0.1;
  // np = 10;
  // x = distfun_unifrnd(-%pi,%pi,np,3);
  // y = nisp_ishigami ( x , a , b )
  //
  // Authors
  // Copyright (C) 2008-2011 - INRIA - Michael Baudin
  // Copyright (C) 2009 - CEA - Jean-Marc Martinez
  //
  // Bibliography
  // "Sensitivity analysis in practice", Saltelli, Tarantolla, Compolongo, Ratto, Wiley, 2004
  // "An importance quantification technique in uncertainty analysis for computer models", Ishigami, Homma, 1990, Proceedings of the ISUMA'90. First international symposium on uncertainty modelling and Analysis, University of Maryland, USA, pp. 398-403.

    [lhs, rhs] = argn();
    apifun_checkrhs ( "nisp_ishigami" , rhs , 3 : 3 )
    apifun_checklhs ( "nisp_ishigami" , lhs , 0:1 )
    //
    //
    // Check input arguments
    //
    // Check type
    apifun_checktype ( "nisp_ishigami" , x ,   "x" ,  1 , "constant" )
    apifun_checktype ( "nisp_ishigami" , a ,   "a" ,  2 , "constant" )
    apifun_checktype ( "nisp_ishigami" , b ,   "b" ,  3 , "constant" )
    //
    // Check size
    apifun_checkdims ( "nisp_ishigami" , x ,    "x" ,    1 , [size(x,"r") 3] )
    apifun_checkscalar ( "nisp_ishigami" , a ,  "a" ,   2 )
    apifun_checkscalar ( "nisp_ishigami" , b ,  "b" ,   3 )
    //
    // Check content
    apifun_checkrange ( "nisp_ishigami" , x ,  "x" ,  1 , -%pi , %pi )
    //
  s1=sin(x(:,1))
  s2=sin(x(:,2))
  x34 = x(:,3).^4
  y(:,1) = s1 + a.*s2.^2 + b.*x34.*s1
endfunction

