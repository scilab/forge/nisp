// Copyright (C) 2013 - Michael Baudin
// Copyright (C) 2010 - 2011 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

function exact = nisp_sumsa ( a , mu , sigma )
    // Returns the sensitivity indices of the Sum function
    //
    // Calling Sequence
    //   exact = nisp_sumsa ( a , mu , sigma )
    //
    // Parameters
    // a: a (nx+1)-by-1 or 1-by-(nx+1) matrix of doubles, where nx is the number of parameters
    // mu: a nx-by-1 or 1-by-nx matrix of doubles, the means.
    // sigma: a nx-by-1 or 1-by-nx matrix of doubles, the standard deviations.
    // exact: a struct with the following fields: expectation, var, S, ST.
    //
    // Description
    //   This function returns the first and 
    //   total order sensitivity indices of the Sum function. 
    //
    // This function is defined by 
    //
    // <latex>
    // \begin{eqnarray}
    // Y = a_1 X_1 + a_2 X_2 + ... + a_{nx} X_{nx} + a_{nx+1}
    // \end{eqnarray}
    // </latex>
    //
    // where X(i) is a random variables with mean  
    // mu(i) and standard variation sigma(i), 
    // for i=1,2,...,nx.
    // 
	// The expected value of Y is:
    //
    // <latex>
    // \begin{eqnarray}
    // E(Y)=a_1 \mu_1+...+a_{nx} \mu_p+a_{nx+1}.
    // \end{eqnarray}
    // </latex>
    // 
	// The variance of Y is:
    //
    // <latex>
    // \begin{eqnarray}
    // V(Y)=a_1^2 \sigma_1^2+...+a_{nx}^2 \sigma_{nx}^2
    // \end{eqnarray}
    // </latex>
	//
    // The first order indices are
    //
    // <latex>
    // \begin{eqnarray}
    // S_i = \frac{a_i^2 \sigma_i^2}{V(Y)}
    // \end{eqnarray}
    // </latex>
    //
    // for i=1,2,...,nx.
    //
    //   Other higher order sensitivity indices are zero, 
	//   since there is no interaction between the variables. 
	//
    //   The total sensitivity indices are equal to the 
    //   first order indices:
    //
    // <latex>
    // \begin{eqnarray}
    // ST_i=S_i
    // \end{eqnarray}
    // </latex>
    //
    // for i=1,2,...,nx.
    //
    // Examples
    // mu=[0 0 0 0]';
    // sigma=[1 2 3 4]';
    // a = [1 1 1 1 0]'
    // exact = nisp_sumsa ( a , mu , sigma )
	// disp(exact.S)
	// disp(exact.ST)
    // // expectation: 0
    // // var: 30
    // // S: [0.0333333 0.1333333 0.3 0.5333333]'
    // // ST: [0.0333333 0.1333333 0.3 0.5333333]'
    //
    // mu=[1 2 3 4]';
    // sigma=[5 6 7 8]';
    // a = [1 2 3 4 5]';
    // exact = nisp_sumsa ( a , mu , sigma )
	// disp(exact.S)
	// disp(exact.ST)
    // // expectation: 35
    // // var: 1634
    // // S: [0.0152999 0.0881273 0.2698898 0.6266830]'
    // // ST: [0.0152999 0.0881273 0.2698898 0.6266830]'
    //
    // Authors
    // Copyright (C) 2013 - Michael Baudin
    // Copyright (C) 2008-2011 - INRIA - Michael Baudin
    //
    // Bibliography
    // "Sensitivity analysis in practice", Saltelli, Tarantolla, Compolongo, Ratto, Wiley, 2004
    // "An importance quantification technique in uncertainty analysis for computer models", Ishigami, Homma, 1990, Proceedings of the ISUMA'90. First international symposium on uncertainty modelling and Analysis, University of Maryland, USA, pp. 398-403.

    [lhs, rhs] = argn();
    apifun_checkrhs ( "nisp_sumsa" , rhs , 3 : 3 )
    apifun_checklhs ( "nisp_sumsa" , lhs , 0:1 )
    //
    //
    // Check input arguments
    //
    // Check type
    apifun_checktype ( "nisp_sumsa" , a ,   "a" ,  1 , "constant" )
    apifun_checktype ( "nisp_sumsa" , mu ,   "mu" ,  2 , "constant" )
    apifun_checktype ( "nisp_sumsa" , sigma ,   "sigma" ,  3 , "constant" )
    //
    // Check size
    nx=size(mu,"*")
    apifun_checkvector ( "nisp_sumsa" , a ,     "a" ,     1 , (nx+1) )
    apifun_checkvector ( "nisp_sumsa" , mu ,    "mu" ,    2 , nx )
    apifun_checkvector ( "nisp_sumsa" , sigma , "sigma" , 3 , nx )
    //
    // Check content
    tiniest=number_properties("tiniest")
    apifun_checkgreq  ( "nisp_sumsa" , sigma ,    "sigma" , 3 , tiniest )
    //
    a=a(:)
    mu=mu(:)
    sigma=sigma(:)
    exact.expectation = sum(a(1:nx).*mu)+a(nx+1);
    exact.var = sum((a(1:nx).*sigma).^2);
    exact.S = a(1:nx).^2 .*sigma(1:nx).^2 ./exact.var
    exact.ST=exact.S
endfunction

