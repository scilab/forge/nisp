// Copyright (C) 2012 - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html


function polychaos_sasummary(pc)
    // Compute sensitivity indices by brute force.
    //
    // Calling Sequence
    //   polychaos_sasummary(pc)
    //
    // Parameters
    //   pc : a polynomial chaos
    //
    // Description
    // Prints a summary of the sensitivity analysis of the 
    // based on the polynomial chaos decomposition. 
    //
    // Examples
    // // Load several functions require for the test.
    // path = nisp_getpath();
    // testpath = fullfile(path,"tests","unit_tests","polychaos");
    // exec(fullfile(testpath,"createMyPolychaos.sci"));
    // exec(fullfile(testpath,"cleanupMyPolychaos.sci"));
    // 
    // [pc,srvx,rvx1,rvx2,srvu,vu1,vu2]=createMyPolychaos();
    // polychaos_computeexp(pc,srvx,"Integration");
    // polychaos_sasummary ( pc );
    // cleanupMyPolychaos(pc,srvx,rvx1,rvx2,srvu,vu1,vu2);
    //
    // Authors
    //   Copyright (C) 2012 - Michael Baudin

    [lhs, rhs] = argn();
    apifun_checkrhs ( "polychaos_sasummary" , rhs , 1:1 )
    apifun_checklhs ( "polychaos_sasummary" , lhs , 0:1 )
    //
    //
    // Check input arguments
    //
    // Check type
    apifun_checktype ( "polychaos_sasummary" , pc ,   "pc" ,  1 , "constant" )
    //
    // Check size
    apifun_checkscalar ( "polychaos_sasummary" , pc ,    "pc" , 1 )
    //
    // Check content
    apifun_checkflint ( "polychaos_sasummary" , pc , "pc" , 1 )
    apifun_checkgreq ( "polychaos_sasummary" , pc , "pc" , 1, 0 )
    pctok = polychaos_tokens()
    if (find(pc==pctok)==[]) then
        error(mprintf(gettext("%s: polychaos %d does not exist"),"polychaos_sasummary",pc))
    end
    //
    // Proceed...

    average = polychaos_getmean(pc);
    var = polychaos_getvariance(pc);
    mprintf("Mean     = %s\n",nisp_tostring(average));
    mprintf("Variance = %s\n",nisp_tostring(var));
    nx = polychaos_getdiminput(pc)
    ny = polychaos_getdimoutput(pc)
    mprintf("First order sensitivity indices\n");
    for j=1:ny
        mprintf("  Output #%d\n",j);
        for i = 1:nx
            si = polychaos_getindexfirst(pc,i,j)
            mprintf("    Variable X%d = %s\n",i,string(si));
        end
    end
    mprintf("Total sensitivity indices\n");
    for j=1:ny
        mprintf("  Output #%d\n",j);
        for i = 1:nx
            sti=polychaos_getindextotal(pc,i,j)
            mprintf("    Variable X%d = %s\n",i,string(sti));
        end
    end
    mprintf("Functional ANOVA:\n");
    polychaos_getanova(pc)
endfunction
function s = nisp_tostring ( x )
    if ( x==[] ) then
        s = "[]"
    else
        n = size ( x , "*" )
        if ( n == 1 ) then
            s = string(x)
        else
            s = "["+strcat(string(x)," ")+"]"
        end
    end
endfunction


